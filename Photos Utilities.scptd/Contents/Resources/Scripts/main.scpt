FasdUAS 1.101.10   ��   ��    k             l     ��  ��    < 6 Original downloaded from http://photosautomation.com/     � 	 	 l   O r i g i n a l   d o w n l o a d e d   f r o m   h t t p : / / p h o t o s a u t o m a t i o n . c o m /   
  
 l     ��  ��    ~ x Updated 14 Aug 2020 by Henry Kautz to use extension "jpeg" instead of "jpg" if Photos uses "jpeg" when exporting a file     �   �   U p d a t e d   1 4   A u g   2 0 2 0   b y   H e n r y   K a u t z   t o   u s e   e x t e n s i o n   " j p e g "   i n s t e a d   o f   " j p g "   i f   P h o t o s   u s e s   " j p e g "   w h e n   e x p o r t i n g   a   f i l e      l     ��������  ��  ��        x     �� ����    4    �� 
�� 
frmk  m       �    M a p K i t��        x    �� ����    4    �� 
�� 
frmk  m       �    C o r e L o c a t i o n��        x    %�� ����    4    ��  
�� 
frmk   m     ! ! � " "  F o u n d a t i o n��     # $ # x   % 2�� %����   % 4   ' +�� &
�� 
frmk & m   ) * ' ' � ( (  A d d r e s s B o o k��   $  ) * ) x   2 >�� +����   + 2  4 7��
�� 
osax��   *  , - , x   > G�� . /��   . 1      ��
�� 
ascr / �� 0��
�� 
minv 0 m       1 1 � 2 2  2 . 4��   -  3 4 3 l     ��������  ��  ��   4  5 6 5 j   G I�� 7�� &0 defaultimagewidth defaultImageWidth 7 m   G H����� 6  8 9 8 l     ��������  ��  ��   9  : ; : l      �� < =��   <
 Photos Utilities Script Library

The Photos Utilities script library is comprised of suites of commands designed to augment the existing scripting support in the Photos application. NOTE: this script library requires the installation of OS X v10.11 El Capitan or newer.

To use this script library, place its bundle file into the Script Libraries folder located within the user�s Library folder (if there is no Script Libraries folder, then add one). Next, insert the following statement at the top of scripts that will be incorporating commands from this library:

	use script "Photos Utilities"

Once the library has been installed in the user�s Library folder, and referenced at the beginning of the script, you can use the library�s commands throughout the script, like this:

	tell application "Photos"
		set theseItems to (get the selection)
		quick export theseItems with resulting file references and Finder reveal
	end tell

Detailed documentation for the Photos Utilities script library can be found online at the link provided below:

	<https://photosautomation.com>

For those interested in creating and writing scripts and script libraries, the Photos Utilities Script Library is written in AppleScriptObj-C, a dynamic fusion of the AppleScript and Objective-C programming languages. Comprehensive 3rd-party documentation and tools for writing and deploying AppleScriptObj-C applications, scripts, and script libraries are available from Myriad Communications online at:

	<https://macosxautomation.com/applescript/apps/index.html>
    = � > >   P h o t o s   U t i l i t i e s   S c r i p t   L i b r a r y 
 
 T h e   P h o t o s   U t i l i t i e s   s c r i p t   l i b r a r y   i s   c o m p r i s e d   o f   s u i t e s   o f   c o m m a n d s   d e s i g n e d   t o   a u g m e n t   t h e   e x i s t i n g   s c r i p t i n g   s u p p o r t   i n   t h e   P h o t o s   a p p l i c a t i o n .   N O T E :   t h i s   s c r i p t   l i b r a r y   r e q u i r e s   t h e   i n s t a l l a t i o n   o f   O S   X   v 1 0 . 1 1   E l   C a p i t a n   o r   n e w e r . 
 
 T o   u s e   t h i s   s c r i p t   l i b r a r y ,   p l a c e   i t s   b u n d l e   f i l e   i n t o   t h e   S c r i p t   L i b r a r i e s   f o l d e r   l o c a t e d   w i t h i n   t h e   u s e r  s   L i b r a r y   f o l d e r   ( i f   t h e r e   i s   n o   S c r i p t   L i b r a r i e s   f o l d e r ,   t h e n   a d d   o n e ) .   N e x t ,   i n s e r t   t h e   f o l l o w i n g   s t a t e m e n t   a t   t h e   t o p   o f   s c r i p t s   t h a t   w i l l   b e   i n c o r p o r a t i n g   c o m m a n d s   f r o m   t h i s   l i b r a r y : 
 
 	 u s e   s c r i p t   " P h o t o s   U t i l i t i e s " 
 
 O n c e   t h e   l i b r a r y   h a s   b e e n   i n s t a l l e d   i n   t h e   u s e r  s   L i b r a r y   f o l d e r ,   a n d   r e f e r e n c e d   a t   t h e   b e g i n n i n g   o f   t h e   s c r i p t ,   y o u   c a n   u s e   t h e   l i b r a r y  s   c o m m a n d s   t h r o u g h o u t   t h e   s c r i p t ,   l i k e   t h i s : 
 
 	 t e l l   a p p l i c a t i o n   " P h o t o s " 
 	 	 s e t   t h e s e I t e m s   t o   ( g e t   t h e   s e l e c t i o n ) 
 	 	 q u i c k   e x p o r t   t h e s e I t e m s   w i t h   r e s u l t i n g   f i l e   r e f e r e n c e s   a n d   F i n d e r   r e v e a l 
 	 e n d   t e l l 
 
 D e t a i l e d   d o c u m e n t a t i o n   f o r   t h e   P h o t o s   U t i l i t i e s   s c r i p t   l i b r a r y   c a n   b e   f o u n d   o n l i n e   a t   t h e   l i n k   p r o v i d e d   b e l o w : 
 
 	 < h t t p s : / / p h o t o s a u t o m a t i o n . c o m > 
 
 F o r   t h o s e   i n t e r e s t e d   i n   c r e a t i n g   a n d   w r i t i n g   s c r i p t s   a n d   s c r i p t   l i b r a r i e s ,   t h e   P h o t o s   U t i l i t i e s   S c r i p t   L i b r a r y   i s   w r i t t e n   i n   A p p l e S c r i p t O b j - C ,   a   d y n a m i c   f u s i o n   o f   t h e   A p p l e S c r i p t   a n d   O b j e c t i v e - C   p r o g r a m m i n g   l a n g u a g e s .   C o m p r e h e n s i v e   3 r d - p a r t y   d o c u m e n t a t i o n   a n d   t o o l s   f o r   w r i t i n g   a n d   d e p l o y i n g   A p p l e S c r i p t O b j - C   a p p l i c a t i o n s ,   s c r i p t s ,   a n d   s c r i p t   l i b r a r i e s   a r e   a v a i l a b l e   f r o m   M y r i a d   C o m m u n i c a t i o n s   o n l i n e   a t : 
 
 	 < h t t p s : / / m a c o s x a u t o m a t i o n . c o m / a p p l e s c r i p t / a p p s / i n d e x . h t m l > 
 ;  ? @ ? l     ��������  ��  ��   @  A B A l      �� C D��   C !  ITEM PATHS WITHIN LIBRARY     D � E E 6   I T E M   P A T H S   W I T H I N   L I B R A R Y   B  F G F l     ��������  ��  ��   G  H I H i   J M J K J I     ���� L
�� .PUTLLIBPnull��� ��� null��   L �� M N
�� 
REPR M o      ���� 0 targetobject targetObject N �� O P
�� 
APID O |���� Q�� R��  ��   Q o      ���� (0 shouldappenditemid shouldAppendItemID��   R m      ��
�� boovfals P �� S��
�� 
DELM S |���� T�� U��  ��   T o      ���� &0 pathitemdelimiter pathItemDelimiter��   U m      ��
�� 
msng��   K k     S V V  W X W Z      Y Z���� Y =     [ \ [ o     ���� &0 pathitemdelimiter pathItemDelimiter \ m    ��
�� 
msng Z r     ] ^ ] n    _ ` _ I    �� a���� 40 getlocalizedstringforkey getLocalizedStringForKey a  b�� b m     c c � d d D D E F A U L T _ L O C A T I O N _ E L E M E N T _ S E P A R A T O R��  ��   `  f     ^ o      ���� &0 pathitemdelimiter pathItemDelimiter��  ��   X  e�� e Z    S f g�� h f =    i j i l    k���� k n     l m l m    ��
�� 
pcls m o    ���� 0 targetobject targetObject��  ��   j m    ��
�� 
list g k    H n n  o p o r     q r q J    ����   r o      ���� "0 locationstrings locationStrings p  s t s Y     E u�� v w�� u k   . @ x x  y z y r   . 4 { | { n   . 2 } ~ } 4   / 2�� 
�� 
cobj  o   0 1���� 0 i   ~ o   . /���� 0 targetobject targetObject | o      ���� 0 
thisobject 
thisObject z  ��� � r   5 @ � � � I   5 =�� ����� V0 )deriveinyternallibrarypathtoalbumorfolder )deriveInyternalLibraryPathToAlbumOrFolder �  � � � o   6 7���� 0 
thisobject 
thisObject �  � � � o   7 8���� (0 shouldappenditemid shouldAppendItemID �  ��� � o   8 9���� &0 pathitemdelimiter pathItemDelimiter��  ��   � l      ����� � n       � � �  ;   > ? � o   = >���� "0 locationstrings locationStrings��  ��  ��  �� 0 i   v m   # $����  w l  $ ) ����� � I  $ )�� ���
�� .corecnte****       **** � o   $ %���� 0 targetobject targetObject��  ��  ��  ��   t  ��� � L   F H � � o   F G���� "0 locationstrings locationStrings��  ��   h I   K S�� ����� V0 )deriveinyternallibrarypathtoalbumorfolder )deriveInyternalLibraryPathToAlbumOrFolder �  � � � o   L M���� 0 targetobject targetObject �  � � � o   M N���� (0 shouldappenditemid shouldAppendItemID �  ��� � o   N O���� &0 pathitemdelimiter pathItemDelimiter��  ��  ��   I  � � � l     ��������  ��  ��   �  � � � l      �� � ���   �   TOP LEVEL ALBUMS     � � � � $   T O P   L E V E L   A L B U M S   �  � � � l     ��������  ��  ��   �  � � � i   N Q � � � I     ������
�� .PUTLTALBnull��� ��� null��  ��   � I     �������� &0 gettoplevelalbums getTopLevelAlbums��  ��   �  � � � l     ��������  ��  ��   �  � � � i   R U � � � I     ������
�� .PUTLTALNnull��� ��� null��  ��   � I     
�� ����� 40 getnamesofspecifieditems getNamesOfSpecifiedItems �  ��� � I    ����~�� &0 gettoplevelalbums getTopLevelAlbums�  �~  ��  ��   �  � � � l     �}�|�{�}  �|  �{   �  � � � i   V Y � � � I     �z�y �
�z .PUTLTABMnull��� ��� null�y   � �x ��w
�x 
naME � o      �v�v 0 thisname thisName�w   � I     �u ��t�u .0 gettoplevelalbumnamed getTopLevelAlbumNamed �  ��s � o    �r�r 0 thisname thisName�s  �t   �  � � � l     �q�p�o�q  �p  �o   �  � � � l      �n � ��n   �   TOP LEVEL FOLDERS     � � � � &   T O P   L E V E L   F O L D E R S   �  � � � l     �m�l�k�m  �l  �k   �  � � � i   Z ] � � � I     �j�i�h
�j .PUTLTFLDnull��� ��� null�i  �h   � I     �g�f�e�g (0 gettoplevelfolders getTopLevelFolders�f  �e   �  � � � l     �d�c�b�d  �c  �b   �  � � � i   ^ a � � � I     �a�`�_
�a .PUTLTFDNnull��� ��� null�`  �_   � I     
�^ ��]�^ 40 getnamesofspecifieditems getNamesOfSpecifiedItems �  ��\ � I    �[�Z�Y�[ (0 gettoplevelfolders getTopLevelFolders�Z  �Y  �\  �]   �  � � � l     �X�W�V�X  �W  �V   �  � � � i   b e � � � I     �U�T �
�U .PUTLTFDRnull��� ��� null�T   � �S ��R
�S 
naME � o      �Q�Q 0 thisname thisName�R   � I     �P ��O�P 00 gettoplevelfoldernamed getTopLevelFolderNamed �  ��N � o    �M�M 0 thisname thisName�N  �O   �  � � � l     �L�K�J�L  �K  �J   �  � � � l     �I�H�G�I  �H  �G   �  � � � l      �F � ��F   �   UTILITY HANDLERS     � � � � $   U T I L I T Y   H A N D L E R S   �  � � � l     �E�D�C�E  �D  �C   �  � � � i   f i � � � I     �B�A �
�B .PUTLWAITnull��� ��� null�A   � �@ ��?
�@ 
WDUR � |�>�= ��< ��>  �=   � o      �;�; 40 timeoutdurationinseconds timeoutDurationInSeconds�<   � m      �:�:,�?   � k      � �  � � � Z      � ��9�8 � A     � � � o     �7�7 40 timeoutdurationinseconds timeoutDurationInSeconds � m    �6�6, � l   	 � � � � r    	 � � � m    �5�5, � o      �4�4 40 timeoutdurationinseconds timeoutDurationInSeconds �  
 5-minutes    � � � �    5 - m i n u t e s�9  �8   �  ��3 � I    �2 ��1�2 0 waitforphotos waitForPhotos �  ��0 � o    �/�/ 40 timeoutdurationinseconds timeoutDurationInSeconds�0  �1  �3   �  � � � l     �.�-�,�.  �-  �,   �  � � � l      �+ � ��+   �   EXPORT COMMANDS     � � � � "   E X P O R T   C O M M A N D S   �  � � � l     �*�)�(�*  �)  �(   �    i   j m I     �'�&
�' .PEXPQEXPnull��� ��� null�&   �%
�% 
EXOF o      �$�$ "0 thesemediaitems theseMediaItems �#
�# 
EXID |�"�!	� 
�"  �!  	 o      �� &0 shouldnameusingid shouldNameUsingID�   
 m      �
� boovfals �
� 
EXRF |����  �   o      �� ,0 shouldreturnfilerefs shouldReturnFileRefs�   m      �
� boovtrue ��
� 
EXRV |����  �   o      �� ,0 shouldrevealinfinder shouldRevealInFinder�   m      �
� boovfals�   k     Y  Z     �� >     l    �� n      m    �
� 
pcls o     �� "0 thesemediaitems theseMediaItems�  �   m    �

�
 
list r     c     o    	�	�	 "0 thesemediaitems theseMediaItems m   	 
�
� 
list o      �� "0 thesemediaitems theseMediaItems�  �    !  Z   &"#��" =   $%$ o    �� "0 thesemediaitems theseMediaItems% J    ��  # R    "�&�
� .ascrerr ****      � ****& n   !'(' I    !� )���  40 getlocalizedstringforkey getLocalizedStringForKey) *��* m    ++ �,, . N O _ M E D I A _ I T E M S _ P R O V I D E D��  ��  (  f    �  �  �  ! -.- Z   ' 6/0��1/ =  ' *232 o   ' (���� &0 shouldnameusingid shouldNameUsingID3 m   ( )��
�� boovfals0 l  - 04564 r   - 0787 m   - .���� 8 o      ���� .0 namingmethodindicator namingMethodIndicator5   current file names   6 �99 &   c u r r e n t   f i l e   n a m e s��  1 l  3 6:;<: r   3 6=>= m   3 4���� > o      ���� .0 namingmethodindicator namingMethodIndicator;    Photos IDs for file names   < �?? 4   P h o t o s   I D s   f o r   f i l e   n a m e s. @A@ r   7 :BCB m   7 8��
�� boovfalsC o      ���� .0 shouldexportoriginals shouldExportOriginalsA DED r   ; >FGF m   ; <HH �II  G o      ���� 0 basename baseNameE JKJ r   ? BLML m   ? @NN �OO  M o      ���� 0 thisseparator thisSeparatorK PQP r   C FRSR m   C D���� S o      ���� "0 thisdigitlength thisDigitLengthQ TUT r   G JVWV m   G H���� W o      ����  0 startingnumber startingNumberU X��X I   K Y��Y���� 0 quickexport quickExportY Z[Z o   L M���� "0 thesemediaitems theseMediaItems[ \]\ o   M N���� ,0 shouldreturnfilerefs shouldReturnFileRefs] ^_^ o   N O���� ,0 shouldrevealinfinder shouldRevealInFinder_ `a` o   O P���� .0 namingmethodindicator namingMethodIndicatora bcb o   P Q���� .0 shouldexportoriginals shouldExportOriginalsc ded o   Q R���� 0 basename baseNamee fgf o   R S���� 0 thisseparator thisSeparatorg hih o   S T���� "0 thisdigitlength thisDigitLengthi j��j o   T U����  0 startingnumber startingNumber��  ��  ��   klk l     ��������  ��  ��  l mnm i   n qopo I     ����q
�� .PEXPEXORnull��� ��� null��  q ��rs
�� 
EXOFr o      ���� "0 thesemediaitems theseMediaItemss ��tu
�� 
EXRFt |����v��w��  ��  v o      ���� ,0 shouldreturnfilerefs shouldReturnFileRefs��  w m      ��
�� boovtrueu ��x��
�� 
EXRVx |����y��z��  ��  y o      ���� ,0 shouldrevealinfinder shouldRevealInFinder��  z m      ��
�� boovfals��  p k     M{{ |}| Z     ~����~ >    ��� l    ������ n     ��� m    ��
�� 
pcls� o     ���� "0 thesemediaitems theseMediaItems��  ��  � m    ��
�� 
list r    ��� c    ��� o    	���� "0 thesemediaitems theseMediaItems� m   	 
��
�� 
list� o      ���� "0 thesemediaitems theseMediaItems��  ��  } ��� Z   &������� =   ��� o    ���� "0 thesemediaitems theseMediaItems� J    ����  � R    "�����
�� .ascrerr ****      � ****� n   !��� I    !������� 40 getlocalizedstringforkey getLocalizedStringForKey� ���� m    �� ��� . N O _ M E D I A _ I T E M S _ P R O V I D E D��  ��  �  f    ��  ��  ��  � ��� r   ' *��� m   ' (��
�� boovtrue� o      ���� .0 shouldexportoriginals shouldExportOriginals� ��� r   + .��� m   + ,�� ���  � o      ���� 0 basename baseName� ��� r   / 2��� m   / 0�� ���  � o      ���� 0 thisseparator thisSeparator� ��� r   3 6��� m   3 4���� � o      ���� "0 thisdigitlength thisDigitLength� ��� r   7 :��� m   7 8���� � o      ����  0 startingnumber startingNumber� ��� r   ; >��� m   ; <���� � o      ���� .0 namingmethodindicator namingMethodIndicator� ���� I   ? M������� 0 quickexport quickExport� ��� o   @ A���� "0 thesemediaitems theseMediaItems� ��� o   A B���� ,0 shouldreturnfilerefs shouldReturnFileRefs� ��� o   B C���� ,0 shouldrevealinfinder shouldRevealInFinder� ��� o   C D���� .0 namingmethodindicator namingMethodIndicator� ��� o   D E���� .0 shouldexportoriginals shouldExportOriginals� ��� o   E F���� 0 basename baseName� ��� o   F G���� 0 thisseparator thisSeparator� ��� o   G H���� "0 thisdigitlength thisDigitLength� ���� o   H I����  0 startingnumber startingNumber��  ��  ��  n ��� l     ��������  ��  ��  � ��� i   r u��� I     �����
�� .PEXPSQXPnull��� ��� null��  � ����
�� 
EXOF� o      ���� "0 thesemediaitems theseMediaItems� ����
�� 
EXBN� |����������  ��  � o      ���� 0 basename baseName��  � m      �� ���  � ����
�� 
EXSN� |����������  ��  � o      ����  0 startingnumber startingNumber��  � m      ��
�� 
msng� ����
�� 
EXSP� |����������  ��  � o      ���� "0 separatorstring separatorString��  � m      ��
�� 
msng� ����
�� 
EXSL� |����������  ��  � o      ����  0 sequencelength sequenceLength��  � m      ��
�� 
msng� ����
�� 
EXRF� |����������  ��  � o      ���� ,0 shouldreturnfilerefs shouldReturnFileRefs��  � m      ��
�� boovtrue� ���~
� 
EXRV� |�}�|��{��}  �|  � o      �z�z ,0 shouldrevealinfinder shouldRevealInFinder�{  � m      �y
�y boovfals�~  � k     ��� ��� Z    ���x�w� >    ��� l    ��v�u� n     ��� m    �t
�t 
pcls� o     �s�s "0 thesemediaitems theseMediaItems�v  �u  � m    �r
�r 
list� r    ��� c    ��� o    	�q�q "0 thesemediaitems theseMediaItems� m   	 
�p
�p 
list� o      �o�o "0 thesemediaitems theseMediaItems�x  �w  � ��� Z   &���n�m� =   ��� o    �l�l "0 thesemediaitems theseMediaItems� J    �k�k  � R    "�j��i
�j .ascrerr ****      � ****� n   !��� I    !�h��g�h 40 getlocalizedstringforkey getLocalizedStringForKey� ��f� m       � . N O _ M E D I A _ I T E M S _ P R O V I D E D�f  �g  �  f    �i  �n  �m  �  Z   ' 9�e�d =  ' * o   ' (�c�c 0 basename baseName m   ( ) �		   r   - 5

 n  - 3 I   . 3�b�a�b 40 getlocalizedstringforkey getLocalizedStringForKey �` m   . / � " D E F A U L T _ B A S E _ N A M E�`  �a    f   - . o      �_�_ 0 basename baseName�e  �d    Z   : ]�^ =  : = o   : ;�]�]  0 startingnumber startingNumber m   ; <�\
�\ 
msng r   @ C m   @ A�[�[  o      �Z�Z  0 startingnumber startingNumber  G   F S l  F K�Y�X >  F K !  n   F I"#" m   G I�W
�W 
pcls# o   F G�V�V  0 startingnumber startingNumber! m   I J�U
�U 
long�Y  �X   l  N Q$�T�S$ A  N Q%&% o   N O�R�R  0 startingnumber startingNumber& m   O P�Q�Q  �T  �S   '�P' k   V Y(( )*) l  V V�O+,�O  + A ; error my getLocalizedStringForKey("STARTING_NUMBER_ERROR")   , �-- v   e r r o r   m y   g e t L o c a l i z e d S t r i n g F o r K e y ( " S T A R T I N G _ N U M B E R _ E R R O R " )* .�N. r   V Y/0/ m   V W�M�M 0 o      �L�L  0 startingnumber startingNumber�N  �P  �^   121 Z   ^ k34�K�J3 =  ^ a565 o   ^ _�I�I "0 separatorstring separatorString6 m   _ `�H
�H 
msng4 r   d g787 1   d e�G
�G 
spac8 o      �F�F "0 separatorstring separatorString�K  �J  2 9:9 Z   l �;<�E�D; =  l o=>= o   l m�C�C  0 sequencelength sequenceLength> m   m n�B
�B 
msng< k   r �?? @A@ r   r yBCB l  r wD�A�@D I  r w�?E�>
�? .corecnte****       ****E o   r s�=�= "0 thesemediaitems theseMediaItems�>  �A  �@  C l     F�<�;F o      �:�:  0 sequencelength sequenceLength�<  �;  A G�9G r   z �HIH l  z J�8�7J n   z KLK 1   } �6
�6 
lengL l  z }M�5�4M c   z }NON o   z {�3�3  0 sequencelength sequenceLengthO m   { |�2
�2 
TEXT�5  �4  �8  �7  I o      �1�1  0 sequencelength sequenceLength�9  �E  �D  : PQP l  � �RSTR r   � �UVU m   � ��0�0  V o      �/�/ .0 namingmethodindicator namingMethodIndicatorS   sequence naming   T �WW     s e q u e n c e   n a m i n gQ XYX r   � �Z[Z m   � ��.
�. boovfals[ o      �-�- .0 shouldexportoriginals shouldExportOriginalsY \�,\ I   � ��+]�*�+ 0 quickexport quickExport] ^_^ o   � ��)�) "0 thesemediaitems theseMediaItems_ `a` o   � ��(�( ,0 shouldreturnfilerefs shouldReturnFileRefsa bcb o   � ��'�' ,0 shouldrevealinfinder shouldRevealInFinderc ded o   � ��&�& .0 namingmethodindicator namingMethodIndicatore fgf o   � ��%�% .0 shouldexportoriginals shouldExportOriginalsg hih o   � ��$�$ 0 basename baseNamei jkj o   � ��#�# "0 separatorstring separatorStringk lml o   � ��"�"  0 sequencelength sequenceLengthm n�!n o   � �� �   0 startingnumber startingNumber�!  �*  �,  � opo l     ����  �  �  p qrq l      �st�  s   IWORK INTEGRATION    t �uu &   I W O R K   I N T E G R A T I O N  r vwv l     ����  �  �  w xyx i   v yz{z I     ��|
� .PEXPKYXPnull��� ��� null�  | �}~
� 
EXUS} o      �� "0 thesemediaitems theseMediaItems~ ��
� 
NDOC |������  �  � o      �� 0 templatename templateName�  � m      �
� 
msng� ���
� 
EXKM� |������  �  � c      ��� o      �
�
 (0 documentdimensions documentDimensions� m      �	
�	 
list�  � m      �
� 
msng� ���
� 
EXKI� |������  �  � o      �� "0 newslideforeach newSlideForEach�  � m      �
� boovtrue�  { k    <�� ��� l     � �����   ��  ��  � ��� l     ������  � 6 0 CHECK TO SEE IF MEDIA ITEMS ARE PASSED AS INPUT   � ��� `   C H E C K   T O   S E E   I F   M E D I A   I T E M S   A R E   P A S S E D   A S   I N P U T� ��� Z    ������� >    ��� l    ������ n     ��� m    ��
�� 
pcls� o     ���� "0 thesemediaitems theseMediaItems��  ��  � m    ��
�� 
list� r    ��� c    ��� o    	���� "0 thesemediaitems theseMediaItems� m   	 
��
�� 
list� o      ���� "0 thesemediaitems theseMediaItems��  ��  � ��� Z   &������� =   ��� o    ���� "0 thesemediaitems theseMediaItems� J    ����  � R    "�����
�� .ascrerr ****      � ****� n   !��� I    !������� 40 getlocalizedstringforkey getLocalizedStringForKey� ���� m    �� ��� . N O _ M E D I A _ I T E M S _ P R O V I D E D��  ��  �  f    ��  ��  ��  � ��� l  ' '��������  ��  ��  � ��� l  ' '������  �   KEYNOTE   � ���    K E Y N O T E� ��� Z   ' E������� =  ' 0��� n   ' .��� 1   , .��
�� 
prun� 5   ' ,�����
�� 
capp� m   ) *�� ��� . c o m . a p p l e . i W o r k . K e y n o t e
�� kfrmID  � m   . /��
�� boovfals� O  3 A��� I  ; @������
�� .ascrnoop****      � ****��  ��  � 5   3 8�����
�� 
capp� m   5 6�� ��� . c o m . a p p l e . i W o r k . K e y n o t e
�� kfrmID  ��  ��  � ��� O   F Y��� r   N X��� l  N V������ I  N V�����
�� .coredoexnull���     ****� 4   N R���
�� 
docu� m   P Q���� ��  ��  ��  � o      ���� 00 existingdocumentstatus existingDocumentStatus� 5   F K�����
�� 
capp� m   H I�� ��� . c o m . a p p l e . i W o r k . K e y n o t e
�� kfrmID  � ��� l  Z Z��������  ��  ��  � ��� l  Z Z������  � "  CHECK THE DIMENSIONS OPTION   � ��� 8   C H E C K   T H E   D I M E N S I O N S   O P T I O N� ��� Z   Z ������� G   Z e��� l  Z ]������ =  Z ]��� o   Z [���� (0 documentdimensions documentDimensions� m   [ \��
�� 
msng��  ��  � l  ` c������ =  ` c��� o   ` a���� (0 documentdimensions documentDimensions� m   a b�� ���  ( n u l l )��  ��  � r   h k��� m   h i��
�� boovfals� o      ���� *0 shouldusedimensions shouldUseDimensions��  � k   n ��� ��� Z   n ������� >  n u��� l  n s������ l  n s������ I  n s�����
�� .corecnte****       ****� o   n o���� (0 documentdimensions documentDimensions��  ��  ��  ��  ��  � m   s t���� � R   x ������
�� .ascrerr ****      � ****� l  z ������� n  z ���� I   { �������� 40 getlocalizedstringforkey getLocalizedStringForKey� ���� m   { ~�� ��� 0 K E Y N O T E _ D I M E N S I O N S _ E R R O R��  ��  �  f   z {��  ��  ��  ��  � k   � ��� ��� s   � �� � o   � ����� (0 documentdimensions documentDimensions  J        o      ���� $0 newdocumentwidth newDocumentWidth �� o      ���� &0 newdocumentheight newDocumentHeight��  � �� Z   � ����� G   � �	 >  � �

 l  � ����� n   � � m   � ���
�� 
pcls o   � ����� $0 newdocumentwidth newDocumentWidth��  ��   m   � ���
�� 
long	 >  � � l  � ����� n   � � m   � ���
�� 
pcls o   � ����� &0 newdocumentheight newDocumentHeight��  ��   m   � ���
�� 
long R   � �����
�� .ascrerr ****      � **** l  � ����� n  � � I   � ������� 40 getlocalizedstringforkey getLocalizedStringForKey �� m   � � � 0 K E Y N O T E _ D I M E N S I O N S _ E R R O R��  ��    f   � ���  ��  ��  ��  ��  ��  � �� r   � � m   � ���
�� boovtrue o      ���� *0 shouldusedimensions shouldUseDimensions��  �   l  � ���������  ��  ��    !"! l  � ���#$��  # !  CHECK THE TEMPLATES OPTION   $ �%% 6   C H E C K   T H E   T E M P L A T E S   O P T I O N" &'& Z   �()��*( G   � �+,+ l  � �-����- =  � �./. o   � ����� 0 templatename templateName/ m   � ���
�� 
msng��  ��  , l  � �0����0 =  � �121 o   � ����� 0 templatename templateName2 m   � �33 �44  ( n u l l )��  ��  ) r   � �565 m   � ���
�� boovfals6 o      ���� .0 shouldusetemplatename shouldUseTemplateName��  * k   �77 898 O   � �:;: r   � �<=< l  � �>����> n   � �?@? 1   � ���
�� 
pnam@ 2   � ���
�� 
Knth��  ��  = l     A����A o      ���� 0 
themenames 
themeNames��  ��  ; 5   � ���B��
�� 
cappB m   � �CC �DD . c o m . a p p l e . i W o r k . K e y n o t e
�� kfrmID  9 EFE Z   �GH����G H   � �II E  � �JKJ o   � ��� 0 
themenames 
themeNamesK o   � ��~�~ 0 templatename templateNameH R   ��}L�|
�} .ascrerr ****      � ****L b   �MNM l  �O�{�zO n  �PQP I   ��yR�x�y 40 getlocalizedstringforkey getLocalizedStringForKeyR S�wS m   �TT �UU & K E Y N O T E _ T H E M E _ E R R O R�w  �x  Q  f   � ��{  �z  N o  �v�v 0 templatename templateName�|  ��  ��  F V�uV r  WXW m  �t
�t boovtrueX o      �s�s .0 shouldusetemplatename shouldUseTemplateName�u  ' YZY l �r�q�p�r  �q  �p  Z [\[ l �o]^�o  ] H B DETERMINE WHETHER TO MAKE A NEW DOCUMENT OR USE EXISTING DOCUMENT   ^ �__ �   D E T E R M I N E   W H E T H E R   T O   M A K E   A   N E W   D O C U M E N T   O R   U S E   E X I S T I N G   D O C U M E N T\ `a` Z  0bc�ndb G  $efe G  ghg l i�m�li = jkj o  �k�k *0 shouldusedimensions shouldUseDimensionsk m  �j
�j boovtrue�m  �l  h l l�i�hl = mnm o  �g�g .0 shouldusetemplatename shouldUseTemplateNamen m  �f
�f boovtrue�i  �h  f l "o�e�do = "pqp o   �c�c 00 existingdocumentstatus existingDocumentStatusq m   !�b
�b boovfals�e  �d  c r  '*rsr m  '(�a
�a boovtrues o      �`�` .0 shouldmakenewdocument shouldMakeNewDocument�n  d r  -0tut m  -.�_
�_ boovfalsu o      �^�^ .0 shouldmakenewdocument shouldMakeNewDocumenta vwv l 11�]�\�[�]  �\  �[  w xyx l 11�Zz{�Z  z V P EXPORT IMAGES WITH IDS FOR FILE NAMES. ENABLES LINKING BACK TO PHOTOS LIBRARY.    { �|| �   E X P O R T   I M A G E S   W I T H   I D S   F O R   F I L E   N A M E S .   E N A B L E S   L I N K I N G   B A C K   T O   P H O T O S   L I B R A R Y .  y }~} r  1G� I  1E�Y��X�Y 0 quickexport quickExport� ��� o  23�W�W "0 thesemediaitems theseMediaItems� ��� m  34�V
�V boovtrue� ��� m  45�U
�U boovfals� ��� m  56�T�T � ��� m  67�S
�S boovfals� ��� m  7:�� ���  � ��� m  :=�� ���  � ��� m  =>�R�R � ��Q� m  >?�P�P �Q  �X  � o      �O�O (0 exportedimagefiles exportedImageFiles~ ��� l HH�N�M�L�N  �M  �L  � ��� l HH�K���K  � $  MAKE A NEW DOCUMENT IF NEEDED   � ��� <   M A K E   A   N E W   D O C U M E N T   I F   N E E D E D� ��� Z  H	���J�� = HK��� o  HI�I�I .0 shouldmakenewdocument shouldMakeNewDocument� m  IJ�H
�H boovtrue� O  N���� k  X��� ��� I X]�G�F�E
�G .miscactvnull��� ��� null�F  �E  � ��D� Z  ^�����C� F  ^i��� = ^a��� o  ^_�B�B .0 shouldusetemplatename shouldUseTemplateName� m  _`�A
�A boovtrue� = dg��� o  de�@�@ *0 shouldusedimensions shouldUseDimensions� m  ef�?
�? boovtrue� r  l���� I l��>�=�
�> .corecrel****      � null�=  � �<��
�< 
kocl� m  pq�;
�; 
docu� �:��9
�: 
prdt� K  t��� �8��
�8 
Kndt� 4  w}�7�
�7 
Knth� o  {|�6�6 0 templatename templateName� �5��
�5 
sitw� o  ���4�4 $0 newdocumentwidth newDocumentWidth� �3��2
�3 
sith� o  ���1�1 &0 newdocumentheight newDocumentHeight�2  �9  � o      �0�0 0 thisdocument thisDocument� ��� F  ����� = ����� o  ���/�/ .0 shouldusetemplatename shouldUseTemplateName� m  ���.
�. boovfals� = ����� o  ���-�- *0 shouldusedimensions shouldUseDimensions� m  ���,
�, boovtrue� ��� r  ����� I ���+�*�
�+ .corecrel****      � null�*  � �)��
�) 
kocl� m  ���(
�( 
docu� �'��&
�' 
prdt� K  ���� �%��
�% 
sitw� o  ���$�$ $0 newdocumentwidth newDocumentWidth� �#��"
�# 
sith� o  ���!�! &0 newdocumentheight newDocumentHeight�"  �&  � o      � �  0 thisdocument thisDocument� ��� F  ����� = ����� o  ���� .0 shouldusetemplatename shouldUseTemplateName� m  ���
� boovtrue� = ����� o  ���� *0 shouldusedimensions shouldUseDimensions� m  ���
� boovfals� ��� r  ����� I �����
� .corecrel****      � null�  � ���
� 
kocl� m  ���
� 
docu� ���
� 
prdt� K  ���� ���
� 
Kndt� 4  ����
� 
Knth� o  ���� 0 templatename templateName�  �  � o      �� 0 thisdocument thisDocument�  �C  �D  � 5  NU���
� 
capp� m  PS�� ��� . c o m . a p p l e . i W o r k . K e y n o t e
� kfrmID  �J  � O  �	��� k  ��� ��� I ����
� .miscactvnull��� ��� null�  �  � ��
� r  ��� 4 �	�
�	 
docu� m  �� � o      �� 0 thisdocument thisDocument�
  � 5  �����
� 
capp� m  ���� ��� . c o m . a p p l e . i W o r k . K e y n o t e
� kfrmID  � ��� l 

����  �  �  � ��� l 

����  �   IMPORT IMAGE FILES   � ��� &   I M P O R T   I M A G E   F I L E S� ��� r  
� � J  
� �     o      ���� (0 newslidereferences newSlideReferences�  Z  ��� =  o  ���� "0 newslideforeach newSlideForEach m  ��
�� boovtrue O  \	 Y  [
����
 k  -V  r  -5 n  -3 4  .3��
�� 
cobj o  12���� 0 i   o  -.���� (0 exportedimagefiles exportedImageFiles o      ���� 0 thisimagefile thisImageFile  r  6@ n 6< I  7<������ 0 newblankslide newBlankSlide �� o  78���� 0 thisdocument thisDocument��  ��    f  67 o      ���� 0 	thisslide 	thisSlide  l AA�� ��   L F (thisSlide, thisImageFile, scaleToFill, shouldUseDescriptionForNotes)     �!! �   ( t h i s S l i d e ,   t h i s I m a g e F i l e ,   s c a l e T o F i l l ,   s h o u l d U s e D e s c r i p t i o n F o r N o t e s ) "#" n AO$%$ I  BO��&���� *0 addimagefiletoslide addImageFileToSlide& '(' o  BC���� 0 thisdocument thisDocument( )*) o  CF���� 0 	thisslide 	thisSlide* +,+ o  FG���� 0 thisimagefile thisImageFile, -.- m  GH��
�� boovtrue. /��/ m  HI��
�� boovtrue��  ��  %  f  AB# 0��0 r  PV121 o  PS���� 0 	thisslide 	thisSlide2 l     3����3 n      454  ;  TU5 o  ST���� (0 newslidereferences newSlideReferences��  ��  ��  �� 0 i   m  "#����  I #(��6��
�� .corecnte****       ****6 o  #$���� (0 exportedimagefiles exportedImageFiles��  ��  	 5  ��7��
�� 
capp7 m  88 �99 . c o m . a p p l e . i W o r k . K e y n o t e
�� kfrmID  ��   O  _�:;: k  i�<< =>= r  ir?@? n inABA I  jn�������� 0 newblankslide newBlankSlide��  ��  B  f  ij@ o      ���� 0 	thisslide 	thisSlide> CDC Y  s�E��FG��E k  ��HH IJI r  ��KLK n  ��MNM 4  ����O
�� 
cobjO o  ������ 0 i  N o  ������ (0 exportedimagefiles exportedImageFilesL o      ���� 0 thisimagefile thisImageFileJ PQP l ����RS��  R L F (thisSlide, thisImageFile, scaleToFill, shouldUseDescriptionForNotes)   S �TT �   ( t h i s S l i d e ,   t h i s I m a g e F i l e ,   s c a l e T o F i l l ,   s h o u l d U s e D e s c r i p t i o n F o r N o t e s )Q U��U n ��VWV I  ����X���� *0 addimagefiletoslide addImageFileToSlideX YZY o  ������ 0 thisdocument thisDocumentZ [\[ o  ������ 0 	thisslide 	thisSlide\ ]^] o  ������ 0 thisimagefile thisImageFile^ _`_ m  ����
�� boovfals` a��a m  ����
�� boovfals��  ��  W  f  ����  �� 0 i  F m  vw���� G I w|��b��
�� .corecnte****       ****b o  wx���� (0 exportedimagefiles exportedImageFiles��  ��  D c��c r  ��ded o  ������ 0 	thisslide 	thisSlidee l     f����f n      ghg  ;  ��h o  ������ (0 newslidereferences newSlideReferences��  ��  ��  ; 5  _f��i��
�� 
cappi m  adjj �kk . c o m . a p p l e . i W o r k . K e y n o t e
�� kfrmID   lml l ����������  ��  ��  m non l ����pq��  p "  MOVE EXPORT FOLDER TO TRASH   q �rr 8   M O V E   E X P O R T   F O L D E R   T O   T R A S Ho sts r  ��uvu n ��wxw I  ����������  0 defaultmanager defaultManager��  ��  x n ��yzy o  ������ 0 nsfilemanager NSFileManagerz m  ����
�� misccurav o      ���� 0 filemanager fileManagert {|{ r  ��}~} l ������ n  ����� 1  ����
�� 
psxp� o  ������ 0 thisimagefile thisImageFile��  ��  ~ o      ���� 0 thisposixpath thisPOSIXPath| ��� r  ����� n ����� I  ��������� &0 stringwithstring_ stringWithString_� ���� o  ������ 0 thisposixpath thisPOSIXPath��  ��  � n ����� o  ������ 0 nsstring NSString� m  ����
�� misccura� o      ���� 0 thisposixpath thisPOSIXPath� ��� r  ����� n ����� I  ���������� F0 !stringbydeletinglastpathcomponent !stringByDeletingLastPathComponent��  ��  � o  ������ 0 thisposixpath thisPOSIXPath� o      ���� ,0 pathoffoldertodelete pathOfFolderToDelete� ��� r  ����� m  ����
�� 
msng� o      ���� 0 resultingurl resultingURL� ��� r  ����� l �������� n ����� I  ��������� $0 fileurlwithpath_ fileURLWithPath_� ���� o  ������ ,0 pathoffoldertodelete pathOfFolderToDelete��  ��  � n ����� o  ������ 	0 NSURL  � m  ����
�� misccura��  ��  � o      ���� $0 urlofitemtotrash URLOfItemToTrash� ��� l ������� n ���� I  �������� P0 &trashitematurl_resultingitemurl_error_ &trashItemAtURL_resultingItemURL_error_� ��� o  ������ $0 urlofitemtotrash URLOfItemToTrash� ��� o  ����� 0 resultingurl resultingURL� ���� l ������ m  ��
�� 
msng��  ��  ��  ��  � o  ������ 0 filemanager fileManager��  ��  � ��� l ��������  ��  ��  � ��� l ������  �   POST NOTIFICATION   � ��� $   P O S T   N O T I F I C A T I O N� ��� r  ��� l ������ n ��� I  ������� 40 getlocalizedstringforkey getLocalizedStringForKey� ���� m  �� ��� 4 K E Y N O T E _ C O M P L E T I T I O N _ T I T L E��  ��  �  f  ��  ��  � o      ���� &0 completitiontitle completitionTitle� ��� r   ��� l ������ n ��� I  ������ 40 getlocalizedstringforkey getLocalizedStringForKey� ��~� m  �� ��� 8 K E Y N O T E _ C O M P L E T I T I O N _ M E S S A G E�~  �  �  f  ��  ��  � o      �}�} *0 completitionmessage completitionMessage� ��� I !.�|��
�| .sysonotfnull��� ��� TEXT� o  !$�{�{ *0 completitionmessage completitionMessage� �z��y
�z 
appr� o  '*�x�x &0 completitiontitle completitionTitle�y  � ��� l //�w�v�u�w  �v  �u  � ��� l //�t���t  �   RETURN SLIDE REFERENCES   � ��� 0   R E T U R N   S L I D E   R E F E R E N C E S� ��s� O  /<��� L  9;�� o  9:�r�r (0 newslidereferences newSlideReferences� 5  /6�q��p
�q 
capp� m  14�� ��� . c o m . a p p l e . i W o r k . K e y n o t e
�p kfrmID  �s  y ��� l     �o�n�m�o  �n  �m  � ��� i   z }��� I      �l��k�l 0 newblankslide newBlankSlide� ��j� o      �i�i 0 thisdocument thisDocument�j  �k  � O     {��� k    z�� ��� Z     ���h�g� G    ��� =   ��� o    	�f�f 0 thisdocument thisDocument� m   	 
�e
�e 
msng� =   ��� o    �d�d 0 thisdocument thisDocument� m    �� ���  � r    ��� l   ��c�b� 4   �a�
�a 
docu� m    �`�` �c  �b  � o      �_�_ 0 thisdocument thisDocument�h  �g  � ��� l  ! !�^���^  � . ( add a new blank slide after title slide   � ��� P   a d d   a   n e w   b l a n k   s l i d e   a f t e r   t i t l e   s l i d e� ��]� O   ! z��� k   % y    r   % , l  % *�\�[ n   % * 1   ( *�Z
�Z 
pnam 2   % (�Y
�Y 
KnMs�\  �[   o      �X�X .0 thesemasterslidenames theseMasterSlideNames 	 Z   - <
�W
 E   - 0 o   - .�V�V .0 thesemasterslidenames theseMasterSlideNames m   . / � 
 B l a n k r   3 6 m   3 4�U
�U boovtrue o      �T�T ,0 shoulduseblankmaster shouldUseBlankMaster�W   r   9 < m   9 :�S
�S boovfals o      �R�R ,0 shoulduseblankmaster shouldUseBlankMaster	  Z   = v�Q =  = @ o   = >�P�P ,0 shoulduseblankmaster shouldUseBlankMaster m   > ?�O
�O boovtrue r   C T I  C R�N�M
�N .corecrel****      � null�M   �L 
�L 
kocl m   E F�K
�K 
KnSd  �J!�I
�J 
prdt! K   G N"" �H#�G
�H 
smas# 4   H L�F$
�F 
KnMs$ m   J K%% �&& 
 B l a n k�G  �I   o      �E�E 0 	thisslide 	thisSlide�Q   k   W v'' ()( r   W `*+* I  W ^�D�C,
�D .corecrel****      � null�C  , �B-�A
�B 
kocl- m   Y Z�@
�@ 
KnSd�A  + o      �?�? 0 	thisslide 	thisSlide) ./. l  a a�>01�>  0   clear the slide of items   1 �22 2   c l e a r   t h e   s l i d e   o f   i t e m s/ 343 r   a l565 m   a b�=
�= boovfals6 n      787 1   g k�<
�< 
pLck8 n   b g9:9 2   c g�;
�; 
fmti: o   b c�:�: 0 	thisslide 	thisSlide4 ;�9; I  m v�8<�7
�8 .coredelonull���     obj < n   m r=>= 2   n r�6
�6 
fmti> o   m n�5�5 0 	thisslide 	thisSlide�7  �9   ?�4? L   w y@@ o   w x�3�3 0 	thisslide 	thisSlide�4  � o   ! "�2�2 0 thisdocument thisDocument�]  � 5     �1A�0
�1 
cappA m    BB �CC . c o m . a p p l e . i W o r k . K e y n o t e
�0 kfrmID  � DED l     �/�.�-�/  �.  �-  E FGF i   ~ �HIH I      �,J�+�, *0 addimagefiletoslide addImageFileToSlideJ KLK o      �*�* 0 thisdocument thisDocumentL MNM o      �)�) 0 	thisslide 	thisSlideN OPO o      �(�( 0 thisimagefile thisImageFileP QRQ o      �'�' 0 scaletofill scaleToFillR S�&S o      �%�% <0 shouldusedescriptionfornotes shouldUseDescriptionForNotes�&  �+  I O    _TUT Q   ^VWXV k   2YY Z[Z Z    \]�$�#\ G    ^_^ =   
`a` o    �"�" 0 thisdocument thisDocumenta m    	�!
�! 
msng_ =   bcb o    � �  0 thisdocument thisDocumentc m    dd �ee  ] r    fgf l   h��h 4   �i
� 
docui m    �� �  �  g o      �� 0 thisdocument thisDocument�$  �#  [ jkj I    %���
� .miscactvnull��� ��� null�  �  k lml r   & 0non n  & .pqp I   ' .�r�� (0 getimagedimensions getImageDimensionsr s�s n   ' *tut 1   ( *�
� 
psxpu o   ' (�� 0 thisimagefile thisImageFile�  �  q  f   & 'o o      �� 0 queryresult queryResultm vwv Z   1 Rxy�zx =  1 4{|{ o   1 2�� 0 queryresult queryResult| m   2 3�
� boovfalsy R   7 =��}
� .ascrerr ****      � ****�  } �~�
� 
errn~ m   9 :�
�
'�  �  z s   @ R� o   @ A�	�	 0 queryresult queryResult� J      �� ��� o      �� 0 
imagewidth 
imageWidth� ��� o      �� 0 imageheight imageHeight�  w ��� O   S c��� k   W b�� ��� r   W \��� n  W Z��� 1   X Z�
� 
sitw�  g   W X� o      �� 0 documentwidth documentWidth� ��� r   ] b��� n  ] `��� 1   ^ `�
� 
sith�  g   ] ^� o      ��  0 documentheight documentHeight�  � o   S T� �  0 thisdocument thisDocument� ���� O   d2��� k   h1�� ��� Z   h������ =  h k��� o   h i���� 0 scaletofill scaleToFill� m   i j��
�� boovfals� k   n ��� ��� r   n ���� I  n ~�����
�� .corecrel****      � null��  � ����
�� 
kocl� m   p q��
�� 
imag� �����
�� 
prdt� K   r x�� �����
�� 
file� o   u v���� 0 thisimagefile thisImageFile��  ��  � o      ���� 0 	thisimage 	thisImage� ���� O   � ���� k   � ��� ��� r   � ���� n  � ���� 1   � ���
�� 
sitw�  g   � �� o      ����  0 thisimagewidth thisImageWidth� ��� r   � ���� n  � ���� 1   � ���
�� 
sith�  g   � �� o      ���� "0 thisimageheight thisImageHeight� ��� l  � �������  �   center image   � ���    c e n t e r   i m a g e� ���� r   � ���� l 	 � ������� J   � ��� ��� ^   � ���� l  � ������� \   � ���� o   � ����� 0 documentwidth documentWidth� o   � �����  0 thisimagewidth thisImageWidth��  ��  � l 
 � ������� m   � ����� ��  ��  � ���� ^   � ���� l  � ������� \   � ���� o   � �����  0 documentheight documentHeight� o   � ����� "0 thisimageheight thisImageHeight��  ��  � m   � ����� ��  ��  ��  � n      ��� 1   � ���
�� 
sipo�  g   � ���  � o   � ����� 0 	thisimage 	thisImage��  ��  � l  ����� k   ��� ��� l  � �������  � N H figure new image height based on setting new image width to slide width   � ��� �   f i g u r e   n e w   i m a g e   h e i g h t   b a s e d   o n   s e t t i n g   n e w   i m a g e   w i d t h   t o   s l i d e   w i d t h� ��� r   � ���� ^   � ���� l  � ������� ]   � ���� o   � ����� 0 imageheight imageHeight� o   � ����� 0 documentwidth documentWidth��  ��  � o   � ����� 0 
imagewidth 
imageWidth� o      ����  0 newimageheight newImageHeight� ��� l  � �������  � S M check to see if new image height is greater than or equal to document height   � ��� �   c h e c k   t o   s e e   i f   n e w   i m a g e   h e i g h t   i s   g r e a t e r   t h a n   o r   e q u a l   t o   d o c u m e n t   h e i g h t� ��� Z   � ������� @  � ���� o   � �����  0 newimageheight newImageHeight� o   � �����  0 documentheight documentHeight� k   � ��� ��� r   � ���� o   � ����� 0 documentwidth documentWidth� o      ���� 0 newimagewidth newImageWidth� ��� l  � �������  �   center image vertically   � ��� 0   c e n t e r   i m a g e   v e r t i c a l l y� ��� r   � �� � ]   � � l  � ����� ^   � � l  � ����� \   � � o   � �����  0 newimageheight newImageHeight o   � �����  0 documentheight documentHeight��  ��   m   � ����� ��  ��   m   � �������  o      ����  0 verticaloffset verticalOffset� 	��	 r   � �

 m   � �����   o      ���� $0 horizontaloffset horizontalOffset��  ��  � k   � �  l  � �����   / ) scale image height to match slide height    � R   s c a l e   i m a g e   h e i g h t   t o   m a t c h   s l i d e   h e i g h t  r   � � o   � �����  0 documentheight documentHeight o      ����  0 newimageheight newImageHeight  r   � � ^   � � l  � ����� ]   � � o   � �����  0 documentheight documentHeight o   � ����� 0 
imagewidth 
imageWidth��  ��   o   � ����� 0 imageheight imageHeight o      ���� 0 newimagewidth newImageWidth   l  � ���!"��  !    center image horizontally   " �## 4   c e n t e r   i m a g e   h o r i z o n t a l l y  $%$ r   � �&'& m   � �����  ' o      ����  0 verticaloffset verticalOffset% (��( r   � �)*) ]   � �+,+ l  � �-����- ^   � �./. l  � �0����0 \   � �121 o   � ����� 0 newimagewidth newImageWidth2 o   � ����� 0 documentwidth documentWidth��  ��  / m   � ����� ��  ��  , m   � �������* o      ���� $0 horizontaloffset horizontalOffset��  � 343 l  � ���56��  5 ( " import, scale, and position image   6 �77 D   i m p o r t ,   s c a l e ,   a n d   p o s i t i o n   i m a g e4 8��8 r   �9:9 I  �����;
�� .corecrel****      � null��  ; ��<=
�� 
kocl< m   � ���
�� 
imag= ��>��
�� 
prdt> l 	 � ?����? K   � @@ ��AB
�� 
fileA l 
 � �C����C o   � ����� 0 thisimagefile thisImageFile��  ��  B ��DE
�� 
sitwD l 
 � �F����F o   � ����� 0 newimagewidth newImageWidth��  ��  E ��GH
�� 
sithG l 
 � �I����I o   � �����  0 newimageheight newImageHeight��  ��  H ��J��
�� 
sipoJ J   � �KK LML o   � ����� $0 horizontaloffset horizontalOffsetM N��N o   � �����  0 verticaloffset verticalOffset��  ��  ��  ��  ��  : o      ���� 0 	thisimage 	thisImage��  �   scale to fill   � �OO    s c a l e   t o   f i l l� PQP r  	RSR n 	TUT I  
��V���� 60 extractkmditemdescription extractkMDItemDescriptionV W��W o  
���� 0 thisimagefile thisImageFile��  ��  U  f  	
S o      ���� "0 thisdescription thisDescriptionQ XYX r  Z[Z o  ���� "0 thisdescription thisDescription[ n      \]\ 1  ��
�� 
dscr] o  ���� 0 	thisimage 	thisImageY ^��^ Z  1_`����_ = !aba o  ���� <0 shouldusedescriptionfornotes shouldUseDescriptionForNotesb m   ��
�� boovtrue` r  $-cdc o  $'���� "0 thisdescription thisDescriptiond n      efe 1  (,�
� 
ksntf  g  '(��  ��  ��  � o   d e�~�~ 0 	thisslide 	thisSlide��  W R      �}gh
�} .ascrerr ****      � ****g o      �|�| 0 errormessage errorMessageh �{i�z
�{ 
errni o      �y�y 0 errornumber errorNumber�z  X k  :^jj klk Z  :Umn�x�wm > :Aopo o  :=�v�v 0 errornumber errorNumberp m  =@�u�u��n I DQ�tqr
�t .sysodisAaleR        TEXTq o  DG�s�s 0 errornumber errorNumberr �rs�q
�r 
mesSs o  JM�p�p 0 errormessage errorMessage�q  �x  �w  l t�ot R  V^�n�mu
�n .ascrerr ****      � ****�m  u �lv�k
�l 
errnv m  X[�j�j���k  �o  U m     ww�                                                                                  keyn  alis    &  Macintosh HD                   BD ����Keynote.app                                                    ����            ����  
 cu             Applications  /:Applications:Keynote.app/     K e y n o t e . a p p    M a c i n t o s h   H D  Applications/Keynote.app  / ��  G xyx l     �i�h�g�i  �h  �g  y z{z i   � �|}| I      �f~�e�f (0 getimagedimensions getImageDimensions~ �d o      �c�c 00 thisimagefileposixpath thisImageFilePOSIXPath�d  �e  } Q     ����� k    ��� ��� I   �b��a
�b .sysoexecTEXT���     TEXT� b    ��� m    �� ���  m d i m p o r t  � l   ��`�_� n    ��� 1    �^
�^ 
strq� o    �]�] 00 thisimagefileposixpath thisImageFilePOSIXPath�`  �_  �a  � ��� r    ��� l   ��\�[� I   �Z��Y
�Z .sysoexecTEXT���     TEXT� b    ��� b    ��� m    �� ��� D m d l s   - r a w   - n a m e   k M D I t e m P i x e l H e i g h t� 1    �X
�X 
spac� n    ��� 1    �W
�W 
strq� o    �V�V 00 thisimagefileposixpath thisImageFilePOSIXPath�Y  �\  �[  � o      �U�U 0 imageheight imageHeight� ��� r    (��� l   &��T�S� I   &�R��Q
�R .sysoexecTEXT���     TEXT� b    "��� b    ��� m    �� ��� B m d l s   - r a w   - n a m e   k M D I t e m P i x e l W i d t h� 1    �P
�P 
spac� n    !��� 1    !�O
�O 
strq� o    �N�N 00 thisimagefileposixpath thisImageFilePOSIXPath�Q  �T  �S  � o      �M�M 0 
imagewidth 
imageWidth� ��� r   ) /��� J   ) -�� ��� o   ) *�L�L 0 
imagewidth 
imageWidth� ��K� o   * +�J�J 0 imageheight imageHeight�K  � o      �I�I *0 imagedimensionsdata imageDimensionsData� ��� Z   0 ����H�G� E   0 3��� o   0 1�F�F *0 imagedimensionsdata imageDimensionsData� m   1 2�� ���  ( n u l l )� O   6 ���� k   : ��� ��� I  : ?�E�D�C
�E .ascrnoop****      � ****�D  �C  � ��B� Q   @ ����� k   C e�� ��� r   C J��� I  C H�A��@
�A .aevtodocnull  �    alis� o   C D�?�? 00 thisimagefileposixpath thisImageFilePOSIXPath�@  � o      �>�> 0 	thisimage 	thisImage� ��� s   K _��� n   K N��� 1   L N�=
�= 
dmns� o   K L�<�< 0 	thisimage 	thisImage� J      �� ��� o      �;�; 0 
imagewidth 
imageWidth� ��:� o      �9�9 0 imageheight imageHeight�:  � ��8� I  ` e�7��6
�7 .coreclosnull���     obj � o   ` a�5�5 0 	thisimage 	thisImage�6  �8  � R      �4�3�2
�4 .ascrerr ****      � ****�3  �2  � k   m ��� ��� Q   m ~���1� I  p u�0��/
�0 .coreclosnull���     obj � o   p q�.�. 0 	thisimage 	thisImage�/  � R      �-�,�+
�- .ascrerr ****      � ****�,  �+  �1  � ��*� R    ��)��(
�) .ascrerr ****      � ****� m   � ��� ���  p r o b l e m   r e a d i n g�(  �*  �B  � m   6 7���                                                                                  imev  alis    X  Macintosh HD                   BD ����Image Events.app                                               ����            ����  
 cu             CoreServices  //:System:Library:CoreServices:Image Events.app/   "  I m a g e   E v e n t s . a p p    M a c i n t o s h   H D  ,System/Library/CoreServices/Image Events.app  / ��  �H  �G  � ��'� L   � ��� J   � ��� ��� o   � ��&�& 0 
imagewidth 
imageWidth� ��%� o   � ��$�$ 0 imageheight imageHeight�%  �'  � R      �#�"�!
�# .ascrerr ****      � ****�"  �!  � L   � ��� m   � �� 
�  boovfals{ ��� l     ����  �  �  � ��� i   � ���� I      ���� 60 extractkmditemdescription extractkMDItemDescription� ��� o      �� 0 thisimagefile thisImageFile�  �  � k     9�� ��� l     ����  � H B uses Spotlight to retrieve embedded description metadata (if any)   � ��� �   u s e s   S p o t l i g h t   t o   r e t r i e v e   e m b e d d e d   d e s c r i p t i o n   m e t a d a t a   ( i f   a n y )� ��� Q     9���� k    /�� ��� r    ��� l   ���� n    � � 1    �
� 
psxp  o    �� 0 thisimagefile thisImageFile�  �  � o      �� (0 thisimageposixpath thisImagePOSIXPath�  I  	 ��
� .sysoexecTEXT���     TEXT b   	  m   	 
 �  m d i m p o r t   l  
 �� n   
 	
	 1    �
� 
strq
 o   
 �� (0 thisimageposixpath thisImagePOSIXPath�  �  �    r     I   ��

� .sysoexecTEXT���     TEXT b     m     � F m d l s   - r a w   - n a m e   k M D I t e m D e s c r i p t i o n   l   �	� n     1    �
� 
strq o    �� (0 thisimageposixpath thisImagePOSIXPath�	  �  �
   l     �� o      �� *0 embeddeddescription embeddedDescription�  �    Z    ,�� =   " o     � �  *0 embeddeddescription embeddedDescription m     ! �  ( n u l l ) r   % ( !  m   % &"" �##  ! o      ���� *0 embeddeddescription embeddedDescription�  �   $��$ L   - /%% o   - .���� *0 embeddeddescription embeddedDescription��  � R      ������
�� .ascrerr ****      � ****��  ��  � L   7 9&& m   7 8'' �((  �  � )*) l     ��������  ��  ��  * +,+ i   � �-.- I     ����/
�� .PEXPNUXPnull��� ��� null��  / ��01
�� 
EXUS0 o      ���� "0 thesemediaitems theseMediaItems1 ��2��
�� 
NDOC2 |����3��4��  ��  3 o      ���� 0 templatename templateName��  4 m      ��
�� 
msng��  . k    �55 676 Z    89����8 >    :;: l    <����< n     =>= m    ��
�� 
pcls> o     ���� "0 thesemediaitems theseMediaItems��  ��  ; m    ��
�� 
list9 r    ?@? c    ABA o    	���� "0 thesemediaitems theseMediaItemsB m   	 
��
�� 
list@ o      ���� "0 thesemediaitems theseMediaItems��  ��  7 CDC Z   &EF����E =   GHG o    ���� "0 thesemediaitems theseMediaItemsH J    ����  F R    "��I��
�� .ascrerr ****      � ****I l   !J����J n   !KLK I    !��M���� 40 getlocalizedstringforkey getLocalizedStringForKeyM N��N m    OO �PP . N O _ M E D I A _ I T E M S _ P R O V I D E D��  ��  L  f    ��  ��  ��  ��  ��  D QRQ l  ' '��ST��  S V P Export images with IDs for file names. Enables linking back to Photos library.    T �UU �   E x p o r t   i m a g e s   w i t h   I D s   f o r   f i l e   n a m e s .   E n a b l e s   l i n k i n g   b a c k   t o   P h o t o s   l i b r a r y .  R VWV r   ' 7XYX I   ' 5��Z���� 0 quickexport quickExportZ [\[ o   ( )���� "0 thesemediaitems theseMediaItems\ ]^] m   ) *��
�� boovtrue^ _`_ m   * +��
�� boovfals` aba m   + ,���� b cdc m   , -��
�� boovfalsd efe m   - .gg �hh  f iji m   . /kk �ll  j mnm m   / 0���� n o��o m   0 1���� ��  ��  Y o      ���� (0 exportedimagefiles exportedImageFilesW pqp l  8 8��rs��  r - ' Make a new Numbers document if needed.   s �tt N   M a k e   a   n e w   N u m b e r s   d o c u m e n t   i f   n e e d e d .q uvu O   8 �wxw k   @ �yy z{z I  @ E������
�� .miscactvnull��� ��� null��  ��  { |��| Z   F �}~����} G   F W� l  F O������ H   F O�� l  F N������ I  F N�����
�� .coredoexnull���     ****� 4   F J���
�� 
docu� m   H I���� ��  ��  ��  ��  ��  � l  R U������ >  R U��� o   R S���� 0 templatename templateName� m   S T��
�� 
msng��  ��  ~ Z   Z ������� >  Z ]��� o   Z [���� 0 templatename templateName� m   [ \��
�� 
msng� Z   ` ������� =  ` e��� o   ` a���� 0 templatename templateName� m   a d�� ���  � r   h s��� I  h q�����
�� .corecrel****      � null��  � �����
�� 
kocl� m   l m��
�� 
docu��  � o      ���� 0 thisdocument thisDocument��  � k   v ��� ��� r   v ���� l  v ������ n   v ��� 1   { ��
�� 
pnam� 2   v {��
�� 
tmpl��  ��  � l     ������ o      ���� 0 templatenames templateNames��  ��  � ���� Z   � ������� E  � ���� o   � ����� 0 templatenames templateNames� o   � ����� 0 templatename templateName� r   � ���� I  � ������
�� .corecrel****      � null��  � ����
�� 
kocl� m   � ���
�� 
docu� �����
�� 
prdt� K   � ��� �����
�� 
Tmpl� 4   � ����
�� 
tmpl� o   � ����� 0 templatename templateName��  ��  � o      ���� 0 thisdocument thisDocument��  � R   � ������
�� .ascrerr ****      � ****� b   � ���� l  � ������� n  � ���� I   � �������� 40 getlocalizedstringforkey getLocalizedStringForKey� ���� m   � ��� ��� , N U M B E R S _ T E M P L A T E _ E R R O R��  ��  �  f   � ���  ��  � o   � ����� 0 templatename templateName��  ��  ��  � r   � ���� I  � ������
�� .corecrel****      � null��  � �����
�� 
kocl� m   � ���
�� 
docu��  � o      ���� 0 thisdocument thisDocument��  ��  ��  x 5   8 =�����
�� 
capp� m   : ;�� ��� . c o m . a p p l e . i W o r k . N u m b e r s
�� kfrmID  v ��� O   �T��� k   �S�� ��� r   � ���� J   � �����  � o      ���� "0 imagereferences imageReferences� ���� O   �S��� O   �R��� Y   �Q�������� k   �L�� ��� r   � ���� n   � ���� 4   � ����
�� 
cobj� o   � ����� 0 i  � o   � ��� (0 exportedimagefiles exportedImageFiles� o      �~�~ 0 thisimagefile thisImageFile� ��� r   ���� I  ��}�|�
�} .corecrel****      � null�|  � �{��
�{ 
kocl� m   �z
�z 
imag� �y��x
�y 
prdt� K  �� �w��v
�w 
file� o  	
�u�u 0 thisimagefile thisImageFile�v  �x  � o      �t�t 0 	thisimage 	thisImage� ��� O  G��� k  F�� ��� r  $��� o  �s�s &0 defaultimagewidth defaultImageWidth� 1  #�r
�r 
sitw� ��� r  %/��� J  %)�� ��� m  %&�q�q  � ��p� m  &'�o�o  �p  � 1  ).�n
�n 
sipo� ��� r  09��� m  03�m�m d� 1  38�l
�l 
pSOp� ��k� r  :F��� n :@��� I  ;@�j��i�j 60 extractkmditemdescription extractkMDItemDescription�  �h  o  ;<�g�g 0 thisimagefile thisImageFile�h  �i  �  f  :;� n       1  AE�f
�f 
dscr  g  @A�k  � o  �e�e 0 	thisimage 	thisImage� �d r  HL o  HI�c�c 0 	thisimage 	thisImage l     �b�a n        ;  JK o  IJ�`�` "0 imagereferences imageReferences�b  �a  �d  �� 0 i  � m   � ��_�_ � l  � �	�^�]	 I  � ��\
�[
�\ .corecnte****       ****
 o   � ��Z�Z (0 exportedimagefiles exportedImageFiles�[  �^  �]  ��  � 1   � ��Y
�Y 
NmAS� 4  � ��X
�X 
docu m   � ��W�W ��  � 5   � ��V�U
�V 
capp m   � � � . c o m . a p p l e . i W o r k . N u m b e r s
�U kfrmID  �  l UU�T�T   "  MOVE EXPORT FOLDER TO TRASH    � 8   M O V E   E X P O R T   F O L D E R   T O   T R A S H  r  Ub n U` I  \`�S�R�Q�S  0 defaultmanager defaultManager�R  �Q   n U\ o  X\�P�P 0 nsfilemanager NSFileManager m  UX�O
�O misccura o      �N�N 0 filemanager fileManager  r  cj l ch �M�L  n  ch!"! 1  dh�K
�K 
psxp" o  cd�J�J 0 thisimagefile thisImageFile�M  �L   o      �I�I 0 thisposixpath thisPOSIXPath #$# r  ky%&% n kw'(' I  rw�H)�G�H &0 stringwithstring_ stringWithString_) *�F* o  rs�E�E 0 thisposixpath thisPOSIXPath�F  �G  ( n kr+,+ o  nr�D�D 0 nsstring NSString, m  kn�C
�C misccura& o      �B�B 0 thisposixpath thisPOSIXPath$ -.- r  z�/0/ n z121 I  {�A�@�?�A F0 !stringbydeletinglastpathcomponent !stringByDeletingLastPathComponent�@  �?  2 o  z{�>�> 0 thisposixpath thisPOSIXPath0 o      �=�= ,0 pathoffoldertodelete pathOfFolderToDelete. 343 r  ��565 m  ���<
�< 
msng6 o      �;�; 0 resultingurl resultingURL4 787 r  ��9:9 l ��;�:�9; n ��<=< I  ���8>�7�8 $0 fileurlwithpath_ fileURLWithPath_> ?�6? o  ���5�5 ,0 pathoffoldertodelete pathOfFolderToDelete�6  �7  = n ��@A@ o  ���4�4 	0 NSURL  A m  ���3
�3 misccura�:  �9  : o      �2�2 $0 urlofitemtotrash URLOfItemToTrash8 BCB l ��D�1�0D n ��EFE I  ���/G�.�/ P0 &trashitematurl_resultingitemurl_error_ &trashItemAtURL_resultingItemURL_error_G HIH o  ���-�- $0 urlofitemtotrash URLOfItemToTrashI JKJ o  ���,�, 0 resultingurl resultingURLK L�+L l ��M�*�)M m  ���(
�( 
msng�*  �)  �+  �.  F o  ���'�' 0 filemanager fileManager�1  �0  C NON l ���&PQ�&  P   POST NOTIFICATION   Q �RR $   P O S T   N O T I F I C A T I O NO STS r  ��UVU l ��W�%�$W n ��XYX I  ���#Z�"�# 40 getlocalizedstringforkey getLocalizedStringForKeyZ [�![ m  ��\\ �]] 4 N U M B E R S _ C O M P L E T I T I O N _ T I T L E�!  �"  Y  f  ���%  �$  V o      � �  &0 completitiontitle completitionTitleT ^_^ r  ��`a` l ��b��b n ��cdc I  ���e�� 40 getlocalizedstringforkey getLocalizedStringForKeye f�f m  ��gg �hh 8 N U M B E R S _ C O M P L E T I T I O N _ M E S S A G E�  �  d  f  ���  �  a o      �� *0 completitionmessage completitionMessage_ iji I ���kl
� .sysonotfnull��� ��� TEXTk o  ���� *0 completitionmessage completitionMessagel �m�
� 
apprm o  ���� &0 completitiontitle completitionTitle�  j non l ���pq�  p   RETURN SLIDE REFERENCES   q �rr 0   R E T U R N   S L I D E   R E F E R E N C E So s�s O  ��tut L  ��vv o  ���� "0 imagereferences imageReferencesu 5  ���w�
� 
cappw m  ��xx �yy . c o m . a p p l e . i W o r k . N u m b e r s
� kfrmID  �  , z{z l     ����  �  �  { |}| i   � �~~ I     ���
� .PEXPPGXPnull��� ��� null�  � �
��
�
 
EXUS� o      �	�	 "0 thesemediaitems theseMediaItems� ���
� 
NDOC� |������  �  � o      �� 0 templatename templateName�  � m      �
� 
msng�   k    ��� ��� Z    ���� � >    ��� l    ������ n     ��� m    ��
�� 
pcls� o     ���� "0 thesemediaitems theseMediaItems��  ��  � m    ��
�� 
list� r    ��� c    ��� o    	���� "0 thesemediaitems theseMediaItems� m   	 
��
�� 
list� o      ���� "0 thesemediaitems theseMediaItems�  �   � ��� Z   &������� =   ��� o    ���� "0 thesemediaitems theseMediaItems� J    ����  � R    "�����
�� .ascrerr ****      � ****� l   !������ n   !��� I    !������� 40 getlocalizedstringforkey getLocalizedStringForKey� ���� m    �� ��� . N O _ M E D I A _ I T E M S _ P R O V I D E D��  ��  �  f    ��  ��  ��  ��  ��  � ��� l  ' '������  � V P Export images with IDs for file names. Enables linking back to Photos library.    � ��� �   E x p o r t   i m a g e s   w i t h   I D s   f o r   f i l e   n a m e s .   E n a b l e s   l i n k i n g   b a c k   t o   P h o t o s   l i b r a r y .  � ��� r   ' 7��� I   ' 5������� 0 quickexport quickExport� ��� o   ( )���� "0 thesemediaitems theseMediaItems� ��� m   ) *��
�� boovtrue� ��� m   * +��
�� boovfals� ��� m   + ,���� � ��� m   , -��
�� boovfals� ��� m   - .�� ���  � ��� m   . /�� ���  � ��� m   / 0���� � ���� m   0 1���� ��  ��  � o      ���� (0 exportedimagefiles exportedImageFiles� ��� l  8 8������  � - ' Make a new Numbers document if needed.   � ��� N   M a k e   a   n e w   N u m b e r s   d o c u m e n t   i f   n e e d e d .� ��� O   8 ���� k   @ ��� ��� I  @ E������
�� .miscactvnull��� ��� null��  ��  � ���� Z   F �������� G   F W��� l  F O������ H   F O�� l  F N������ I  F N�����
�� .coredoexnull���     ****� 4   F J���
�� 
docu� m   H I���� ��  ��  ��  ��  ��  � l  R U������ >  R U��� o   R S���� 0 templatename templateName� m   S T��
�� 
msng��  ��  � Z   Z ������� >  Z ]��� o   Z [���� 0 templatename templateName� m   [ \��
�� 
msng� Z   ` ������� =  ` e��� o   ` a���� 0 templatename templateName� m   a d�� ���  � r   h s��� I  h q�����
�� .corecrel****      � null��  � �����
�� 
kocl� m   l m��
�� 
docu��  � o      ���� 0 thisdocument thisDocument��  � k   v ��� ��� r   v ���� l  v ������ n   v ��� 1   { ��
�� 
pnam� 2   v {��
�� 
tmpl��  ��  � l     ������ o      ���� 0 templatenames templateNames��  ��  � ���� Z   � ������� E  � ���� o   � ����� 0 templatenames templateNames� o   � ����� 0 templatename templateName� r   � ���� I  � ������
�� .corecrel****      � null��  � ����
�� 
kocl� m   � ���
�� 
docu� �����
�� 
prdt� K   � ��� ��	 ��
�� 
Tmpl	  4   � ���	
�� 
tmpl	 o   � ����� 0 templatename templateName��  ��  � o      ���� 0 thisdocument thisDocument��  � R   � ���	��
�� .ascrerr ****      � ****	 b   � �			 l  � �	����	 n  � �			 I   � ���	���� 40 getlocalizedstringforkey getLocalizedStringForKey	 		��		 m   � �	
	
 �		 ( P A G E S _ T E M P L A T E _ E R R O R��  ��  	  f   � ���  ��  	 o   � ����� 0 templatename templateName��  ��  ��  � r   � �			 I  � �����	
�� .corecrel****      � null��  	 ��	��
�� 
kocl	 m   � ���
�� 
docu��  	 o      ���� 0 thisdocument thisDocument��  ��  ��  � 5   8 =��	��
�� 
capp	 m   : ;		 �		 * c o m . a p p l e . i W o r k . P a g e s
�� kfrmID  � 			 O   �T			 k   �S		 			 r   � �			 J   � �����  	 o      ���� "0 imagereferences imageReferences	 	��	 O   �S			 O   �R		 	 Y   �Q	!��	"	#��	! k   �L	$	$ 	%	&	% r   � �	'	(	' n   � �	)	*	) 4   � ���	+
�� 
cobj	+ o   � ����� 0 i  	* o   � ����� (0 exportedimagefiles exportedImageFiles	( o      ���� 0 thisimagefile thisImageFile	& 	,	-	, r   �	.	/	. I  �����	0
�� .corecrel****      � null��  	0 ��	1	2
�� 
kocl	1 m   ��
�� 
imag	2 ��	3��
�� 
prdt	3 K  	4	4 ��	5��
�� 
file	5 o  	
���� 0 thisimagefile thisImageFile��  ��  	/ o      ���� 0 	thisimage 	thisImage	- 	6	7	6 O  G	8	9	8 k  F	:	: 	;	<	; r  $	=	>	= o  ���� &0 defaultimagewidth defaultImageWidth	> 1  #��
�� 
sitw	< 	?	@	? r  %/	A	B	A J  %)	C	C 	D	E	D m  %&����  	E 	F��	F m  &'����  ��  	B 1  ).��
�� 
sipo	@ 	G	H	G r  09	I	J	I m  03���� d	J 1  38��
�� 
pSOp	H 	K��	K r  :F	L	M	L n :@	N	O	N I  ;@��	P��� 60 extractkmditemdescription extractkMDItemDescription	P 	Q�~	Q o  ;<�}�} 0 thisimagefile thisImageFile�~  �  	O  f  :;	M n      	R	S	R 1  AE�|
�| 
dscr	S  g  @A��  	9 o  �{�{ 0 	thisimage 	thisImage	7 	T�z	T r  HL	U	V	U o  HI�y�y 0 	thisimage 	thisImage	V l     	W�x�w	W n      	X	Y	X  ;  JK	Y o  IJ�v�v "0 imagereferences imageReferences�x  �w  �z  �� 0 i  	" m   � ��u�u 	# l  � �	Z�t�s	Z I  � ��r	[�q
�r .corecnte****       ****	[ o   � ��p�p (0 exportedimagefiles exportedImageFiles�q  �t  �s  ��  	  1   � ��o
�o 
pCpa	 4  � ��n	\
�n 
docu	\ m   � ��m�m ��  	 5   � ��l	]�k
�l 
capp	] m   � �	^	^ �	_	_ * c o m . a p p l e . i W o r k . P a g e s
�k kfrmID  	 	`	a	` l UU�j	b	c�j  	b "  MOVE EXPORT FOLDER TO TRASH   	c �	d	d 8   M O V E   E X P O R T   F O L D E R   T O   T R A S H	a 	e	f	e r  Ub	g	h	g n U`	i	j	i I  \`�i�h�g�i  0 defaultmanager defaultManager�h  �g  	j n U\	k	l	k o  X\�f�f 0 nsfilemanager NSFileManager	l m  UX�e
�e misccura	h o      �d�d 0 filemanager fileManager	f 	m	n	m r  cj	o	p	o l ch	q�c�b	q n  ch	r	s	r 1  dh�a
�a 
psxp	s o  cd�`�` 0 thisimagefile thisImageFile�c  �b  	p o      �_�_ 0 thisposixpath thisPOSIXPath	n 	t	u	t r  ky	v	w	v n kw	x	y	x I  rw�^	z�]�^ &0 stringwithstring_ stringWithString_	z 	{�\	{ o  rs�[�[ 0 thisposixpath thisPOSIXPath�\  �]  	y n kr	|	}	| o  nr�Z�Z 0 nsstring NSString	} m  kn�Y
�Y misccura	w o      �X�X 0 thisposixpath thisPOSIXPath	u 	~		~ r  z�	�	�	� n z	�	�	� I  {�W�V�U�W F0 !stringbydeletinglastpathcomponent !stringByDeletingLastPathComponent�V  �U  	� o  z{�T�T 0 thisposixpath thisPOSIXPath	� o      �S�S ,0 pathoffoldertodelete pathOfFolderToDelete	 	�	�	� r  ��	�	�	� m  ���R
�R 
msng	� o      �Q�Q 0 resultingurl resultingURL	� 	�	�	� r  ��	�	�	� l ��	��P�O	� n ��	�	�	� I  ���N	��M�N $0 fileurlwithpath_ fileURLWithPath_	� 	��L	� o  ���K�K ,0 pathoffoldertodelete pathOfFolderToDelete�L  �M  	� n ��	�	�	� o  ���J�J 	0 NSURL  	� m  ���I
�I misccura�P  �O  	� o      �H�H $0 urlofitemtotrash URLOfItemToTrash	� 	�	�	� l ��	��G�F	� n ��	�	�	� I  ���E	��D�E P0 &trashitematurl_resultingitemurl_error_ &trashItemAtURL_resultingItemURL_error_	� 	�	�	� o  ���C�C $0 urlofitemtotrash URLOfItemToTrash	� 	�	�	� o  ���B�B 0 resultingurl resultingURL	� 	��A	� l ��	��@�?	� m  ���>
�> 
msng�@  �?  �A  �D  	� o  ���=�= 0 filemanager fileManager�G  �F  	� 	�	�	� l ���<	�	��<  	�   POST NOTIFICATION   	� �	�	� $   P O S T   N O T I F I C A T I O N	� 	�	�	� r  ��	�	�	� l ��	��;�:	� n ��	�	�	� I  ���9	��8�9 40 getlocalizedstringforkey getLocalizedStringForKey	� 	��7	� m  ��	�	� �	�	� 0 P A G E S _ C O M P L E T I T I O N _ T I T L E�7  �8  	�  f  ���;  �:  	� o      �6�6 &0 completitiontitle completitionTitle	� 	�	�	� r  ��	�	�	� l ��	��5�4	� n ��	�	�	� I  ���3	��2�3 40 getlocalizedstringforkey getLocalizedStringForKey	� 	��1	� m  ��	�	� �	�	� 4 P A G E S _ C O M P L E T I T I O N _ M E S S A G E�1  �2  	�  f  ���5  �4  	� o      �0�0 *0 completitionmessage completitionMessage	� 	�	�	� I ���/	�	�
�/ .sysonotfnull��� ��� TEXT	� o  ���.�. *0 completitionmessage completitionMessage	� �-	��,
�- 
appr	� o  ���+�+ &0 completitiontitle completitionTitle�,  	� 	�	�	� l ���*	�	��*  	�   RETURN SLIDE REFERENCES   	� �	�	� 0   R E T U R N   S L I D E   R E F E R E N C E S	� 	��)	� O  ��	�	�	� L  ��	�	� o  ���(�( "0 imagereferences imageReferences	� 5  ���'	��&
�' 
capp	� m  ��	�	� �	�	� * c o m . a p p l e . i W o r k . P a g e s
�& kfrmID  �)  } 	�	�	� l     �%�$�#�%  �$  �#  	� 	�	�	� l      �"	�	��"  	�   SUPPORT ROUTINES    	� �	�	� $   S U P P O R T   R O U T I N E S  	� 	�	�	� l     �!� ��!  �   �  	� 	�	�	� i   � �	�	�	� I      �	��� 40 getnamesofspecifieditems getNamesOfSpecifiedItems	� 	��	� o      �� 0 
theseitems 
theseItems�  �  	� k     0	�	� 	�	�	� I     �	��� 0 waitforphotos waitForPhotos	� 	��	� m    ��,�  �  	� 	��	� O    0	�	�	� k    /	�	� 	�	�	� r    	�	�	� J    ��  	� o      �� 0 
thesenames 
theseNames	� 	�	�	� Y    ,	��	�	��	� r    '	�	�	� l   $	���	� n    $	�	�	� 1   " $�
� 
pnam	� l   "	���	� n    "	�	�	� 4    "�	�
� 
cobj	� o     !�� 0 i  	� o    �
�
 0 
theseitems 
theseItems�  �  �  �  	� l     	��	�	� n      	�	�	�  ;   % &	� o   $ %�� 0 
thesenames 
theseNames�	  �  � 0 i  	� m    �� 	� l   	���	� I   �	��
� .corecnte****       ****	� o    �� 0 
theseitems 
theseItems�  �  �  �  	� 	�� 	� L   - /	�	� o   - .���� 0 
thesenames 
theseNames�   	� m    	�	��                                                                                  Phts  alis    0  Macintosh HD                   BD ����
Photos.app                                                     ����            ����  
 cu             Applications  !/:System:Applications:Photos.app/    
 P h o t o s . a p p    M a c i n t o s h   H D  System/Applications/Photos.app  / ��  �  	� 	�	�	� l     ��������  ��  ��  	� 	�
 	� i   � �


 I      �������� &0 gettoplevelalbums getTopLevelAlbums��  ��  
 k     D

 


 I     ��
���� 0 waitforphotos waitForPhotos
 
��
 m    ����,��  ��  
 
��
 O    D
	


	 k    C

 


 r    


 2    ��
�� 
IPal
 o      ���� 0 thesealbums theseAlbums
 


 r    


 J    ����  
 o      ����  0 toplevelalbums topLevelAlbums
 


 Y    @
��

��
 k   $ ;

 


 r   $ *


 n   $ (


 4   % (��
 
�� 
cobj
  o   & '���� 0 i  
 o   $ %���� 0 thesealbums theseAlbums
 o      ���� 0 	thisalbum 	thisAlbum
 
!��
! Z   + ;
"
#����
" =  + 0
$
%
$ l  + .
&����
& n   + .
'
(
' 1   , .��
�� 
pare
( o   + ,���� 0 	thisalbum 	thisAlbum��  ��  
% m   . /��
�� 
msng
# r   3 7
)
*
) o   3 4���� 0 	thisalbum 	thisAlbum
* l     
+����
+ n      
,
-
,  ;   5 6
- o   4 5����  0 toplevelalbums topLevelAlbums��  ��  ��  ��  ��  �� 0 i  
 m    ���� 
 l   
.����
. I   ��
/��
�� .corecnte****       ****
/ o    ���� 0 thesealbums theseAlbums��  ��  ��  ��  
 
0��
0 L   A C
1
1 o   A B����  0 toplevelalbums topLevelAlbums��  

 m    
2
2�                                                                                  Phts  alis    0  Macintosh HD                   BD ����
Photos.app                                                     ����            ����  
 cu             Applications  !/:System:Applications:Photos.app/    
 P h o t o s . a p p    M a c i n t o s h   H D  System/Applications/Photos.app  / ��  ��  
  
3
4
3 l     ��������  ��  ��  
4 
5
6
5 i   � �
7
8
7 I      �������� (0 gettoplevelfolders getTopLevelFolders��  ��  
8 k     D
9
9 
:
;
: I     ��
<���� 0 waitforphotos waitForPhotos
< 
=��
= m    ����,��  ��  
; 
>��
> O    D
?
@
? k    C
A
A 
B
C
B r    
D
E
D 2    ��
�� 
IPfd
E o      ���� 0 thesefolders theseFolders
C 
F
G
F r    
H
I
H J    ����  
I o      ���� "0 toplevelfolders topLevelFolders
G 
J
K
J Y    @
L��
M
N��
L k   $ ;
O
O 
P
Q
P r   $ *
R
S
R n   $ (
T
U
T 4   % (��
V
�� 
cobj
V o   & '���� 0 i  
U o   $ %���� 0 thesefolders theseFolders
S o      ���� 0 
thisfolder 
thisFolder
Q 
W��
W Z   + ;
X
Y����
X =  + 0
Z
[
Z l  + .
\����
\ n   + .
]
^
] 1   , .��
�� 
pare
^ o   + ,���� 0 
thisfolder 
thisFolder��  ��  
[ m   . /��
�� 
msng
Y r   3 7
_
`
_ o   3 4���� 0 
thisfolder 
thisFolder
` l     
a����
a n      
b
c
b  ;   5 6
c o   4 5���� "0 toplevelfolders topLevelFolders��  ��  ��  ��  ��  �� 0 i  
M m    ���� 
N l   
d����
d I   ��
e��
�� .corecnte****       ****
e o    ���� 0 thesefolders theseFolders��  ��  ��  ��  
K 
f��
f L   A C
g
g o   A B���� "0 toplevelfolders topLevelFolders��  
@ m    
h
h�                                                                                  Phts  alis    0  Macintosh HD                   BD ����
Photos.app                                                     ����            ����  
 cu             Applications  !/:System:Applications:Photos.app/    
 P h o t o s . a p p    M a c i n t o s h   H D  System/Applications/Photos.app  / ��  ��  
6 
i
j
i l     ��������  ��  ��  
j 
k
l
k i   � �
m
n
m I      ��
o���� 00 gettoplevelfoldernamed getTopLevelFolderNamed
o 
p��
p o      ���� 0 thisname thisName��  ��  
n k     7
q
q 
r
s
r r     
t
u
t I     �������� (0 gettoplevelfolders getTopLevelFolders��  ��  
u o      ���� 0 thesefolders theseFolders
s 
v
w
v Y    0
x��
y
z��
x k    +
{
{ 
|
}
| r    
~

~ n    
�
�
� 4    ��
�
�� 
cobj
� o    ���� 0 i  
� o    ���� 0 thesefolders theseFolders
 o      ���� 0 
thisfolder 
thisFolder
} 
���
� Z    +
�
�����
� =   "
�
�
� l    
�����
� n     
�
�
� 1     ��
�� 
pnam
� o    ���� 0 
thisfolder 
thisFolder��  ��  
� o     !���� 0 thisname thisName
� L   % '
�
� o   % &���� 0 
thisfolder 
thisFolder��  ��  ��  �� 0 i  
y m    ���� 
z l   
�����
� I   ��
���
�� .corecnte****       ****
� o    ���� 0 thesefolders theseFolders��  ��  ��  ��  
w 
���
� R   1 7��
���
�� .ascrerr ****      � ****
� b   3 6
�
�
� m   3 4
�
� �
�
� H T h e r e   i s   n o   t o p   l e v e l   f o l d e r   n a m e d :  
� o   4 5���� 0 thisname thisName��  ��  
l 
�
�
� l     ��������  ��  ��  
� 
�
�
� i   � �
�
�
� I      ��
����� .0 gettoplevelalbumnamed getTopLevelAlbumNamed
� 
���
� o      ���� 0 thisname thisName��  ��  
� k     7
�
� 
�
�
� r     
�
�
� I     ������� &0 gettoplevelalbums getTopLevelAlbums��  �  
� o      �~�~ 0 thesealbums theseAlbums
� 
�
�
� Y    0
��}
�
��|
� k    +
�
� 
�
�
� r    
�
�
� n    
�
�
� 4    �{
�
�{ 
cobj
� o    �z�z 0 i  
� o    �y�y 0 thesealbums theseAlbums
� o      �x�x 0 	thisalbum 	thisAlbum
� 
��w
� Z    +
�
��v�u
� =   "
�
�
� l    
��t�s
� n     
�
�
� 1     �r
�r 
pnam
� o    �q�q 0 	thisalbum 	thisAlbum�t  �s  
� o     !�p�p 0 thisname thisName
� L   % '
�
� o   % &�o�o 0 	thisalbum 	thisAlbum�v  �u  �w  �} 0 i  
� m    �n�n 
� l   
��m�l
� I   �k
��j
�k .corecnte****       ****
� o    �i�i 0 thesealbums theseAlbums�j  �m  �l  �|  
� 
��h
� R   1 7�g
��f
�g .ascrerr ****      � ****
� b   3 6
�
�
� m   3 4
�
� �
�
� F T h e r e   i s   n o   t o p   l e v e l   a l b u m   n a m e d :  
� o   4 5�e�e 0 thisname thisName�f  �h  
� 
�
�
� l     �d�c�b�d  �c  �b  
� 
�
�
� i   � �
�
�
� I      �a
��`�a >0 isthisfolderoralbumattoplevel isThisFolderOrAlbumAtTopLevel
� 
��_
� o      �^�^ 0 thisitem thisItem�_  �`  
� k     #
�
� 
�
�
� I     �]
��\�] 0 waitforphotos waitForPhotos
� 
��[
� m    �Z�Z,�[  �\  
� 
��Y
� Q    #
�
�
�
� Z   
 
�
��X
�
� =  
 
�
�
� n   
 
�
�
� 1    �W
�W 
pare
� o   
 �V�V 0 thisitem thisItem
� m    �U
�U 
msng
� L    
�
� m    �T
�T boovtrue�X  
� L    
�
� m    �S
�S boovfals
� R      �R�Q�P
�R .ascrerr ****      � ****�Q  �P  
� L   ! #
�
� m   ! "�O
�O boovfals�Y  
� 
�
�
� l     �N�M�L�N  �M  �L  
� 
�
�
� i   � �
�
�
� I      �K
��J�K V0 )deriveinyternallibrarypathtoalbumorfolder )deriveInyternalLibraryPathToAlbumOrFolder
� 
�
�
� o      �I�I 0 targetobject targetObject
� 
�
�
� o      �H�H 0 appenditemid appendItemID
� 
��G
� o      �F�F &0 pathitemdelimiter pathItemDelimiter�G  �J  
� k     �
�
� 
�
�
� I     �E
��D�E 0 waitforphotos waitForPhotos
� 
��C
� m    �B�B,�C  �D  
� 
��A
� O    �
�
�
� k    �
�
� 
�
�
� Z    
�
��@�?
� H    
�
� l   
��>�=
� I   �<
��;
�< .coredoexnull���     ****
� o    �:�: 0 targetobject targetObject�;  �>  �=  
� R    �9
��8
�9 .ascrerr ****      � ****
� m    
�
� �
�
� D T h e   i n d i c a t e d   i t e m   d o e s   n o t   e x i s t .�8  �@  �?  
� 
�
�
� Z    A
�
�
�
�
� =   "
� 
� n      m     �7
�7 
pcls o    �6�6 0 targetobject targetObject  m     !�5
�5 
IPal
� r   % * l  % (�4�3 n   % ( 1   & (�2
�2 
pnam o   % &�1�1 0 targetobject targetObject�4  �3   o      �0�0 $0 targetobjectname targetObjectName
� 	 =  - 2

 n   - 0 m   . 0�/
�/ 
pcls o   - .�.�. 0 targetobject targetObject m   0 1�-
�- 
IPfd	 �, r   5 : l  5 8�+�* n   5 8 1   6 8�)
�) 
pnam o   5 6�(�( 0 targetobject targetObject�+  �*   o      �'�' $0 targetobjectname targetObjectName�,  
� R   = A�&�%
�& .ascrerr ****      � **** m   ? @ � Z T h e   i n d i c a t e d   i t e m   m u s t   b e   a   f o l d e r   o r   a l b u m .�%  
�  Z   B _�$ =  B E o   B C�#�# 0 appenditemid appendItemID m   C D�"
�" boovtrue k   H Y   r   H M!"! l  H K#�!� # n   H K$%$ 1   I K�
� 
ID  % o   H I�� 0 targetobject targetObject�!  �   " o      ��  0 targetobjectid targetObjectID  &�& r   N Y'(' b   N W)*) b   N U+,+ b   N S-.- b   N Q/0/ o   N O�� $0 targetobjectname targetObjectName0 1   O P�
� 
spac. m   Q R11 �22  (, o   S T��  0 targetobjectid targetObjectID* m   U V33 �44  )( o      �� 0 pathtoobject pathToObject�  �$   r   \ _565 o   \ ]�� $0 targetobjectname targetObjectName6 o      �� 0 pathtoobject pathToObject 7�7 T   ` �88 Q   e �9:;9 Z   h �<=>�< =  h o?@? n   h mABA m   k m�
� 
pclsB n   h kCDC 1   i k�
� 
pareD o   h i�� 0 targetobject targetObject@ m   m n�
� 
IPfd= k   r �EE FGF r   r yHIH l  r wJ��J n   r wKLK 1   u w�
� 
pnamL n   r uMNM 1   s u�
� 
pareN o   r s�� 0 targetobject targetObject�  �  I o      �
�
 0 
foldername 
folderNameG OPO r   z �QRQ b   z STS b   z }UVU o   z {�	�	 0 
foldername 
folderNameV o   { |�� &0 pathitemdelimiter pathItemDelimiterT o   } ~�� 0 pathtoobject pathToObjectR o      �� 0 pathtoobject pathToObjectP WXW r   � �YZY n   � �[\[ 1   � ��
� 
ID  \ n   � �]^] 1   � ��
� 
pare^ o   � ��� 0 targetobject targetObjectZ o      �� 0 thisid thisIDX _�_ r   � �`a` 5   � �� b��
�  
IPfdb o   � ����� 0 thisid thisID
�� kfrmID  a o      ���� 0 targetobject targetObject�  > cdc =  � �efe n   � �ghg m   � ���
�� 
pclsh n   � �iji 1   � ���
�� 
parej o   � ����� 0 targetobject targetObjectf m   � ���
�� 
IPald k��k k   � �ll mnm r   � �opo l  � �q����q n   � �rsr 1   � ���
�� 
pnams n   � �tut 1   � ���
�� 
pareu o   � ����� 0 targetobject targetObject��  ��  p o      ���� 0 	albumname 	albumNamen vwv r   � �xyx b   � �z{z b   � �|}| o   � ����� 0 	albumname 	albumName} o   � ����� &0 pathitemdelimiter pathItemDelimiter{ o   � ����� 0 pathtoobject pathToObjecty o      ���� 0 pathtoobject pathToObjectw ~~ r   � ���� n   � ���� 1   � ���
�� 
ID  � n   � ���� 1   � ���
�� 
pare� o   � ����� 0 targetobject targetObject� o      ���� 0 thisid thisID ���� r   � ���� 5   � ������
�� 
IPal� o   � ����� 0 thisid thisID
�� kfrmID  � o      ���� 0 targetobject targetObject��  ��  �  : R      ������
�� .ascrerr ****      � ****��  ��  ; L   � ��� o   � ����� 0 pathtoobject pathToObject�  
� m    ���                                                                                  Phts  alis    0  Macintosh HD                   BD ����
Photos.app                                                     ����            ����  
 cu             Applications  !/:System:Applications:Photos.app/    
 P h o t o s . a p p    M a c i n t o s h   H D  System/Applications/Photos.app  / ��  �A  
� ��� l     ��������  ��  ��  � ��� i   � ���� I      ������� 0 waitforphotos waitForPhotos� ���� o      ���� 40 timeoutdurationinseconds timeoutDurationInSeconds��  ��  � k     p�� ��� Z     m������� =    ��� n     ��� 1    ��
�� 
prun� m     ���                                                                                  Phts  alis    0  Macintosh HD                   BD ����
Photos.app                                                     ����            ����  
 cu             Applications  !/:System:Applications:Photos.app/    
 P h o t o s . a p p    M a c i n t o s h   H D  System/Applications/Photos.app  / ��  � m    ��
�� boovfals� k    i�� ��� O   ��� I   ������
�� .ascrnoop****      � ****��  ��  � m    	���                                                                                  Phts  alis    0  Macintosh HD                   BD ����
Photos.app                                                     ����            ����  
 cu             Applications  !/:System:Applications:Photos.app/    
 P h o t o s . a p p    M a c i n t o s h   H D  System/Applications/Photos.app  / ��  � ��� O    b��� k    a�� ��� r     ��� l   ������ n    ��� 1    ��
�� 
time� l   ������ I   ������
�� .misccurdldt    ��� null��  ��  ��  ��  ��  ��  � o      ���� ,0 currenttimeinseconds currentTimeInSeconds� ���� W   ! a��� Q   1 \����� k   4 S�� ��� O   4 B��� r   8 A��� l  8 ?������ I  8 ?�����
�� .corecnte****       ****� 2  8 ;��
�� 
IPmi��  ��  ��  � o      ����  0 mediaitemcount mediaItemCount� m   4 5���                                                                                  Phts  alis    0  Macintosh HD                   BD ����
Photos.app                                                     ����            ����  
 cu             Applications  !/:System:Applications:Photos.app/    
 P h o t o s . a p p    M a c i n t o s h   H D  System/Applications/Photos.app  / ��  � ���� Z   C S������ >  C F��� o   C D����  0 mediaitemcount mediaItemCount� m   D E����  � L   I K�� m   I J��
�� boovtrue��  � I  N S�����
�� .sysodelanull��� ��� nmbr� m   N O���� ��  ��  � R      ������
�� .ascrerr ****      � ****��  ��  ��  � ?  % 0��� l  % ,������ n   % ,��� 1   * ,��
�� 
time� l  % *������ I  % *������
�� .misccurdldt    ��� null��  ��  ��  ��  ��  ��  � l 	 , /������ l  , /������ [   , /��� o   , -���� ,0 currenttimeinseconds currentTimeInSeconds� o   - .���� 40 timeoutdurationinseconds timeoutDurationInSeconds��  ��  ��  ��  ��  � m    ��
�� misccura� ���� R   c i�����
�� .ascrerr ****      � ****��  � �����
�� 
errn� m   e f��������  ��  ��  ��  � ���� L   n p�� m   n o��
�� boovtrue��  � ��� l     ��������  ��  ��  � ��� l      ������  �   EXPORT HANDLERS    � ��� "   E X P O R T   H A N D L E R S  � ��� l     ��������  ��  ��  � ��� i   � ���� I      ������� 0 quickexport quickExport� ��� o      ���� 0 
theseitems 
theseItems� ��� o      ���� :0 returnfilereferencesboolean returnFileReferencesBoolean� ��� o      ���� 00 revealindicatorboolean revealIndicatorBoolean� ��� o      ���� .0 namingmethodindicator namingMethodIndicator� ��� o      ���� .0 shouldexportoriginals shouldExportOriginals� ��� o      ���� 0 basename baseName� ��� o      ���� 0 thisseparator thisSeparator� ��� o      ���� "0 thisdigitlength thisDigitLength� ���� o      ����  0 startingnumber startingNumber��  ��  � k    ��� ��� l     ������  � e _ display alert "quickExport where namingMethodIndicator = " & (namingMethodIndicator as string)   � ��� �   d i s p l a y   a l e r t   " q u i c k E x p o r t   w h e r e   n a m i n g M e t h o d I n d i c a t o r   =   "   &   ( n a m i n g M e t h o d I n d i c a t o r   a s   s t r i n g )� ��� r     ��� J     
    m      �  m p 4  m     �  m o v 	
	 m     �  a v i
  m     �  3 g p  m     �  m x f  m     �  m t s  m     �  m 2 t s �� m     �  m 4 v��  � o      ���� "0 videoextensions VideoExtensions�  !  r    "#" n   $%$ I    �������� (0 createexportfolder createExportFolder��  ��  %  f    # o      ���� 0 exportfolder exportFolder! &'& r    ()( n    *+* 1    ��
�� 
psxp+ o    ���� 0 exportfolder exportFolder) o      ���� .0 exportfolderposixpath exportFolderPOSIXPath' ,-, Z   /./0��. =   121 o    ���� .0 namingmethodindicator namingMethodIndicator2 m    ���� / l  !3453 k   !66 787 r   ! *9:9 n  ! (;<; I   $ (��~�}�  0 defaultmanager defaultManager�~  �}  < n  ! $=>= o   " $�|�| 0 nsfilemanager NSFileManager> m   ! "�{
�{ misccura: o      �z�z 0 filemanager fileManager8 ?�y? Y   +@�xAB�w@ k   9CC DED O   9 jFGF k   = iHH IJI r   = EKLK n   = CMNM 4   > C�vO
�v 
cobjO o   A B�u�u 0 i  N o   = >�t�t 0 
theseitems 
theseItemsL o      �s�s 0 thisitem thisItemJ PQP r   F MRSR n   F KTUT 1   G K�r
�r 
ID  U o   F G�q�q 0 thisitem thisItemS o      �p�p 0 thisid thisIDQ VWV r   N WXYX n   N SZ[Z 1   O S�o
�o 
filn[ o   N O�n�n 0 thisitem thisItemY o      �m�m 0 currentname currentNameW \�l\ I  X i�k]^
�k .IPXSexponull���     ****] J   X [__ `�j` o   X Y�i�i 0 thisitem thisItem�j  ^ �hab
�h 
insha o   ^ _�g�g 0 exportfolder exportFolderb �fc�e
�f 
usMAc m   b c�d
�d boovfals�e  �l  G m   9 :dd�                                                                                  Phts  alis    0  Macintosh HD                   BD ����
Photos.app                                                     ����            ����  
 cu             Applications  !/:System:Applications:Photos.app/    
 P h o t o s . a p p    M a c i n t o s h   H D  System/Applications/Photos.app  / ��  E efe r   k {ghg l  k wi�c�bi n  k wjkj I   p w�al�`�a &0 stringwithstring_ stringWithString_l m�_m o   p s�^�^ 0 currentname currentName�_  �`  k n  k pnon o   l p�]�] 0 nsstring NSStringo m   k l�\
�\ misccura�c  �b  h o      �[�[ 0 thisitemname thisItemNamef pqp r   | �rsr c   | �tut l  | �v�Z�Yv n  | �wxw I    ��X�W�V�X >0 stringbydeletingpathextension stringByDeletingPathExtension�W  �V  x o   | �U�U 0 thisitemname thisItemName�Z  �Y  u m   � ��T
�T 
TEXTs o      �S�S ,0 namewithoutextension nameWithoutExtensionq yzy l  � ��R�Q�P�R  �Q  �P  z {|{ r   � �}~} c   � �� n   � ���� o   � ��O�O 0 pathextension pathExtension� o   � ��N�N 0 thisitemname thisItemName� m   � ��M
�M 
TEXT~ o      �L�L &0 internalextension internalExtension| ��� l  � ��K�J�I�K  �J  �I  � ��� l  � ��H���H  � / ) Modifications by Henry Kautz	27 Jan 2021   � ��� R   M o d i f i c a t i o n s   b y   H e n r y   K a u t z 	 2 7   J a n   2 0 2 1� ��� l  � ��G���G  � 9 3 Current version of Photos exports as .mov or .jpeg   � ��� f   C u r r e n t   v e r s i o n   o f   P h o t o s   e x p o r t s   a s   . m o v   o r   . j p e g� ��� l  � ��F���F  � P J The internal file extension is on longer the same as the exported version   � ��� �   T h e   i n t e r n a l   f i l e   e x t e n s i o n   i s   o n   l o n g e r   t h e   s a m e   a s   t h e   e x p o r t e d   v e r s i o n� ��� r   � ���� c   � ���� b   � ���� b   � ���� o   � ��E�E .0 exportfolderposixpath exportFolderPOSIXPath� o   � ��D�D ,0 namewithoutextension nameWithoutExtension� m   � ��� ���  . m o v� m   � ��C
�C 
TEXT� o      �B�B 0 filetestpath fileTestPath� ��� Z   � ����A�� l  � ���@�?� n  � ���� I   � ��>��=�> &0 fileexistsatpath_ fileExistsAtPath_� ��<� o   � ��;�; 0 filetestpath fileTestPath�<  �=  � o   � ��:�: 0 filemanager fileManager�@  �?  � r   � ���� m   � ��� ���  m o v� o      �9�9 60 extensionexportedbyphotos extensionExportedByPhotos�A  � r   � ���� m   � ��� ���  j p e g� o      �8�8 60 extensionexportedbyphotos extensionExportedByPhotos� ��� r   � ���� b   � ���� b   � ���� o   � ��7�7 ,0 namewithoutextension nameWithoutExtension� m   � ��� ���  .� o   � ��6�6 60 extensionexportedbyphotos extensionExportedByPhotos� o      �5�5  0 targetfilename targetFileName� ��� r   � ���� n  � ���� I   � ��4��3�4 &0 createnumericname createNumericName� ��� o   � ��2�2 0 basename baseName� ��� o   � ��1�1 0 thisseparator thisSeparator� ��� o   � ��0�0 "0 thisdigitlength thisDigitLength� ��� o   � ��/�/ 0 	thisindex 	thisIndex� ��.� o   � ��-�- 60 extensionexportedbyphotos extensionExportedByPhotos�.  �3  �  f   � �� o      �,�, 0 newname newName� ��� l  � ��+���+  �   End modifications   � ��� $   E n d   m o d i f i c a t i o n s� ��� l  � ��*�)�(�*  �)  �(  � ��� l  � ��'���'  � / ) use File Manager to rename exported item   � ��� R   u s e   F i l e   M a n a g e r   t o   r e n a m e   e x p o r t e d   i t e m� ��� r   � ���� b   � ���� o   � ��&�& .0 exportfolderposixpath exportFolderPOSIXPath� o   � ��%�%  0 targetfilename targetFileName� o      �$�$ "0 currentfilepath currentFilePath� ��� r   ���� b   � ���� o   � ��#�# .0 exportfolderposixpath exportFolderPOSIXPath� o   � ��"�" 0 newname newName� o      �!�! 0 newfilepath newFilePath� �� � l ���� n ��� I  ���� <0 moveitematpath_topath_error_ moveItemAtPath_toPath_error_� ��� o  �� "0 currentfilepath currentFilePath� ��� o  �� 0 newfilepath newFilePath� ��� l ���� m  �
� 
msng�  �  �  �  � o  �� 0 filemanager fileManager�  �  �   �x 0 i  A m   . /�� B l  / 4���� I  / 4���
� .corecnte****       ****� o   / 0�� 0 
theseitems 
theseItems�  �  �  �w  �y  4 - ' name exported items to their Photos ID   5 ��� N   n a m e   e x p o r t e d   i t e m s   t o   t h e i r   P h o t o s   I D0 ��� = ��� o  �� .0 namingmethodindicator namingMethodIndicator� m  �� � ��� l  4���� O   4��� I $3���
� .IPXSexponull���     ****� o  $%�� 0 
theseitems 
theseItems� �
� 
�
 
insh� o  ()�	�	 0 exportfolder exportFolder  ��
� 
usMA o  ,-�� .0 shouldexportoriginals shouldExportOriginals�  � m   !�                                                                                  Phts  alis    0  Macintosh HD                   BD ����
Photos.app                                                     ����            ����  
 cu             Applications  !/:System:Applications:Photos.app/    
 P h o t o s . a p p    M a c i n t o s h   H D  System/Applications/Photos.app  / ��  � 5 / current file names. Option to export originals   � � ^   c u r r e n t   f i l e   n a m e s .   O p t i o n   t o   e x p o r t   o r i g i n a l s�  = 7: o  78�� .0 namingmethodindicator namingMethodIndicator m  89��   � l =+	
	 k  =+  r  =F n =D I  @D��� �  0 defaultmanager defaultManager�  �    n =@ o  >@���� 0 nsfilemanager NSFileManager m  =>��
�� misccura o      ���� 0 filemanager fileManager  r  GL o  GH����  0 startingnumber startingNumber o      ���� 0 	thisindex 	thisIndex �� Y  M+���� k  [&  O  [� !  k  _�"" #$# r  _g%&% n  _e'(' 4  `e��)
�� 
cobj) o  cd���� 0 i  ( o  _`���� 0 
theseitems 
theseItems& o      ���� 0 thisitem thisItem$ *+* r  hq,-, n  hm./. 1  im��
�� 
filn/ o  hi���� 0 thisitem thisItem- o      ���� 0 currentname currentName+ 0��0 I r���12
�� .IPXSexponull���     ****1 J  ru33 4��4 o  rs���� 0 thisitem thisItem��  2 ��56
�� 
insh5 o  xy���� 0 exportfolder exportFolder6 ��7��
�� 
usMA7 m  |}��
�� boovfals��  ��  ! m  [\88�                                                                                  Phts  alis    0  Macintosh HD                   BD ����
Photos.app                                                     ����            ����  
 cu             Applications  !/:System:Applications:Photos.app/    
 P h o t o s . a p p    M a c i n t o s h   H D  System/Applications/Photos.app  / ��   9:9 l ����;<��  ; + % determine new name for exported file   < �== J   d e t e r m i n e   n e w   n a m e   f o r   e x p o r t e d   f i l e: >?> r  ��@A@ l ��B����B n ��CDC I  ����E���� &0 stringwithstring_ stringWithString_E F��F o  ������ 0 currentname currentName��  ��  D n ��GHG o  ������ 0 nsstring NSStringH m  ����
�� misccura��  ��  A o      ���� 0 thisitemname thisItemName? IJI r  ��KLK c  ��MNM l ��O����O n ��PQP I  ���������� >0 stringbydeletingpathextension stringByDeletingPathExtension��  ��  Q o  ������ 0 thisitemname thisItemName��  ��  N m  ����
�� 
TEXTL o      ���� ,0 namewithoutextension nameWithoutExtensionJ RSR l ����������  ��  ��  S TUT l ����VW��  V / ) Modifications by Henry Kautz	27 Jan 2021   W �XX R   M o d i f i c a t i o n s   b y   H e n r y   K a u t z 	 2 7   J a n   2 0 2 1U YZY l ����[\��  [ 9 3 Current version of Photos exports as .mov or .jpeg   \ �]] f   C u r r e n t   v e r s i o n   o f   P h o t o s   e x p o r t s   a s   . m o v   o r   . j p e gZ ^_^ l ����`a��  ` P J The internal file extension is on longer the same as the exported version   a �bb �   T h e   i n t e r n a l   f i l e   e x t e n s i o n   i s   o n   l o n g e r   t h e   s a m e   a s   t h e   e x p o r t e d   v e r s i o n_ cdc r  ��efe c  ��ghg b  ��iji b  ��klk o  ������ .0 exportfolderposixpath exportFolderPOSIXPathl o  ������ ,0 namewithoutextension nameWithoutExtensionj m  ��mm �nn  . m o vh m  ����
�� 
TEXTf o      ���� 0 filetestpath fileTestPathd opo Z  ��qr��sq l ��t����t n ��uvu I  ����w���� &0 fileexistsatpath_ fileExistsAtPath_w x��x o  ������ 0 filetestpath fileTestPath��  ��  v o  ������ 0 filemanager fileManager��  ��  r r  ��yzy m  ��{{ �||  m o vz o      ���� 60 extensionexportedbyphotos extensionExportedByPhotos��  s r  ��}~} m  �� ���  j p e g~ o      ���� 60 extensionexportedbyphotos extensionExportedByPhotosp ��� r  ����� b  ����� b  ����� o  ������ ,0 namewithoutextension nameWithoutExtension� m  ���� ���  .� o  ������ 60 extensionexportedbyphotos extensionExportedByPhotos� o      ����  0 targetfilename targetFileName� ��� r  ����� n ����� I  ��������� &0 createnumericname createNumericName� ��� o  ������ 0 basename baseName� ��� o  ������ 0 thisseparator thisSeparator� ��� o  ������ "0 thisdigitlength thisDigitLength� ��� o  ������ 0 	thisindex 	thisIndex� ���� o  ������ 60 extensionexportedbyphotos extensionExportedByPhotos��  ��  �  f  ��� o      ���� 0 newname newName� ��� l ��������  �   End modifications   � ��� $   E n d   m o d i f i c a t i o n s� ��� l ����������  ��  ��  � ��� l ��������  � / ) use File Manager to rename exported item   � ��� R   u s e   F i l e   M a n a g e r   t o   r e n a m e   e x p o r t e d   i t e m� ��� r  ���� b  ����� o  ������ .0 exportfolderposixpath exportFolderPOSIXPath� o  ������  0 targetfilename targetFileName� o      ���� "0 currentfilepath currentFilePath� ��� r  ��� b  	��� o  ���� .0 exportfolderposixpath exportFolderPOSIXPath� o  ���� 0 newname newName� o      ���� 0 newfilepath newFilePath� ��� l ������ n ��� I  ������� <0 moveitematpath_topath_error_ moveItemAtPath_toPath_error_� ��� o  ���� "0 currentfilepath currentFilePath� ��� o  ���� 0 newfilepath newFilePath� ���� l ������ m  ��
�� 
msng��  ��  ��  ��  � o  ���� 0 filemanager fileManager��  ��  � ��� l ��������  ��  ��  � ���� r  &��� [  "��� o   ���� 0 	thisindex 	thisIndex� m   !���� � o      ���� 0 	thisindex 	thisIndex��  �� 0 i   m  PQ����  l QV������ I QV�����
�� .corecnte****       ****� o  QR���� 0 
theseitems 
theseItems��  ��  ��  ��  ��  
   sequential naming    ��� $   s e q u e n t i a l   n a m i n g�  ��  - ��� Z  0l������ = 03��� o  01���� :0 returnfilereferencesboolean returnFileReferencesBoolean� m  12��
�� boovtrue� k  6d�� ��� l 66������  � N H RETRIEVE REFERENCES TO EXPORTED ITEMS AND SORT BY CREATION DATE OR NAME   � ��� �   R E T R I E V E   R E F E R E N C E S   T O   E X P O R T E D   I T E M S   A N D   S O R T   B Y   C R E A T I O N   D A T E   O R   N A M E� ��� r  6=��� n  69��� 1  79��
�� 
psxp� o  67���� 0 exportfolder exportFolder� o      ���� "0 folderposixpath folderPosixPath� ���� Z  >d������ E >D��� J  >B�� ��� m  >?����  � ���� m  ?@���� ��  � o  BC���� .0 namingmethodindicator namingMethodIndicator� k  GT�� ��� l GG������  �   SORT BY NAME   � ���    S O R T   B Y   N A M E� ���� r  GT��� I  GP������� *0 sortedlistofitemsin sortedListOfItemsIn� ��� o  HK���� "0 folderposixpath folderPosixPath� ���� m  KL��  ��  ��  � o      �~�~ *0 resultingreferences resultingReferences��  ��  � l Wd���� r  Wd��� I  W`�}��|�} *0 sortedlistofitemsin sortedListOfItemsIn� ��� o  X[�{�{ "0 folderposixpath folderPosixPath� ��z� m  [\�y�y �z  �|  � o      �x�x *0 resultingreferences resultingReferences�   SORT BY DATE   � ���    S O R T   B Y   D A T E��  ��  � r  gl��� o  gh�w�w 0 exportfolder exportFolder� o      �v�v *0 resultingreferences resultingReferences� � � Z  m�u�t = mp o  mn�s�s 00 revealindicatorboolean revealIndicatorBoolean m  no�r
�r boovtrue I  s{�q�p�q  0 revealinfinder revealInFinder �o o  tw�n�n *0 resultingreferences resultingReferences�o  �p  �u  �t    �m L  �� o  ���l�l *0 resultingreferences resultingReferences�m  � 	
	 l     �k�j�i�k  �j  �i  
  i   � � I      �h�g�h *0 sortedlistofitemsin sortedListOfItemsIn  o      �f�f "0 folderposixpath folderPosixPath �e o      �d�d $0 sortkeyindicator sortKeyIndicator�e  �g   k     �  l     r      n    	 I    	�c�b�c $0 fileurlwithpath_ fileURLWithPath_ �a o    �`�` "0 folderposixpath folderPosixPath�a  �b   n      4    �_!
�_ 
pcls! m    "" �## 
 N S U R L  m     �^
�^ misccura o      �]�] 0 thensurl theNSURL 7 1 make URL for folder because that's what's needed    �$$ b   m a k e   U R L   f o r   f o l d e r   b e c a u s e   t h a t ' s   w h a t ' s   n e e d e d %&% l   '()' r    *+* n   ,-, I    �\�[�Z�\  0 defaultmanager defaultManager�[  �Z  - n   ./. o    �Y�Y 0 nsfilemanager NSFileManager/ m    �X
�X misccura+ o      �W�W $0 thensfilemanager theNSFileManager(   get file manager   ) �00 "   g e t   f i l e   m a n a g e r& 121 l   �V34�V  3 !  get contents of the folder   4 �55 6   g e t   c o n t e n t s   o f   t h e   f o l d e r2 676 l    89:8 r     ;<; J    == >?> n   @A@ o    �U�U 0 nsurlpathkey NSURLPathKeyA m    �T
�T misccura? B�SB n   CDC o    �R�R B0 nsurlcontentmodificationdatekey NSURLContentModificationDateKeyD m    �Q
�Q misccura�S  < o      �P�P 0 keystorequest keysToRequest9 , & keys for values we want for each item   : �EE L   k e y s   f o r   v a l u e s   w e   w a n t   f o r   e a c h   i t e m7 FGF r   ! .HIH n  ! ,JKJ I   " ,�OL�N�O �0 Bcontentsofdirectoryaturl_includingpropertiesforkeys_options_error_ BcontentsOfDirectoryAtURL_includingPropertiesForKeys_options_error_L MNM o   " #�M�M 0 thensurl theNSURLN OPO o   # $�L�L 0 keystorequest keysToRequestP QRQ l  $ 'S�K�JS n  $ 'TUT o   % '�I�I P0 &nsdirectoryenumerationskipshiddenfiles &NSDirectoryEnumerationSkipsHiddenFilesU m   $ %�H
�H misccura�K  �J  R V�GV l  ' (W�F�EW m   ' (�D
�D 
msng�F  �E  �G  �N  K o   ! "�C�C $0 thensfilemanager theNSFileManagerI o      �B�B 0 theurls theURLsG XYX l  / /�AZ[�A  Z ' ! get mod dates and paths for URLs   [ �\\ B   g e t   m o d   d a t e s   a n d   p a t h s   f o r   U R L sY ]^] l  / 8_`a_ r   / 8bcb n  / 6ded I   2 6�@�?�>�@ 	0 array  �?  �>  e n  / 2fgf o   0 2�=�=  0 nsmutablearray NSMutableArrayg m   / 0�<
�< misccurac o      �;�; .0 theinfonsmutablearray theInfoNSMutableArray` #  array to store new values in   a �hh :   a r r a y   t o   s t o r e   n e w   v a l u e s   i n^ iji Y   9 ck�:lm�9k k   G ^nn opo l  G Qqrsq r   G Qtut l  G Ov�8�7v n  G Owxw I   H O�6y�5�6  0 objectatindex_ objectAtIndex_y z�4z l  H K{�3�2{ \   H K|}| o   H I�1�1 0 i  } m   I J�0�0 �3  �2  �4  �5  x o   G H�/�/ 0 theurls theURLs�8  �7  u o      �.�. 0 annsurl anNSURLr   zero-based   s �~~    z e r o - b a s e dp �- l  R ^���� l  R ^��,�+� n  R ^��� I   S ^�*��)�* 0 
addobject_ 
addObject_� ��(� l  S Z��'�&� n  S Z��� I   T Z�%��$�% <0 resourcevaluesforkeys_error_ resourceValuesForKeys_error_� ��� o   T U�#�# 0 keystorequest keysToRequest� ��"� l  U V��!� � m   U V�
� 
msng�!  �   �"  �$  � o   S T�� 0 annsurl anNSURL�'  �&  �(  �)  � o   R S�� .0 theinfonsmutablearray theInfoNSMutableArray�,  �+  � - ' get values dictionary and add to array   � ��� N   g e t   v a l u e s   d i c t i o n a r y   a n d   a d d   t o   a r r a y�-  �: 0 i  l m   < =�� m n  = B��� I   > B���� 	0 count  �  �  � o   = >�� 0 theurls theURLs�9  j ��� Z   d ������ =  d g��� o   d e�� $0 sortkeyindicator sortKeyIndicator� m   e f��  � k   j {�� ��� l  j j����  �   sort in name order   � ��� &   s o r t   i n   n a m e   o r d e r� ��� l  j {���� r   j {��� n  j y��� I   o y���� D0  sortdescriptorwithkey_ascending_  sortDescriptorWithKey_ascending_� ��� l  o t���� n  o t��� o   p t�� .0 nsurllocalizednamekey NSURLLocalizedNameKey� m   o p�
� misccura�  �  � ��� m   t u�
� boovfals�  �  � n  j o��� o   k o�
�
 $0 nssortdescriptor NSSortDescriptor� m   j k�	
�	 misccura� o      �� *0 thenssortdescriptor theNSSortDescriptor� $  describes the sort to perform   � ��� <   d e s c r i b e s   t h e   s o r t   t o   p e r f o r m�  �  � k   ~ ��� ��� l  ~ ~����  �   sort them in date order   � ��� 0   s o r t   t h e m   i n   d a t e   o r d e r� ��� l  ~ ����� r   ~ ���� n  ~ ���� I   � ����� D0  sortdescriptorwithkey_ascending_  sortDescriptorWithKey_ascending_� ��� l  � ����� n  � ���� o   � ��� B0 nsurlcontentmodificationdatekey NSURLContentModificationDateKey� m   � �� 
�  misccura�  �  � ���� m   � ���
�� boovfals��  �  � n  ~ ���� o    ����� $0 nssortdescriptor NSSortDescriptor� m   ~ ��
�� misccura� o      ���� *0 thenssortdescriptor theNSSortDescriptor� $  describes the sort to perform   � ��� <   d e s c r i b e s   t h e   s o r t   t o   p e r f o r m�  � ��� l  � ����� n  � ���� I   � �������� .0 sortusingdescriptors_ sortUsingDescriptors_� ���� J   � ��� ���� o   � ����� *0 thenssortdescriptor theNSSortDescriptor��  ��  ��  � o   � ����� .0 theinfonsmutablearray theInfoNSMutableArray�   do the sort   � ���    d o   t h e   s o r t� ��� l  � �������  � %  get the path of the first item   � ��� >   g e t   t h e   p a t h   o f   t h e   f i r s t   i t e m� ��� l  � ����� r   � ���� c   � ���� l  � ������� n  � ���� I   � �������� 0 valueforkey_ valueForKey_� ���� l  � ������� n  � ���� o   � ����� 0 nsurlpathkey NSURLPathKey� m   � ���
�� misccura��  ��  ��  ��  � o   � ����� .0 theinfonsmutablearray theInfoNSMutableArray��  ��  � m   � ���
�� 
list� o      ���� 0 
thesepaths 
thesePaths�   extract paths   � ���    e x t r a c t   p a t h s� ��� r   � ���� J   � �����  � o      ���� ,0 thesealiasreferences theseAliasReferences� ��� Y   � ��������� r   � ���� l  � ������� c   � ���� c   � ���� l  � ������� n   � ���� 4   � ��� 
�� 
cobj  o   � ����� 0 i  � o   � ����� 0 
thesepaths 
thesePaths��  ��  � m   � ���
�� 
psxf� m   � ���
�� 
alis��  ��  � l     ���� n        ;   � � o   � ����� ,0 thesealiasreferences theseAliasReferences��  ��  �� 0 i  � m   � ����� � l  � ����� I  � �����
�� .corecnte****       **** o   � ����� 0 
thesepaths 
thesePaths��  ��  ��  ��  � �� L   � � o   � ����� ,0 thesealiasreferences theseAliasReferences��   	 l     ��������  ��  ��  	 

 i   � � I      ������  0 revealinfinder revealInFinder �� o      ���� 0 
theseitems 
theseItems��  ��   k     ]  l     ����   D > convert passed file refs to POSIX paths and then to file URLs    � |   c o n v e r t   p a s s e d   f i l e   r e f s   t o   P O S I X   p a t h s   a n d   t h e n   t o   f i l e   U R L s  Z    ���� >     l    ���� n      m    ��
�� 
pcls o     ���� 0 
theseitems 
theseItems��  ��   m    ��
�� 
list r      c    !"! o    	���� 0 
theseitems 
theseItems" m   	 
��
�� 
list  o      ���� 0 
theseitems 
theseItems��  ��   #$# r    %&% J    ����  & o      ���� 0 	theseurls 	theseURLs$ '(' Y    B)��*+��) k   % =,, -.- r   % +/0/ n   % )121 4   & )��3
�� 
cobj3 o   ' (���� 0 i  2 o   % &���� 0 
theseitems 
theseItems0 o      ���� 0 thisitem thisItem. 454 r   , 1676 l  , /8����8 n   , /9:9 1   - /��
�� 
psxp: o   , -���� 0 thisitem thisItem��  ��  7 o      ���� &0 thisitemposixpath thisItemPOSIXPath5 ;��; r   2 =<=< l  2 :>����> n  2 :?@? I   5 :��A���� $0 fileurlwithpath_ fileURLWithPath_A B��B o   5 6���� &0 thisitemposixpath thisItemPOSIXPath��  ��  @ n  2 5CDC o   3 5���� 0 nsurl NSURLD m   2 3��
�� misccura��  ��  = l     E����E n      FGF  ;   ; <G o   : ;���� 0 	theseurls 	theseURLs��  ��  ��  �� 0 i  * m    ���� + l    H����H I    ��I��
�� .corecnte****       ****I o    ���� 0 
theseitems 
theseItems��  ��  ��  ��  ( JKJ l  C C��LM��  L "  reveal items in file viewer   M �NN 8   r e v e a l   i t e m s   i n   f i l e   v i e w e rK OPO O  C QQRQ r   I PSTS I   I N�������� "0 sharedworkspace sharedWorkspace��  ��  T o      ���� 0 theworkspace theWorkspaceR n  C FUVU o   D F���� 0 nsworkspace NSWorkspaceV m   C D��
�� misccuraP W��W O  R ]XYX I   V \��Z���� D0  activatefileviewerselectingurls_  activateFileViewerSelectingURLs_Z [��[ o   W X���� 0 	theseurls 	theseURLs��  ��  Y o   R S���� 0 theworkspace theWorkspace��   \]\ l     ��������  ��  ��  ] ^_^ i   � �`a` I      ��b���� (0 namesoffolderitems namesOfFolderItemsb c��c o      ���� 0 targetfolder targetFolder��  ��  a l    bdefd k     bgg hih Z     7jk��lj =    mnm n     opo m    ��
�� 
pclsp o     ���� 0 targetfolder targetFoldern m    ��
�� 
alisk P    )qr��q Z    (st��us @    vwv n   xyx m    ��
�� 
versy 1    ��
�� 
ascrw m    zz �{{  2 . 5t r    |}| o    ���� 0 targetfolder targetFolder} o      ���� 0 theurl theURL��  u r    (~~ n   &��� I    &������ $0 fileurlwithpath_ fileURLWithPath_� ��~� l   "��}�|� n    "��� 1     "�{
�{ 
psxp� o     �z�z 0 targetfolder targetFolder�}  �|  �~  �  � n   ��� 4    �y�
�y 
pcls� m    �� ��� 
 N S U R L� m    �x
�x misccura o      �w�w 0 theurl theURLr �v�u
�v consnume�u  ��  ��  l r   , 7��� n  , 5��� I   0 5�t��s�t $0 fileurlwithpath_ fileURLWithPath_� ��r� o   0 1�q�q 0 targetfolder targetFolder�r  �s  � n  , 0��� 4   - 0�p�
�p 
pcls� m   . /�� ��� 
 N S U R L� m   , -�o
�o misccura� o      �n�n 0 theurl theURLi ��� r   8 A��� n  8 ?��� I   ; ?�m�l�k�m  0 defaultmanager defaultManager�l  �k  � n  8 ;��� o   9 ;�j�j 0 nsfilemanager NSFileManager� m   8 9�i
�i misccura� o      �h�h 0 filemanager fileManager� ��� r   B P��� n  B N��� I   C N�g��f�g �0 Bcontentsofdirectoryaturl_includingpropertiesforkeys_options_error_ BcontentsOfDirectoryAtURL_includingPropertiesForKeys_options_error_� ��� o   C D�e�e 0 theurl theURL� ��� J   D F�d�d  � ��� l  F I��c�b� n  F I��� o   G I�a�a P0 &nsdirectoryenumerationskipshiddenfiles &NSDirectoryEnumerationSkipsHiddenFiles� m   F G�`
�` misccura�c  �b  � ��_� l  I J��^�]� m   I J�\
�\ 
msng�^  �]  �_  �f  � o   B C�[�[ 0 filemanager fileManager� o      �Z�Z 0 allurls allURLs� ��� r   Q _��� c   Q ]��� l  Q Y��Y�X� n  Q Y��� I   R Y�W��V�W 0 valueforkey_ valueForKey_� ��U� m   R U�� ��� " l a s t P a t h C o m p o n e n t�U  �V  � o   Q R�T�T 0 allurls allURLs�Y  �X  � m   Y \�S
�S 
list� o      �R�R 0 thenames theNames� ��Q� L   ` b�� o   ` a�P�P 0 thenames theNames�Q  e "  expects alias or POSIX path   f ��� 8   e x p e c t s   a l i a s   o r   P O S I X   p a t h_ ��� l     �O�N�M�O  �N  �M  � ��� i   � ���� I      �L�K�J�L (0 createexportfolder createExportFolder�K  �J  � k     P�� ��� l     �I���I  � - ' generate new folder in Pictures folder   � ��� N   g e n e r a t e   n e w   f o l d e r   i n   P i c t u r e s   f o l d e r� ��� r     	��� l    ��H�G� n     ��� 1    �F
�F 
psxp� l    ��E�D� I    �C��B
�C .earsffdralis        afdr� m     �A
�A afdrpdoc�B  �E  �D  �H  �G  � o      �@�@ 20 picturesfolderposixpath picturesFolderPOSIXPath� ��� r   
 ��� b   
 ��� o   
 �?�? 20 picturesfolderposixpath picturesFolderPOSIXPath� I    �>�=�<�> 40 generateexportfoldername generateExportFolderName�=  �<  � o      �;�; 0 
targetpath 
targetPath� ��� r    ��� n   ��� I    �:�9�8�:  0 defaultmanager defaultManager�9  �8  � n   ��� o    �7�7 0 nsfilemanager NSFileManager� m    �6
�6 misccura� o      �5�5 0 filemanager fileManager� ��4� T    P�� k   # K�� ��� r   # .��� n  # ,��� I   $ ,�3��2�3 �0 Ccreatedirectoryatpath_withintermediatedirectories_attributes_error_ CcreateDirectoryAtPath_withIntermediateDirectories_attributes_error_� ��� o   $ %�1�1 0 
targetpath 
targetPath� ��� m   % &�0
�0 boovfals� ��� l  & '��/�.� m   & '�-
�- 
msng�/  �.  � ��,� l  ' (��+�*� m   ' (�)
�) 
msng�+  �*  �,  �2  � o   # $�(�( 0 filemanager fileManager� o      �'�' 0 	theresult 	theResult� ��&� Z   / K���%�� =  / 6��� l  / 4��$�#� =   / 4��� c   / 2   o   / 0�"�" 0 	theresult 	theResult m   0 1�!
�! 
long� m   2 3� �  �$  �#  � m   4 5�
� boovtrue� L   9 ? l  9 >�� c   9 > c   9 < o   9 :�� 0 
targetpath 
targetPath m   : ;�
� 
psxf m   < =�
� 
alis�  �  �%  � r   B K	 b   B I

 o   B C�� 20 picturesfolderposixpath picturesFolderPOSIXPath I   C H���� 40 generateexportfoldername generateExportFolderName�  �  	 o      �� 0 
targetpath 
targetPath�&  �4  �  l     ����  �  �    i   � � I      ��� &0 createnumericname createNumericName  o      �� 0 basename baseName  o      �� 00 numericsuffixseparator numericSuffixSeparator  o      �� ,0 minimumnumericlength minimumNumericLength  o      �� 0 	thisindex 	thisIndex � o      �
�
 &0 thisfileextension thisFileExtension�  �   k     F  l     �	 �	   3 - add leading zeros if indicated and necessary     �!! Z   a d d   l e a d i n g   z e r o s   i f   i n d i c a t e d   a n d   n e c e s s a r y "#" Z     5$%�&$ =    '(' o     �� ,0 minimumnumericlength minimumNumericLength( m    ��  % r    	)*) o    �� 0 	thisindex 	thisIndex* o      �� *0 thisincrementstring thisIncrementString�  & k    5++ ,-, r    ./. \    010 o    �� ,0 minimumnumericlength minimumNumericLength1 l   2��2 n    343 1    � 
�  
leng4 l   5����5 c    676 o    ���� 0 	thisindex 	thisIndex7 m    ��
�� 
ctxt��  ��  �  �  / o      ���� ,0 numberofleadingzeros numberOfLeadingZeros- 898 r    :;: o    ���� 0 	thisindex 	thisIndex; o      ���� *0 thisincrementstring thisIncrementString9 <��< Z    5=>����= @   ?@? o    ���� ,0 numberofleadingzeros numberOfLeadingZeros@ m    ���� > U     1ABA r   ' ,CDC b   ' *EFE m   ' (GG �HH  0F o   ( )���� *0 thisincrementstring thisIncrementStringD o      ���� *0 thisincrementstring thisIncrementStringB o   # $���� ,0 numberofleadingzeros numberOfLeadingZeros��  ��  ��  # IJI r   6 CKLK c   6 AMNM l  6 ?O����O b   6 ?PQP b   6 =RSR b   6 ;TUT b   6 9VWV o   6 7���� 0 basename baseNameW o   7 8���� 00 numericsuffixseparator numericSuffixSeparatorU o   9 :���� *0 thisincrementstring thisIncrementStringS m   ; <XX �YY  .Q o   = >���� &0 thisfileextension thisFileExtension��  ��  N m   ? @��
�� 
TEXTL o      ���� 0 newname newNameJ Z��Z L   D F[[ o   D E���� 0 newname newName��   \]\ l     ��������  ��  ��  ] ^_^ i   � �`a` I      �������� 40 generateexportfoldername generateExportFolderName��  ��  a k     xbb cdc l     ��ef��  e = 7 GET THE LOCALIZATION STRING FOR THE EXPORT FOLDER NAME   f �gg n   G E T   T H E   L O C A L I Z A T I O N   S T R I N G   F O R   T H E   E X P O R T   F O L D E R   N A M Ed hih r     jkj l    l����l n    mnm I    ��o���� 40 getlocalizedstringforkey getLocalizedStringForKeyo p��p m    qq �rr $ E X P O R T _ F O L D E R _ N A M E��  ��  n  f     ��  ��  k o      ���� D0  localizeddatestringforfoldername  localizedDateStringForFolderNamei sts l  	 	��uv��  u J Dset localizedDateStringForFolderName to "Photos Export %1$@ at %2$@"   v �ww � s e t   l o c a l i z e d D a t e S t r i n g F o r F o l d e r N a m e   t o   " P h o t o s   E x p o r t   % 1 $ @   a t   % 2 $ @ "t xyx l  	 	��z{��  z   convert to Cocoa string   { �|| 0   c o n v e r t   t o   C o c o a   s t r i n gy }~} r   	 � n  	 ��� I    ������� &0 stringwithstring_ stringWithString_� ���� o    ���� D0  localizeddatestringforfoldername  localizedDateStringForFolderName��  ��  � n  	 ��� o   
 ���� 0 nsstring NSString� m   	 
��
�� misccura� o      ���� .0 localizedstringobject localizedStringObject~ ��� l   ������  �   GET THE CURRENT DATE   � ��� *   G E T   T H E   C U R R E N T   D A T E� ��� r    ��� n   ��� I    �������� 0 date  ��  ��  � n   ��� o    ���� 0 nsdate NSDate� m    ��
�� misccura� o      ���� 0 
targetdate 
targetDate� ��� l   ������  � "  CREATE A NEW DATE FORMATTER   � ��� 8   C R E A T E   A   N E W   D A T E   F O R M A T T E R� ��� r    +��� n   )��� I   % )�������� 0 init  ��  ��  � n   %��� I   ! %�������� 	0 alloc  ��  ��  � n   !��� o    !���� "0 nsdateformatter NSDateFormatter� m    ��
�� misccura� o      ���� 0 dateformatter dateFormatter� ��� l  , ,������  � ) # SET FORMTTER TO DATE STRING FORMAT   � ��� F   S E T   F O R M T T E R   T O   D A T E   S T R I N G   F O R M A T� ��� n  , 2��� I   - 2�������  0 setdateformat_ setDateFormat_� ���� m   - .�� ���  y y y y - M M - d d��  ��  � o   , -���� 0 dateformatter dateFormatter� ��� l  3 3������  �   GENERATE THE DATE STRING   � ��� 2   G E N E R A T E   T H E   D A T E   S T R I N G� ��� r   3 ;��� l  3 9������ n  3 9��� I   4 9������� "0 stringfromdate_ stringFromDate_� ���� o   4 5���� 0 
targetdate 
targetDate��  ��  � o   3 4���� 0 dateformatter dateFormatter��  ��  � o      ���� &0 currentdatestring currentDateString� ��� l  < <������  �  > "2015-07-15"   � ���  >   " 2 0 1 5 - 0 7 - 1 5 "� ��� l  < <������  � * $ SET FORMATTER TO TIME STRING FORMAT   � ��� H   S E T   F O R M A T T E R   T O   T I M E   S T R I N G   F O R M A T� ��� n  < B��� I   = B�������  0 setdateformat_ setDateFormat_� ���� m   = >�� ���  H H : m m : s s   a��  ��  � o   < =���� 0 dateformatter dateFormatter� ��� l  C C������  �   GENERATE THE TIME STRING   � ��� 2   G E N E R A T E   T H E   T I M E   S T R I N G� ��� r   C K��� l  C I������ n  C I��� I   D I������� "0 stringfromdate_ stringFromDate_� ���� o   D E���� 0 
targetdate 
targetDate��  ��  � o   C D���� 0 dateformatter dateFormatter��  ��  � o      ���� &0 currenttimestring currentTimeString� ��� l  L L������  �  > "18:18:32 PM"   � ���  >   " 1 8 : 1 8 : 3 2   P M "� ��� l  L L������  � _ Y REPLACE PLACEHOLDER VARIABLES WITH GENERATED FILE-SYSTEM COMPLIANT DATE AND TIME STRINGS   � ��� �   R E P L A C E   P L A C E H O L D E R   V A R I A B L E S   W I T H   G E N E R A T E D   F I L E - S Y S T E M   C O M P L I A N T   D A T E   A N D   T I M E   S T R I N G S� ��� r   L U��� l  L S������ n  L S��� I   M S������� d0 0stringbyreplacingoccurrencesofstring_withstring_ 0stringByReplacingOccurrencesOfString_withString_� ��� m   M N�� ���  % 1 $ @� ���� o   N O���� &0 currentdatestring currentDateString��  ��  � o   L M���� .0 localizedstringobject localizedStringObject��  ��  � o      ���� .0 localizedstringobject localizedStringObject� ��� r   V a��� l  V _������ n  V _��� I   W _������� d0 0stringbyreplacingoccurrencesofstring_withstring_ 0stringByReplacingOccurrencesOfString_withString_� ��� m   W Z�� �    % 2 $ @� �� o   Z [���� &0 currenttimestring currentTimeString��  ��  � o   V W���� .0 localizedstringobject localizedStringObject��  ��  � o      ���� .0 localizedstringobject localizedStringObject�  l  b b����   1 +> "Photos Export 2015-07-15 at 18:37:23 PM"    � V >   " P h o t o s   E x p o r t   2 0 1 5 - 0 7 - 1 5   a t   1 8 : 3 7 : 2 3   P M "  r   b o	
	 n  b m I   c m��� d0 0stringbyreplacingoccurrencesofstring_withstring_ 0stringByReplacingOccurrencesOfString_withString_  m   c f �  : � m   f i �  .�  �   o   b c�� .0 localizedstringobject localizedStringObject
 o      �� 0 
foldername 
folderName  l  p p��   1 +> "Photos Export 2015-07-15 at 18.37.23 PM"    � V >   " P h o t o s   E x p o r t   2 0 1 5 - 0 7 - 1 5   a t   1 8 . 3 7 . 2 3   P M "  l  p p��     RETURN THE FOLDER NAME    � .   R E T U R N   T H E   F O L D E R   N A M E   L   p v!! l  p u"��" c   p u#$# o   p q�� 0 
foldername 
folderName$ m   q t�
� 
TEXT�  �    %�% l  w w�&'�  & 1 +> "Photos Export 2015-07-15 at 18.37.23 PM"   ' �(( V >   " P h o t o s   E x p o r t   2 0 1 5 - 0 7 - 1 5   a t   1 8 . 3 7 . 2 3   P M "�  _ )*) l     ����  �  �  * +,+ l      �-.�  -   MAPS HANDLER    . �//    M A P S   H A N D L E R  , 010 l     �~�}�|�~  �}  �|  1 232 i   � �454 I     �{�z6
�{ .PEXTMAPSnull��� ��� null�z  6 �y78
�y 
REPR7 o      �x�x "0 thesemediaitems theseMediaItems8 �w9�v
�w 
MTYP9 |�u�t:�s;�u  �t  : o      �r�r $0 maptypeindicator mapTypeIndicator�s  ; m      �q
�q 
msng�v  5 I     �p<�o�p (0 showlocationinmaps showLocationInMaps< =>= o    �n�n $0 maptypeindicator mapTypeIndicator> ?�m? o    �l�l "0 thesemediaitems theseMediaItems�m  �o  3 @A@ l     �k�j�i�k  �j  �i  A BCB i   � �DED I      �hF�g�h (0 showlocationinmaps showLocationInMapsF GHG o      �f�f $0 maptypeindicator mapTypeIndicatorH I�eI o      �d�d "0 thesemediaitems theseMediaItems�e  �g  E k    �JJ KLK l      �cMN�c  M   GET MAP TYPE    N �OO    G E T   M A P   T Y P E  L PQP Z    RS�b�aR H     TT E    UVU J     WW XYX m     �`�`  Y Z[Z m    �_�_ [ \�^\ m    �]�] �^  V o    �\�\ $0 maptypeindicator mapTypeIndicatorS r    ]^] m    �[�[  ^ o      �Z�Z $0 maptypeindicator mapTypeIndicator�b  �a  Q _`_ Z    &ab�Yca =   ded o    �X�X $0 maptypeindicator mapTypeIndicatore m    �W�W b l    fghf k     ii jkj r    lml m    �V�V m o      �U�U $0 maptypeindicator mapTypeIndicatork n�Tn r     opo m    �S
�S boovtruep o      �R�R &0 shouldshowtraffic shouldShowTraffic�T  g   hybrid   h �qq    h y b r i d�Y  c r   # &rsr m   # $�Q
�Q boovfalss o      �P�P &0 shouldshowtraffic shouldShowTraffic` tut l  ' '�O�N�M�O  �N  �M  u vwv O   ' �xyx k   + �zz {|{ l  + +�L}~�L  }  	 activate   ~ �    a c t i v a t e| ��� Z  + ?���K�J� =  + /��� o   + ,�I�I "0 thesemediaitems theseMediaItems� J   , .�H�H  � R   2 ;�G��F
�G .ascrerr ****      � ****� l  4 :��E�D� n  4 :��� I   5 :�C��B�C 40 getlocalizedstringforkey getLocalizedStringForKey� ��A� m   5 6�� ��� . N O _ M E D I A _ I T E M S _ P R O V I D E D�A  �B  �  f   4 5�E  �D  �F  �K  �J  � ��� r   @ D��� J   @ B�@�@  � o      �?�? (0 mediaitemsmetadata mediaItemsMetadata� ��� Z  E V���>�=� >  E J��� n   E H��� m   F H�<
�< 
pcls� o   E F�;�; "0 thesemediaitems theseMediaItems� m   H I�:
�: 
list� r   M R��� c   M P��� o   M N�9�9 "0 thesemediaitems theseMediaItems� m   N O�8
�8 
list� o      �7�7 "0 thesemediaitems theseMediaItems�>  �=  � ��� Y   W ���6���5� k   e ��� ��� r   e k��� n   e i��� 4   f i�4�
�4 
cobj� o   g h�3�3 0 i  � o   e f�2�2 "0 thesemediaitems theseMediaItems� o      �1�1 0 thismediaitem thisMediaItem� ��� r   l q��� l  l o��0�/� n   l o��� 1   m o�.
�. 
IPlo� o   l m�-�- 0 thismediaitem thisMediaItem�0  �/  � o      �,�, 0 thislocation thisLocation� ��+� Z   r ����*�)� >  r x��� o   r s�(�( 0 thislocation thisLocation� J   s w�� ��� m   s t�'
�' 
msng� ��&� m   t u�%
�% 
msng�&  � k   { ��� ��� r   { ���� l  { ~��$�#� n   { ~��� 1   | ~�"
�" 
pnam� o   { |�!�! 0 thismediaitem thisMediaItem�$  �#  � o      � �  0 	thistitle 	thisTitle� ��� Z   � ������ =  � ���� o   � ��� 0 	thistitle 	thisTitle� m   � ��
� 
msng� r   � ���� n   � ���� 1   � ��
� 
filn� o   � ��� 0 thismediaitem thisMediaItem� o      �� 0 	thistitle 	thisTitle�  �  � ��� r   � ���� l  � ����� n   � ���� 1   � ��
� 
ID  � o   � ��� 0 thismediaitem thisMediaItem�  �  � o      �� 0 thisid thisID� ��� r   � ���� n  � ���� I   � ����� \0 ,encodereservedcharactersusingpercentencoding ,encodeReservedCharactersUsingPercentEncoding� ��� o   � ��� 0 thisid thisID�  �  �  f   � �� o      �� 0 	encodedid 	encodedID� ��� r   � ���� l  � ����� b   � ���� m   � ��� ��� : p h o t o s : o n e u p / a s s e t ? a s s e t U u i d =� o   � ��� 0 	encodedid 	encodedID�  �  � o      �� 0 thisurl thisURL� ��
� r   � ���� J   � ��� ��� o   � ��	�	 0 	thistitle 	thisTitle� ��� o   � ��� 0 thislocation thisLocation� ��� o   � ��� 0 thisurl thisURL�  � l     ���� n      ���  ;   � �� o   � ��� (0 mediaitemsmetadata mediaItemsMetadata�  �  �
  �*  �)  �+  �6 0 i  � m   Z [�� � l  [ `��� � I  [ `�����
�� .corecnte****       ****� o   [ \���� "0 thesemediaitems theseMediaItems��  �  �   �5  � ���� Z  � �������� =  � ���� o   � ����� (0 mediaitemsmetadata mediaItemsMetadata� J   � �����  � R   � ������
�� .ascrerr ****      � ****� l  � ������� n  � ���� I   � �������� 40 getlocalizedstringforkey getLocalizedStringForKey� ���� m   � ��� ���   N O _ L O C A T I O N _ D A T A��  ��  �  f   � ���  ��  ��  ��  ��  ��  y m   ' (  �                                                                                  Phts  alis    0  Macintosh HD                   BD ����
Photos.app                                                     ����            ����  
 cu             Applications  !/:System:Applications:Photos.app/    
 P h o t o s . a p p    M a c i n t o s h   H D  System/Applications/Photos.app  / ��  w  l  � ���������  ��  ��    r   � � J   � �����   o      ���� 0 thesemapitems theseMapItems  Y   ��	��
��	 k   ��  r   � � n   � � 4   � ���
�� 
cobj o   � ����� 0 i   o   � ����� (0 mediaitemsmetadata mediaItemsMetadata o      ���� 0 thismetadata thisMetadata  s   � o   � ����� 0 thismetadata thisMetadata J        o      ���� (0 thismediaitemtitle thisMediaItemTitle  o      ���� 60 thislatitudelongitudepair thisLatitudeLongitudePair �� o      ���� $0 thismediaitemurl thisMediaItemURL��    s   !  o  ���� 60 thislatitudelongitudepair thisLatitudeLongitudePair! J      "" #$# o      ���� 0 thislatitude thisLatitude$ %��% o      ���� 0 thislongitude thisLongitude��   &'& l ��()��  (   create a coordinate   ) �** (   c r e a t e   a   c o o r d i n a t e' +,+ r  ,-.- K  (// ��01�� 0 latitude  0 o  ���� 0 thislatitude thisLatitude1 ��2���� 0 	longitude  2 o  !$���� 0 thislongitude thisLongitude��  . o      ���� 0 acoordinate aCoordinate, 343 l --��56��  5 ( " create an address book dictionary   6 �77 D   c r e a t e   a n   a d d r e s s   b o o k   d i c t i o n a r y4 898 r  -@:;: l -<<����< n -<=>= I  4<��?���� @0 dictionarywithobjects_forkeys_ dictionaryWithObjects_forKeys_? @A@ J  46����  A B��B J  68����  ��  ��  > n -4CDC o  04���� 0 nsdictionary NSDictionaryD m  -0��
�� misccura��  ��  ; o      ���� 00 aaddressbookdictionary aAddressBookDictionary9 EFE l  AA��GH��  G . ( To include actual address information:    H �II P   T o   i n c l u d e   a c t u a l   a d d r e s s   i n f o r m a t i o n :  F JKJ l AA��LM��  LTNset aAddressBookDictionary to (current application's NSDictionary's dictionaryWithObjects:{"28 Rose Street", "Hope Town", "State of Depression", "Utopia"} forKeys:{current application's kABAddressStreetKey, current application's kABAddressCityKey, current application's kABAddressStateKey, current application's kABAddressCountryKey})   M �NN� s e t   a A d d r e s s B o o k D i c t i o n a r y   t o   ( c u r r e n t   a p p l i c a t i o n ' s   N S D i c t i o n a r y ' s   d i c t i o n a r y W i t h O b j e c t s : { " 2 8   R o s e   S t r e e t " ,   " H o p e   T o w n " ,   " S t a t e   o f   D e p r e s s i o n " ,   " U t o p i a " }   f o r K e y s : { c u r r e n t   a p p l i c a t i o n ' s   k A B A d d r e s s S t r e e t K e y ,   c u r r e n t   a p p l i c a t i o n ' s   k A B A d d r e s s C i t y K e y ,   c u r r e n t   a p p l i c a t i o n ' s   k A B A d d r e s s S t a t e K e y ,   c u r r e n t   a p p l i c a t i o n ' s   k A B A d d r e s s C o u n t r y K e y } )K OPO l AA��QR��  Q E ? create a placemark using the coordinate and address dictionary   R �SS ~   c r e a t e   a   p l a c e m a r k   u s i n g   t h e   c o o r d i n a t e   a n d   a d d r e s s   d i c t i o n a r yP TUT r  AZVWV l AVX����X n AVYZY I  LV��[���� N0 %initwithcoordinate_addressdictionary_ %initWithCoordinate_addressDictionary_[ \]\ o  LO���� 0 acoordinate aCoordinate] ^��^ o  OR���� 00 aaddressbookdictionary aAddressBookDictionary��  ��  Z n AL_`_ I  HL������ 	0 alloc  �  �  ` n AHaba o  DH�� 0 mkplacemark MKPlacemarkb m  AD�
� misccura��  ��  W o      �� 0 
aplacemark 
aPlacemarkU cdc l [[�ef�  e , & create a map item using the placemark   f �gg L   c r e a t e   a   m a p   i t e m   u s i n g   t h e   p l a c e m a r kd hih r  [qjkj l [ml��l n [mmnm I  fm�o�� (0 initwithplacemark_ initWithPlacemark_o p�p o  fi�� 0 
aplacemark 
aPlacemark�  �  n n [fqrq I  bf���� 	0 alloc  �  �  r n [bsts o  ^b�� 0 	mkmapitem 	MKMapItemt m  [^�
� misccura�  �  k o      �� 0 amapitem aMapItemi uvu r  r{wxw o  rs�� (0 thismediaitemtitle thisMediaItemTitlex n     yzy o  vz�� 0 name  z o  sv�� 0 amapitem aMapItemv {|{ r  |�}~} l |��� n |���� I  ������  0 urlwithstring_ URLWithString_� ��� o  ���� $0 thismediaitemurl thisMediaItemURL�  �  � n |���� o  ��� 0 nsurl NSURL� m  |�
� misccura�  �  ~ o      �� 0 aurl aURL| ��� l ������  � #  set aMapItem's |url| to aURL   � ��� :   s e t   a M a p I t e m ' s   | u r l |   t o   a U R L� ��� l ������  � 5 / set aMapItem's |phoneNumber| to "555-555-5555"   � ��� ^   s e t   a M a p I t e m ' s   | p h o n e N u m b e r |   t o   " 5 5 5 - 5 5 5 - 5 5 5 5 "� ��� l ������  � 4 . add the new map item to the list of map items   � ��� \   a d d   t h e   n e w   m a p   i t e m   t o   t h e   l i s t   o f   m a p   i t e m s� ��� r  ����� o  ���� 0 amapitem aMapItem� l     ���� n      ���  ;  ��� o  ���� 0 thesemapitems theseMapItems�  �  �  �� 0 i  
 m   � ���  l  � ����� I  � ����
� .corecnte****       ****� o   � ��� (0 mediaitemsmetadata mediaItemsMetadata�  �  �  ��   ��� l ������  � . ( create a dictionary of the map settings   � ��� P   c r e a t e   a   d i c t i o n a r y   o f   t h e   m a p   s e t t i n g s� ��� r  ����� n ����� I  ������ @0 dictionarywithobjects_forkeys_ dictionaryWithObjects_forKeys_� ��� J  ���� ��� o  ���� &0 shouldshowtraffic shouldShowTraffic� ��� o  ���� $0 maptypeindicator mapTypeIndicator�  � ��� J  ���� ��� m  ���� ��� 6 M K L a u n c h O p t i o n s S h o w s T r a f f i c� ��� m  ���� ��� , M K L a u n c h O p t i o n s M a p T y p e�  �  �  � n ����� o  ���� 0 nsdictionary NSDictionary� m  ���
� misccura� o      ��  0 mapoptionsdict mapOptionsDict� ��� l ������  � 3 - display the location in the Maps application   � ��� Z   d i s p l a y   t h e   l o c a t i o n   i n   t h e   M a p s   a p p l i c a t i o n� ��� n ����� I  ������ D0  openmapswithitems_launchoptions_  openMapsWithItems_launchOptions_� ��� o  ���� 0 thesemapitems theseMapItems� ��� o  ����  0 mapoptionsdict mapOptionsDict�  �  � n ����� o  ���� 0 	mkmapitem 	MKMapItem� m  ���
� misccura� ��� O ����� I �����~
� .miscactvnull��� ��� null�  �~  � m  �����                                                                                  MAPX  alis    (  Macintosh HD                   BD ����Maps.app                                                       ����            ����  
 cu             Applications  /:System:Applications:Maps.app/     M a p s . a p p    M a c i n t o s h   H D  System/Applications/Maps.app  / ��  � ��}� L  ���� o  ���|�| "0 thesemediaitems theseMediaItems�}  C ��� l     �{�z�y�{  �z  �y  � ��� i   � ���� I      �x��w�x \0 ,encodereservedcharactersusingpercentencoding ,encodeReservedCharactersUsingPercentEncoding� ��v� o      �u�u 0 
sourcetext 
sourceText�v  �w  � k     ��� ��� l     �t���t  � w q create a Cocoa string from the passed AppleScript string, by calling the NSString class method stringWithString:   � ��� �   c r e a t e   a   C o c o a   s t r i n g   f r o m   t h e   p a s s e d   A p p l e S c r i p t   s t r i n g ,   b y   c a l l i n g   t h e   N S S t r i n g   c l a s s   m e t h o d   s t r i n g W i t h S t r i n g :� ��� r     
��� n    ��� I    �s��r�s &0 stringwithstring_ stringWithString_� ��q� o    �p�p 0 
sourcetext 
sourceText�q  �r  � n    ��� o    �o�o 0 nsstring NSString� m     �n
�n misccura� l     ��m�l� o      �k�k 0 sourcestring sourceString�m  �l  � ��� l   �j���j  � = 7 apply the indicated transformation to the Cooca string   � ��� n   a p p l y   t h e   i n d i c a t e d   t r a n s f o r m a t i o n   t o   t h e   C o o c a   s t r i n g� ��� r    6��� J    4�� ��� m    �� ���  %� ��� m    �� ���   � ��� m    �� ���  !� � � m     �  #   m     �  $  m    		 �

  &  m     �  '  m     �  (  m     �  )  m     �  *  m     �  +   m    !! �""  ,  #$# m    %% �&&  /$ '(' m    )) �**  :( +,+ m    -- �..  ;, /0/ m    !11 �22  =0 343 m   ! $55 �66  ?4 787 m   $ '99 �::  @8 ;<; m   ' *== �>>  [< ?@? m   * -AA �BB  ]@ C�iC m   - 0DD �EE  '�i  � o      �h�h (0 reservedcharacters reservedCharacters� FGF r   7 |HIH J   7 zJJ KLK m   7 :MM �NN  % 2 5L OPO m   : =QQ �RR  % 2 0P STS m   = @UU �VV  % 2 1T WXW m   @ CYY �ZZ  % 2 3X [\[ m   C F]] �^^  % 2 4\ _`_ m   F Iaa �bb  % 2 6` cdc m   I Lee �ff  % 2 7d ghg m   L Oii �jj  % 2 8h klk m   O Rmm �nn  % 2 9l opo m   R Uqq �rr  % 2 Ap sts m   U Xuu �vv  % 2 Bt wxw m   X [yy �zz  % 2 Cx {|{ m   [ ^}} �~~  % 2 F| � m   ^ a�� ���  % 3 A� ��� m   a d�� ���  % 3 B� ��� m   d g�� ���  % 3 D� ��� m   g j�� ���  % 3 F� ��� m   j m�� ���  % 4 0� ��� m   m p�� ���  % 5 B� ��� m   p s�� ���  % 5 D� ��g� m   s v�� ���  % 2 7�g  I o      �f�f (0 replacementstrings replacementStringsG ��� Y   } ���e���d� k   � ��� ��� r   � ���� n   � ���� 4   � ��c�
�c 
cobj� o   � ��b�b 0 i  � o   � ��a�a (0 reservedcharacters reservedCharacters� o      �`�` 0 searchstring searchString� ��� r   � ���� n   � ���� 4   � ��_�
�_ 
cobj� o   � ��^�^ 0 i  � o   � ��]�] (0 replacementstrings replacementStrings� o      �\�\ &0 replacementstring replacementString� ��[� r   � ���� l  � ���Z�Y� l  � ���X�W� n  � ���� I   � ��V��U�V d0 0stringbyreplacingoccurrencesofstring_withstring_ 0stringByReplacingOccurrencesOfString_withString_� ��� o   � ��T�T 0 searchstring searchString� ��S� o   � ��R�R &0 replacementstring replacementString�S  �U  � o   � ��Q�Q 0 sourcestring sourceString�X  �W  �Z  �Y  � l     ��P�O� o      �N�N 0 sourcestring sourceString�P  �O  �[  �e 0 i  � m   � ��M�M � l  � ���L�K� I  � ��J��I
�J .corecnte****       ****� o   � ��H�H (0 reservedcharacters reservedCharacters�I  �L  �K  �d  � ��� l  � ��G���G  � 5 / coerce from Cocoa string to AppleScript string   � ��� ^   c o e r c e   f r o m   C o c o a   s t r i n g   t o   A p p l e S c r i p t   s t r i n g� ��F� L   � ��� l  � ���E�D� c   � ���� o   � ��C�C 0 sourcestring sourceString� m   � ��B
�B 
TEXT�E  �D  �F  � ��� l     �A�@�?�A  �@  �?  � ��� l      �>���>  �   LOCALIZATION ROUTINE    � ��� ,   L O C A L I Z A T I O N   R O U T I N E  � ��=� i   � ���� I      �<��;�< 40 getlocalizedstringforkey getLocalizedStringForKey� ��:� o      �9�9 0 thiskey thisKey�:  �;  � L     �� l    ��8�7� I    �6��5
�6 .sysolocSutxt        TEXT� o     �4�4 0 thiskey thisKey�5  �8  �7  �=       )�3���2��������������������������������������3  � '�1�0�/�.�-�,�+�*�)�(�'�&�%�$�#�"�!� ���������������������
�1 
pimr�0 &0 defaultimagewidth defaultImageWidth
�/ .PUTLLIBPnull��� ��� null
�. .PUTLTALBnull��� ��� null
�- .PUTLTALNnull��� ��� null
�, .PUTLTABMnull��� ��� null
�+ .PUTLTFLDnull��� ��� null
�* .PUTLTFDNnull��� ��� null
�) .PUTLTFDRnull��� ��� null
�( .PUTLWAITnull��� ��� null
�' .PEXPQEXPnull��� ��� null
�& .PEXPEXORnull��� ��� null
�% .PEXPSQXPnull��� ��� null
�$ .PEXPKYXPnull��� ��� null�# 0 newblankslide newBlankSlide�" *0 addimagefiletoslide addImageFileToSlide�! (0 getimagedimensions getImageDimensions�  60 extractkmditemdescription extractkMDItemDescription
� .PEXPNUXPnull��� ��� null
� .PEXPPGXPnull��� ��� null� 40 getnamesofspecifieditems getNamesOfSpecifiedItems� &0 gettoplevelalbums getTopLevelAlbums� (0 gettoplevelfolders getTopLevelFolders� 00 gettoplevelfoldernamed getTopLevelFolderNamed� .0 gettoplevelalbumnamed getTopLevelAlbumNamed� >0 isthisfolderoralbumattoplevel isThisFolderOrAlbumAtTopLevel� V0 )deriveinyternallibrarypathtoalbumorfolder )deriveInyternalLibraryPathToAlbumOrFolder� 0 waitforphotos waitForPhotos� 0 quickexport quickExport� *0 sortedlistofitemsin sortedListOfItemsIn�  0 revealinfinder revealInFinder� (0 namesoffolderitems namesOfFolderItems� (0 createexportfolder createExportFolder� &0 createnumericname createNumericName� 40 generateexportfoldername generateExportFolderName
� .PEXTMAPSnull��� ��� null� (0 showlocationinmaps showLocationInMaps� \0 ,encodereservedcharactersusingpercentencoding ,encodeReservedCharactersUsingPercentEncoding� 40 getlocalizedstringforkey getLocalizedStringForKey� �
 �
     �	�
�	 
cobj    � 
� 
frmk�   �	�
� 
cobj	 

   � 
� 
frmk�   ��
� 
cobj    � !
� 
frmk�   � ��
�  
cobj    �� '
�� 
frmk��   ����
�� 
cobj    ��
�� 
osax��   �� 1��
�� 
vers��  �2�� �� K������
�� .PUTLLIBPnull��� ��� null��  �� ����
�� 
REPR�� 0 targetobject targetObject ��
�� 
APID {�������� (0 shouldappenditemid shouldAppendItemID��  
�� boovfals ����
�� 
DELM {�������� &0 pathitemdelimiter pathItemDelimiter��  
�� 
msng��   �������������� 0 targetobject targetObject�� (0 shouldappenditemid shouldAppendItemID�� &0 pathitemdelimiter pathItemDelimiter�� "0 locationstrings locationStrings�� 0 i  �� 0 
thisobject 
thisObject �� c������������
�� 
msng�� 40 getlocalizedstringforkey getLocalizedStringForKey
�� 
pcls
�� 
list
�� .corecnte****       ****
�� 
cobj�� V0 )deriveinyternallibrarypathtoalbumorfolder )deriveInyternalLibraryPathToAlbumOrFolder�� T��  )�k+ E�Y hO��,�  2jvE�O $k�j kh ��/E�O*���m+ �6F[OY��O�Y 
*���m+ � �� �������
�� .PUTLTALBnull��� ��� null��  ��     ���� &0 gettoplevelalbums getTopLevelAlbums�� *j+  � �� �������
�� .PUTLTALNnull��� ��� null��  ��     ������ &0 gettoplevelalbums getTopLevelAlbums�� 40 getnamesofspecifieditems getNamesOfSpecifiedItems�� **j+  k+ � �� �������
�� .PUTLTABMnull��� ��� null��  �� ������
�� 
naME�� 0 thisname thisName��   ���� 0 thisname thisName ���� .0 gettoplevelalbumnamed getTopLevelAlbumNamed�� *�k+  � �� �������
�� .PUTLTFLDnull��� ��� null��  ��     ���� (0 gettoplevelfolders getTopLevelFolders�� *j+  � �� ����� ��
�� .PUTLTFDNnull��� ��� null��  ��      ��� (0 gettoplevelfolders getTopLevelFolders� 40 getnamesofspecifieditems getNamesOfSpecifiedItems�� **j+  k+ � � ���!"�
� .PUTLTFDRnull��� ��� null�  � ���
� 
naME� 0 thisname thisName�  ! �� 0 thisname thisName" �� 00 gettoplevelfoldernamed getTopLevelFolderNamed� *�k+  � � ���#$�
� .PUTLWAITnull��� ��� null�  � �%�
� 
WDUR% {���� 40 timeoutdurationinseconds timeoutDurationInSeconds�  �,�  # �� 40 timeoutdurationinseconds timeoutDurationInSeconds$ ���,� 0 waitforphotos waitForPhotos� �� �E�Y hO*�k+ � ���&'�
� .PEXPQEXPnull��� ��� null�  � ��(
� 
EXOF� "0 thesemediaitems theseMediaItems( �)*
� 
EXID) {���� &0 shouldnameusingid shouldNameUsingID�  
� boovfals* �+,
� 
EXRF+ {���� ,0 shouldreturnfilerefs shouldReturnFileRefs�  
� boovtrue, �-�
� 
EXRV- {���� ,0 shouldrevealinfinder shouldRevealInFinder�  
� boovfals�  & 
����������� "0 thesemediaitems theseMediaItems� &0 shouldnameusingid shouldNameUsingID� ,0 shouldreturnfilerefs shouldReturnFileRefs� ,0 shouldrevealinfinder shouldRevealInFinder� .0 namingmethodindicator namingMethodIndicator� .0 shouldexportoriginals shouldExportOriginals� 0 basename baseName� 0 thisseparator thisSeparator� "0 thisdigitlength thisDigitLength�  0 startingnumber startingNumber' ��+�HN��
� 
pcls
� 
list� 40 getlocalizedstringforkey getLocalizedStringForKey� 	� 0 quickexport quickExport� Z��,� 
��&E�Y hO�jv  )j)�k+ Y hO�f  kE�Y lE�OfE�O�E�O�E�OmE�OkE�O*����������+ � �p��./�
� .PEXPEXORnull��� ��� null�  � ��0
� 
EXOF� "0 thesemediaitems theseMediaItems0 �12
� 
EXRF1 {��~�}� ,0 shouldreturnfilerefs shouldReturnFileRefs�~  
�} boovtrue2 �|3�{
�| 
EXRV3 {�z�y�x�z ,0 shouldrevealinfinder shouldRevealInFinder�y  
�x boovfals�{  . 	�w�v�u�t�s�r�q�p�o�w "0 thesemediaitems theseMediaItems�v ,0 shouldreturnfilerefs shouldReturnFileRefs�u ,0 shouldrevealinfinder shouldRevealInFinder�t .0 shouldexportoriginals shouldExportOriginals�s 0 basename baseName�r 0 thisseparator thisSeparator�q "0 thisdigitlength thisDigitLength�p  0 startingnumber startingNumber�o .0 namingmethodindicator namingMethodIndicator/ �n�m��l���k�j
�n 
pcls
�m 
list�l 40 getlocalizedstringforkey getLocalizedStringForKey�k 	�j 0 quickexport quickExport� N��,� 
��&E�Y hO�jv  )j)�k+ Y hOeE�O�E�O�E�OmE�OkE�OkE�O*����������+ � �i��h�g45�f
�i .PEXPSQXPnull��� ��� null�h  �g �e�d6
�e 
EXOF�d "0 thesemediaitems theseMediaItems6 �c78
�c 
EXBN7 {�b�a��b 0 basename baseName�a  8 �`9:
�` 
EXSN9 {�_�^�]�_  0 startingnumber startingNumber�^  
�] 
msng: �\;<
�\ 
EXSP; {�[�Z�Y�[ "0 separatorstring separatorString�Z  
�Y 
msng< �X=>
�X 
EXSL= {�W�V�U�W  0 sequencelength sequenceLength�V  
�U 
msng> �T?@
�T 
EXRF? {�S�R�Q�S ,0 shouldreturnfilerefs shouldReturnFileRefs�R  
�Q boovtrue@ �PA�O
�P 
EXRVA {�N�M�L�N ,0 shouldrevealinfinder shouldRevealInFinder�M  
�L boovfals�O  4 	�K�J�I�H�G�F�E�D�C�K "0 thesemediaitems theseMediaItems�J 0 basename baseName�I  0 startingnumber startingNumber�H "0 separatorstring separatorString�G  0 sequencelength sequenceLength�F ,0 shouldreturnfilerefs shouldReturnFileRefs�E ,0 shouldrevealinfinder shouldRevealInFinder�D .0 namingmethodindicator namingMethodIndicator�C .0 shouldexportoriginals shouldExportOriginals5 �B�A �@�?�>�=�<�;�:�9�8�7
�B 
pcls
�A 
list�@ 40 getlocalizedstringforkey getLocalizedStringForKey
�? 
msng
�> 
long
�= 
bool
�< 
spac
�; .corecnte****       ****
�: 
TEXT
�9 
leng�8 	�7 0 quickexport quickExport�f ���,� 
��&E�Y hO�jv  )j)�k+ Y hO��  )�k+ E�Y hO��  kE�Y ��,�
 �j�& kE�Y hO��  �E�Y hO��  �j 
E�O��&�,E�Y hOjE�OfE�O*����������+ � �6{�5�4BC�3
�6 .PEXPKYXPnull��� ��� null�5  �4 �2�1D
�2 
EXUS�1 "0 thesemediaitems theseMediaItemsD �0EF
�0 
NDOCE {�/�.�-�/ 0 templatename templateName�.  
�- 
msngF �,GH
�, 
EXKMG {�+�*�)�+ (0 documentdimensions documentDimensions
�* 
list
�) 
msngH �(I�'
�( 
EXKII {�&�%�$�& "0 newslideforeach newSlideForEach�%  
�$ boovtrue�'  B �#�"�!� ���������������������# "0 thesemediaitems theseMediaItems�" 0 templatename templateName�! (0 documentdimensions documentDimensions�  "0 newslideforeach newSlideForEach� 00 existingdocumentstatus existingDocumentStatus� *0 shouldusedimensions shouldUseDimensions� $0 newdocumentwidth newDocumentWidth� &0 newdocumentheight newDocumentHeight� .0 shouldusetemplatename shouldUseTemplateName� 0 
themenames 
themeNames� .0 shouldmakenewdocument shouldMakeNewDocument� (0 exportedimagefiles exportedImageFiles� 0 thisdocument thisDocument� (0 newslidereferences newSlideReferences� 0 i  � 0 thisimagefile thisImageFile� 0 	thisslide 	thisSlide� 0 filemanager fileManager� 0 thisposixpath thisPOSIXPath� ,0 pathoffoldertodelete pathOfFolderToDelete� 0 resultingurl resultingURL� $0 urlofitemtotrash URLOfItemToTrash� &0 completitiontitle completitionTitle� *0 completitionmessage completitionMessageC =��
��	������������� �����3C����T��������������������������8������j���������������������������
� 
pcls
�
 
list�	 40 getlocalizedstringforkey getLocalizedStringForKey
� 
capp
� kfrmID  
� 
prun
� .ascrnoop****      � ****
� 
docu
� .coredoexnull���     ****
� 
msng
� 
bool
�  .corecnte****       ****
�� 
cobj
�� 
long
�� 
Knth
�� 
pnam�� 	�� 0 quickexport quickExport
�� .miscactvnull��� ��� null
�� 
kocl
�� 
prdt
�� 
Kndt
�� 
sitw
�� 
sith�� �� 
�� .corecrel****      � null�� 0 newblankslide newBlankSlide�� �� *0 addimagefiletoslide addImageFileToSlide
�� misccura�� 0 nsfilemanager NSFileManager��  0 defaultmanager defaultManager
�� 
psxp�� 0 nsstring NSString�� &0 stringwithstring_ stringWithString_�� F0 !stringbydeletinglastpathcomponent !stringByDeletingLastPathComponent�� 	0 NSURL  �� $0 fileurlwithpath_ fileURLWithPath_�� P0 &trashitematurl_resultingitemurl_error_ &trashItemAtURL_resultingItemURL_error_
�� 
appr
�� .sysonotfnull��� ��� TEXT�3=��,� 
��&E�Y hO�jv  )j)�k+ Y hO)���0�,f  )���0 *j 	UY hO)���0 *�k/j E�UO�� 
 �� �& fE�Y Z�j l )j)a k+ Y >�E[a k/EQ�Z[a l/EQ�ZO��,a 
 ��,a �& )j)a k+ Y hOeE�O�� 
 	�a  �& fE�Y 5)�a �0 *a -a ,E�UO�� )j)a k+ �%Y hOeE�O�e 
 �e �&
 �f �& eE�Y fE�O*�eflfa a kka + E�O�e  �)�a �0 �*j O�e 	 �e �& **a  �a !a "*a �/a #�a $�a %a & 'E�Y \�f 	 �e �& !*a  �a !a #�a $�a &a & 'E�Y /�e 	 �f �&  *a  �a !a "*a �/la & 'E�Y hUY )�a (�0 *j O*�k/E�UOjvE�O�e  L)�a )�0 > ;k�j kh �a �/E�O)�k+ *E^ O)�] �eea ++ ,O] �6F[OY��UY H)�a -�0 =)j+ *E^ O )k�j kh �a �/E�O)�] �ffa ++ ,[OY��O] �6FUOa .a /,j+ 0E^ O�a 1,E^ Oa .a 2,] k+ 3E^ O] j+ 4E^ O�E^ Oa .a 5,] k+ 6E^ O] ] ] �m+ 7O)a 8k+ E^ O)a 9k+ E^ O] a :] l ;O)�a <�0 �U� �������JK���� 0 newblankslide newBlankSlide�� ��L�� L  ���� 0 thisdocument thisDocument��  J ���������� 0 thisdocument thisDocument�� .0 thesemasterslidenames theseMasterSlideNames�� ,0 shoulduseblankmaster shouldUseBlankMaster�� 0 	thisslide 	thisSlideK ��B���������������������%����������
�� 
capp
�� kfrmID  
�� 
msng
�� 
bool
�� 
docu
�� 
KnMs
�� 
pnam
�� 
kocl
�� 
KnSd
�� 
prdt
�� 
smas�� 
�� .corecrel****      � null
�� 
fmti
�� 
pLck
�� .coredelonull���     obj �� |)���0 t�� 
 �� �& *�k/E�Y hO� V*�-�,E�O�� eE�Y fE�O�e  *����*��/l� E�Y !*��l E�Of�a -a ,FO�a -j O�UU� ��I����MN���� *0 addimagefiletoslide addImageFileToSlide�� ��O�� O  ���������� 0 thisdocument thisDocument�� 0 	thisslide 	thisSlide�� 0 thisimagefile thisImageFile� 0 scaletofill scaleToFill� <0 shouldusedescriptionfornotes shouldUseDescriptionForNotes��  M ��������������������� 0 thisdocument thisDocument� 0 	thisslide 	thisSlide� 0 thisimagefile thisImageFile� 0 scaletofill scaleToFill� <0 shouldusedescriptionfornotes shouldUseDescriptionForNotes� 0 queryresult queryResult� 0 
imagewidth 
imageWidth� 0 imageheight imageHeight� 0 documentwidth documentWidth�  0 documentheight documentHeight� 0 	thisimage 	thisImage�  0 thisimagewidth thisImageWidth� "0 thisimageheight thisImageHeight�  0 newimageheight newImageHeight� 0 newimagewidth newImageWidth�  0 verticaloffset verticalOffset� $0 horizontaloffset horizontalOffset� "0 thisdescription thisDescription� 0 errormessage errorMessage� 0 errornumber errorNumberN w�d����������������������P���
� 
msng
� 
bool
� 
docu
� .miscactvnull��� ��� null
� 
psxp� (0 getimagedimensions getImageDimensions
� 
errn�'
� 
cobj
� 
sitw
� 
sith
� 
kocl
� 
imag
� 
prdt
� 
file� 
� .corecrel****      � null
� 
sipo� � 60 extractkmditemdescription extractkMDItemDescription
� 
dscr
� 
ksnt� 0 errormessage errorMessageP ���
� 
errn� 0 errornumber errorNumber�  ���
� 
mesS
� .sysodisAaleR        TEXT��`�\0�� 
 �� �& *�k/E�Y hO*j O)��,k+ E�O�f  )��lhY �E[�k/EQ�Z[�l/EQ�ZO� *�,E�O*�,E�UO� ˣf  ;*���a �la  E�O�  *�,E�O*�,E�O��l!��l!lv*a ,FUY c�� �!E�O�� �E�O��l!i E�OjE^ Y �E�O�� �!E�OjE�O��l!i E^ O*���a ���a ] �lva a  E�O)�k+ E^ O] �a ,FO�e  ] *a ,FY hUW +X  ] a  ] a ] l Y hO)�a lhU� �}��QR�� (0 getimagedimensions getImageDimensions� �S� S  �� 00 thisimagefileposixpath thisImageFilePOSIXPath�  Q ������ 00 thisimagefileposixpath thisImageFilePOSIXPath� 0 imageheight imageHeight� 0 
imagewidth 
imageWidth� *0 imagedimensionsdata imageDimensionsData� 0 	thisimage 	thisImageR ���������~�}�|�{�z�y�x�
� 
strq
� .sysoexecTEXT���     TEXT
� 
spac
�~ .ascrnoop****      � ****
�} .aevtodocnull  �    alis
�| 
dmns
�{ 
cobj
�z .coreclosnull���     obj �y  �x  � � ���,%j O��%��,%j E�O��%��,%j E�O��lvE�O�� S� K*j O '�j 	E�O��,E[�k/EQ�Z[�l/EQ�ZO�j W X   
�j W X  hO)j�UY hO��lvW 	X  f� �w��v�uTU�t�w 60 extractkmditemdescription extractkMDItemDescription�v �sV�s V  �r�r 0 thisimagefile thisImageFile�u  T �q�p�o�q 0 thisimagefile thisImageFile�p (0 thisimageposixpath thisImagePOSIXPath�o *0 embeddeddescription embeddedDescriptionU 
�n�m�l"�k�j'
�n 
psxp
�m 
strq
�l .sysoexecTEXT���     TEXT�k  �j  �t : 1��,E�O��,%j O��,%j E�O��  �E�Y hO�W 	X  �� �i.�h�gWX�f
�i .PEXPNUXPnull��� ��� null�h  �g �e�dY
�e 
EXUS�d "0 thesemediaitems theseMediaItemsY �cZ�b
�c 
NDOCZ {�a�`�_�a 0 templatename templateName�`  
�_ 
msng�b  W �^�]�\�[�Z�Y�X�W�V�U�T�S�R�Q�P�O�^ "0 thesemediaitems theseMediaItems�] 0 templatename templateName�\ (0 exportedimagefiles exportedImageFiles�[ 0 thisdocument thisDocument�Z 0 templatenames templateNames�Y "0 imagereferences imageReferences�X 0 i  �W 0 thisimagefile thisImageFile�V 0 	thisimage 	thisImage�U 0 filemanager fileManager�T 0 thisposixpath thisPOSIXPath�S ,0 pathoffoldertodelete pathOfFolderToDelete�R 0 resultingurl resultingURL�Q $0 urlofitemtotrash URLOfItemToTrash�P &0 completitiontitle completitionTitle�O *0 completitionmessage completitionMessageX 4�N�MO�Lgk�K�J�I��H�G�F�E�D�C��B�A�@�?�>�=�<��;�:�9�8�7�6�5�4�3�2�1�0�/�.�-�,�+�*�)�(�'\g�&�%x
�N 
pcls
�M 
list�L 40 getlocalizedstringforkey getLocalizedStringForKey�K 	�J 0 quickexport quickExport
�I 
capp
�H kfrmID  
�G .miscactvnull��� ��� null
�F 
docu
�E .coredoexnull���     ****
�D 
msng
�C 
bool
�B 
kocl
�A .corecrel****      � null
�@ 
tmpl
�? 
pnam
�> 
prdt
�= 
Tmpl�< 
�; 
NmAS
�: .corecnte****       ****
�9 
cobj
�8 
imag
�7 
file
�6 
sitw
�5 
sipo�4 d
�3 
pSOp�2 60 extractkmditemdescription extractkMDItemDescription
�1 
dscr
�0 misccura�/ 0 nsfilemanager NSFileManager�.  0 defaultmanager defaultManager
�- 
psxp�, 0 nsstring NSString�+ &0 stringwithstring_ stringWithString_�* F0 !stringbydeletinglastpathcomponent !stringByDeletingLastPathComponent�) 	0 NSURL  �( $0 fileurlwithpath_ fileURLWithPath_�' P0 &trashitematurl_resultingitemurl_error_ &trashItemAtURL_resultingItemURL_error_
�& 
appr
�% .sysonotfnull��� ��� TEXT�f̠�,� 
��&E�Y hO�jv  )j)�k+ Y hO*�eflf��kk�+ E�O)���0 �*j O*�k/j 
 ���& l�� X�a   *a �l E�Y ?*a -a ,E�O��  *a �a a *a �/la  E�Y )j)a k+ �%Y *a �l E�Y hUO)�a �0 �jvE�O*�k/ w*a , n kk�j kh �a �/E�O*a a a a �la  E�O� /b  *a ,FOjjlv*a  ,FOa !*a ",FO)�k+ #*a $,FUO��6F[OY��UUUOa %a &,j+ 'E�O�a (,E�Oa %a ),�k+ *E�O�j+ +E�O�E�Oa %a ,,�k+ -E�O����m+ .O)a /k+ E�O)a 0k+ E�O�a 1�l 2O)�a 3�0 �U� �$�#�"[\�!
�$ .PEXPPGXPnull��� ��� null�#  �" � �]
�  
EXUS� "0 thesemediaitems theseMediaItems] �^�
� 
NDOC^ {���� 0 templatename templateName�  
� 
msng�  [ ����������������
� "0 thesemediaitems theseMediaItems� 0 templatename templateName� (0 exportedimagefiles exportedImageFiles� 0 thisdocument thisDocument� 0 templatenames templateNames� "0 imagereferences imageReferences� 0 i  � 0 thisimagefile thisImageFile� 0 	thisimage 	thisImage� 0 filemanager fileManager� 0 thisposixpath thisPOSIXPath� ,0 pathoffoldertodelete pathOfFolderToDelete� 0 resultingurl resultingURL� $0 urlofitemtotrash URLOfItemToTrash� &0 completitiontitle completitionTitle�
 *0 completitionmessage completitionMessage\ 4�	��������	���� �������������������	
	^������������������������������������������	�	�����	�
�	 
pcls
� 
list� 40 getlocalizedstringforkey getLocalizedStringForKey� 	� 0 quickexport quickExport
� 
capp
� kfrmID  
� .miscactvnull��� ��� null
� 
docu
�  .coredoexnull���     ****
�� 
msng
�� 
bool
�� 
kocl
�� .corecrel****      � null
�� 
tmpl
�� 
pnam
�� 
prdt
�� 
Tmpl�� 
�� 
pCpa
�� .corecnte****       ****
�� 
cobj
�� 
imag
�� 
file
�� 
sitw
�� 
sipo�� d
�� 
pSOp�� 60 extractkmditemdescription extractkMDItemDescription
�� 
dscr
�� misccura�� 0 nsfilemanager NSFileManager��  0 defaultmanager defaultManager
�� 
psxp�� 0 nsstring NSString�� &0 stringwithstring_ stringWithString_�� F0 !stringbydeletinglastpathcomponent !stringByDeletingLastPathComponent�� 	0 NSURL  �� $0 fileurlwithpath_ fileURLWithPath_�� P0 &trashitematurl_resultingitemurl_error_ &trashItemAtURL_resultingItemURL_error_
�� 
appr
�� .sysonotfnull��� ��� TEXT�!̠�,� 
��&E�Y hO�jv  )j)�k+ Y hO*�eflf��kk�+ E�O)���0 �*j O*�k/j 
 ���& l�� X�a   *a �l E�Y ?*a -a ,E�O��  *a �a a *a �/la  E�Y )j)a k+ �%Y *a �l E�Y hUO)�a �0 �jvE�O*�k/ w*a , n kk�j kh �a �/E�O*a a a a �la  E�O� /b  *a ,FOjjlv*a  ,FOa !*a ",FO)�k+ #*a $,FUO��6F[OY��UUUOa %a &,j+ 'E�O�a (,E�Oa %a ),�k+ *E�O�j+ +E�O�E�Oa %a ,,�k+ -E�O����m+ .O)a /k+ E�O)a 0k+ E�O�a 1�l 2O)�a 3�0 �U� ��	�����_`���� 40 getnamesofspecifieditems getNamesOfSpecifiedItems�� ��a�� a  ���� 0 
theseitems 
theseItems��  _ �������� 0 
theseitems 
theseItems�� 0 
thesenames 
theseNames�� 0 i  ` ����	���������,�� 0 waitforphotos waitForPhotos
�� .corecnte****       ****
�� 
cobj
�� 
pnam�� 1*�k+ O� &jvE�O k�j kh ��/�,�6F[OY��O�U� ��
����bc���� &0 gettoplevelalbums getTopLevelAlbums��  ��  b ���������� 0 thesealbums theseAlbums��  0 toplevelalbums topLevelAlbums�� 0 i  �� 0 	thisalbum 	thisAlbumc ����
2������������,�� 0 waitforphotos waitForPhotos
�� 
IPal
�� .corecnte****       ****
�� 
cobj
�� 
pare
�� 
msng�� E*�k+ O� :*�-E�OjvE�O )k�j kh ��/E�O��,�  	��6FY h[OY��O�U� ��
8����de��� (0 gettoplevelfolders getTopLevelFolders��  ��  d ����� 0 thesefolders theseFolders� "0 toplevelfolders topLevelFolders� 0 i  � 0 
thisfolder 
thisFoldere ��
h������,� 0 waitforphotos waitForPhotos
� 
IPfd
� .corecnte****       ****
� 
cobj
� 
pare
� 
msng� E*�k+ O� :*�-E�OjvE�O )k�j kh ��/E�O��,�  	��6FY h[OY��O�U� �
n��fg�� 00 gettoplevelfoldernamed getTopLevelFolderNamed� �h� h  �� 0 thisname thisName�  f ����� 0 thisname thisName� 0 thesefolders theseFolders� 0 i  � 0 
thisfolder 
thisFolderg ����
�� (0 gettoplevelfolders getTopLevelFolders
� .corecnte****       ****
� 
cobj
� 
pnam� 8*j+  E�O 'k�j kh ��/E�O��,�  �Y h[OY��O)j�%� �
���ij�� .0 gettoplevelalbumnamed getTopLevelAlbumNamed� �k� k  �� 0 thisname thisName�  i ����� 0 thisname thisName� 0 thesealbums theseAlbums� 0 i  � 0 	thisalbum 	thisAlbumj ����
�� &0 gettoplevelalbums getTopLevelAlbums
� .corecnte****       ****
� 
cobj
� 
pnam� 8*j+  E�O 'k�j kh ��/E�O��,�  �Y h[OY��O)j�%� �
���lm�� >0 isthisfolderoralbumattoplevel isThisFolderOrAlbumAtTopLevel� �n� n  �� 0 thisitem thisItem�  l �� 0 thisitem thisItemm ������������,�� 0 waitforphotos waitForPhotos
�� 
pare
�� 
msng��  ��  � $*�k+ O ��,�  eY fW 	X  f� ��
�����op���� V0 )deriveinyternallibrarypathtoalbumorfolder )deriveInyternalLibraryPathToAlbumOrFolder�� ��q�� q  �������� 0 targetobject targetObject�� 0 appenditemid appendItemID�� &0 pathitemdelimiter pathItemDelimiter��  o 	��������~�}�|�{�z�� 0 targetobject targetObject�� 0 appenditemid appendItemID�� &0 pathitemdelimiter pathItemDelimiter� $0 targetobjectname targetObjectName�~  0 targetobjectid targetObjectID�} 0 pathtoobject pathToObject�| 0 
foldername 
folderName�{ 0 thisid thisID�z 0 	albumname 	albumNamep �y�x��w
��v�u�t�s�r�q13�p�o�n�m�y,�x 0 waitforphotos waitForPhotos
�w .coredoexnull���     ****
�v 
pcls
�u 
IPal
�t 
pnam
�s 
IPfd
�r 
ID  
�q 
spac
�p 
pare
�o kfrmID  �n  �m  �� �*�k+ O� Ǡj  	)j�Y hO��,�  
��,E�Y ��,�  
��,E�Y )j�O�e  ��,E�O��%�%�%�%E�Y �E�O ohZ ^��,�,�  $��,�,E�O��%�%E�O��,�,E�O*��0E�Y /��,�,�  $��,�,E�O��%�%E�O��,�,E�O*��0E�Y hW 	X  �[OY��U� �l��k�jrs�i�l 0 waitforphotos waitForPhotos�k �ht�h t  �g�g 40 timeoutdurationinseconds timeoutDurationInSeconds�j  r �f�e�d�f 40 timeoutdurationinseconds timeoutDurationInSeconds�e ,0 currenttimeinseconds currentTimeInSeconds�d  0 mediaitemcount mediaItemCounts ��c�b�a�`�_�^�]�\�[�Z�Y�X
�c 
prun
�b .ascrnoop****      � ****
�a misccura
�` .misccurdldt    ��� null
�_ 
time
�^ 
IPmi
�] .corecnte****       ****
�\ .sysodelanull��� ��� nmbr�[  �Z  
�Y 
errn�X���i q��,f  f� *j UO� L*j �,E�O ?h*j �,�� $� *�-j E�UO�j eY kj W X 	 
h[OY��UO)��lhY hOe� �W��V�Uuv�T�W 0 quickexport quickExport�V �Sw�S 	w 	 �R�Q�P�O�N�M�L�K�J�R 0 
theseitems 
theseItems�Q :0 returnfilereferencesboolean returnFileReferencesBoolean�P 00 revealindicatorboolean revealIndicatorBoolean�O .0 namingmethodindicator namingMethodIndicator�N .0 shouldexportoriginals shouldExportOriginals�M 0 basename baseName�L 0 thisseparator thisSeparator�K "0 thisdigitlength thisDigitLength�J  0 startingnumber startingNumber�U  u �I�H�G�F�E�D�C�B�A�@�?�>�=�<�;�:�9�8�7�6�5�4�3�2�1�0�/�.�-�I 0 
theseitems 
theseItems�H :0 returnfilereferencesboolean returnFileReferencesBoolean�G 00 revealindicatorboolean revealIndicatorBoolean�F .0 namingmethodindicator namingMethodIndicator�E .0 shouldexportoriginals shouldExportOriginals�D 0 basename baseName�C 0 thisseparator thisSeparator�B "0 thisdigitlength thisDigitLength�A  0 startingnumber startingNumber�@ "0 videoextensions VideoExtensions�? 0 exportfolder exportFolder�> .0 exportfolderposixpath exportFolderPOSIXPath�= 0 filemanager fileManager�< 0 i  �; 0 thisitem thisItem�: 0 thisid thisID�9 0 currentname currentName�8 0 thisitemname thisItemName�7 ,0 namewithoutextension nameWithoutExtension�6 &0 internalextension internalExtension�5 0 filetestpath fileTestPath�4 60 extensionexportedbyphotos extensionExportedByPhotos�3  0 targetfilename targetFileName�2 0 	thisindex 	thisIndex�1 0 newname newName�0 "0 currentfilepath currentFilePath�/ 0 newfilepath newFilePath�. "0 folderposixpath folderPosixPath�- *0 resultingreferences resultingReferencesv +�,�+�*�)�(�'�&d�%�$�#�"�!� ���������������m{����, �+ (0 createexportfolder createExportFolder
�* 
psxp
�) misccura�( 0 nsfilemanager NSFileManager�'  0 defaultmanager defaultManager
�& .corecnte****       ****
�% 
cobj
�$ 
ID  
�# 
filn
�" 
insh
�! 
usMA�  
� .IPXSexponull���     ****� 0 nsstring NSString� &0 stringwithstring_ stringWithString_� >0 stringbydeletingpathextension stringByDeletingPathExtension
� 
TEXT� 0 pathextension pathExtension� &0 fileexistsatpath_ fileExistsAtPath_� � &0 createnumericname createNumericName
� 
msng� <0 moveitematpath_topath_error_ moveItemAtPath_toPath_error_� *0 sortedlistofitemsin sortedListOfItemsIn�  0 revealinfinder revealInFinder�T����������vE�O)j+ 	E�O��,E�O�l  ���,j+ E�O �k�j kh � .�a �/E�O�a ,E�O�a ,E^ O�kva �a fa  UO�a ,] k+ E^ O] j+ a &E^ O] a ,a &E^ O�] %a %a &E^ O�] k+  a E^ Y 	a E^ O] a  %] %E^ O)���] ] a !+ "E^ O�] %E^ O�] %E^ O�] ] a #m+ $[OY�!Y�k  � �a �a �a  UY ��j  ���,j+ E�O�E^ O �k�j kh � &�a �/E�O�a ,E^ O�kva �a fa  UO�a ,] k+ E^ O] j+ a &E^ O�] %a %%a &E^ O�] k+  a &E^ Y 	a 'E^ O] a (%] %E^ O)���] ] a !+ "E^ O�] %E^ O�] %E^ O�] ] a #m+ $O] kE^ [OY�/Y hO�e  3��,E^ Ojklv� *] jl+ )E^ Y *] kl+ )E^ Y �E^ O�e  *] k+ *Y hO] � ���xy�� *0 sortedlistofitemsin sortedListOfItemsIn� �z� z  ��� "0 folderposixpath folderPosixPath� $0 sortkeyindicator sortKeyIndicator�  x ��
�	��������� � "0 folderposixpath folderPosixPath�
 $0 sortkeyindicator sortKeyIndicator�	 0 thensurl theNSURL� $0 thensfilemanager theNSFileManager� 0 keystorequest keysToRequest� 0 theurls theURLs� .0 theinfonsmutablearray theInfoNSMutableArray� 0 i  � 0 annsurl anNSURL� *0 thenssortdescriptor theNSSortDescriptor� 0 
thesepaths 
thesePaths�  ,0 thesealiasreferences theseAliasReferencesy ����"��������������������������������������������������
�� misccura
�� 
pcls�� $0 fileurlwithpath_ fileURLWithPath_�� 0 nsfilemanager NSFileManager��  0 defaultmanager defaultManager�� 0 nsurlpathkey NSURLPathKey�� B0 nsurlcontentmodificationdatekey NSURLContentModificationDateKey�� P0 &nsdirectoryenumerationskipshiddenfiles &NSDirectoryEnumerationSkipsHiddenFiles
�� 
msng�� �� �0 Bcontentsofdirectoryaturl_includingpropertiesforkeys_options_error_ BcontentsOfDirectoryAtURL_includingPropertiesForKeys_options_error_��  0 nsmutablearray NSMutableArray�� 	0 array  �� 	0 count  ��  0 objectatindex_ objectAtIndex_�� <0 resourcevaluesforkeys_error_ resourceValuesForKeys_error_�� 0 
addobject_ 
addObject_�� $0 nssortdescriptor NSSortDescriptor�� .0 nsurllocalizednamekey NSURLLocalizedNameKey�� D0  sortdescriptorwithkey_ascending_  sortDescriptorWithKey_ascending_�� .0 sortusingdescriptors_ sortUsingDescriptors_�� 0 valueforkey_ valueForKey_
�� 
list
�� .corecnte****       ****
�� 
cobj
�� 
psxf
�� 
alis� ����/�k+ E�O��,j+ E�O��,��,lvE�O�����,��+ E�O��,j+ E�O )k�j+ kh ��kk+ E�O����l+ k+ [OY��O�j  �a ,�a ,fl+ E�Y �a ,��,fl+ E�O��kvk+ O���,k+ a &E�OjvE�O #k�j kh �a �/a &a &�6F[OY��O�� ������{|����  0 revealinfinder revealInFinder�� ��}�� }  ���� 0 
theseitems 
theseItems��  { �������������� 0 
theseitems 
theseItems�� 0 	theseurls 	theseURLs�� 0 i  �� 0 thisitem thisItem�� &0 thisitemposixpath thisItemPOSIXPath�� 0 theworkspace theWorkspace| ����������������������
�� 
pcls
�� 
list
�� .corecnte****       ****
�� 
cobj
�� 
psxp
�� misccura�� 0 nsurl NSURL�� $0 fileurlwithpath_ fileURLWithPath_�� 0 nsworkspace NSWorkspace�� "0 sharedworkspace sharedWorkspace�� D0  activatefileviewerselectingurls_  activateFileViewerSelectingURLs_�� ^��,� 
��&E�Y hOjvE�O *k�j kh ��/E�O��,E�O��,�k+ �6F[OY��O��, 	*j+ 	E�UO� *�k+ 
U� ��a����~���� (0 namesoffolderitems namesOfFolderItems�� ����� �  ���� 0 targetfolder targetFolder��  ~ ������������ 0 targetfolder targetFolder�� 0 theurl theURL�� 0 filemanager fileManager�� 0 allurls allURLs�� 0 thenames theNames ����r���z��������������
�� 
pcls
�� 
alis
�� 
ascr
� 
vers
� misccura
� 
psxp� $0 fileurlwithpath_ fileURLWithPath_� 0 nsfilemanager NSFileManager�  0 defaultmanager defaultManager� P0 &nsdirectoryenumerationskipshiddenfiles &NSDirectoryEnumerationSkipsHiddenFiles
� 
msng� � �0 Bcontentsofdirectoryaturl_includingpropertiesforkeys_options_error_ BcontentsOfDirectoryAtURL_includingPropertiesForKeys_options_error_� 0 valueforkey_ valueForKey_
� 
list�� c��,�  &�g ��,� �E�Y ���/��,k+ 	E�VY ���/�k+ 	E�O��,j+ E�O��jv��,��+ E�O�a k+ a &E�O�� �������� (0 createexportfolder createExportFolder�  �  � ����� 20 picturesfolderposixpath picturesFolderPOSIXPath� 0 
targetpath 
targetPath� 0 filemanager fileManager� 0 	theresult 	theResult� �������������
� afdrpdoc
� .earsffdralis        afdr
� 
psxp� 40 generateexportfoldername generateExportFolderName
� misccura� 0 nsfilemanager NSFileManager�  0 defaultmanager defaultManager
� 
msng� � �0 Ccreatedirectoryatpath_withintermediatedirectories_attributes_error_ CcreateDirectoryAtPath_withIntermediateDirectories_attributes_error_
� 
long
� 
psxf
� 
alis� Q�j �,E�O�*j+ %E�O��,j+ E�O 1hZ��f���+ 	E�O��&k e  ��&�&Y �*j+ %E�[OY��� ������� &0 createnumericname createNumericName� ��� �  ������ 0 basename baseName� 00 numericsuffixseparator numericSuffixSeparator� ,0 minimumnumericlength minimumNumericLength� 0 	thisindex 	thisIndex� &0 thisfileextension thisFileExtension�  � ��������� 0 basename baseName� 00 numericsuffixseparator numericSuffixSeparator� ,0 minimumnumericlength minimumNumericLength� 0 	thisindex 	thisIndex� &0 thisfileextension thisFileExtension� *0 thisincrementstring thisIncrementString� ,0 numberofleadingzeros numberOfLeadingZeros� 0 newname newName� ��GX�
� 
ctxt
� 
leng
� 
TEXT� G�j  �E�Y +���&�,E�O�E�O�k  �kh�%E�[OY��Y hO��%�%�%�%�&E�O�� �a������ 40 generateexportfoldername generateExportFolderName�  �  � �������� D0  localizeddatestringforfoldername  localizedDateStringForFolderName� .0 localizedstringobject localizedStringObject� 0 
targetdate 
targetDate� 0 dateformatter dateFormatter� &0 currentdatestring currentDateString� &0 currenttimestring currentTimeString� 0 
foldername 
folderName� q�~�}�|�{�z�y�x�w�v��u�t���s��r�~ 40 getlocalizedstringforkey getLocalizedStringForKey
�} misccura�| 0 nsstring NSString�{ &0 stringwithstring_ stringWithString_�z 0 nsdate NSDate�y 0 date  �x "0 nsdateformatter NSDateFormatter�w 	0 alloc  �v 0 init  �u  0 setdateformat_ setDateFormat_�t "0 stringfromdate_ stringFromDate_�s d0 0stringbyreplacingoccurrencesofstring_withstring_ 0stringByReplacingOccurrencesOfString_withString_
�r 
TEXT� y)�k+ E�O��,�k+ E�O��,j+ E�O��,j+ j+ 	E�O��k+ O��k+ E�O��k+ O��k+ E�O��l+ E�O�a �l+ E�O�a a l+ E�O�a &OP� �q5�p�o���n
�q .PEXTMAPSnull��� ��� null�p  �o �m�l�
�m 
REPR�l "0 thesemediaitems theseMediaItems� �k��j
�k 
MTYP� {�i�h�g�i $0 maptypeindicator mapTypeIndicator�h  
�g 
msng�j  � �f�e�f "0 thesemediaitems theseMediaItems�e $0 maptypeindicator mapTypeIndicator� �d�d (0 showlocationinmaps showLocationInMaps�n *��l+  � �cE�b�a���`�c (0 showlocationinmaps showLocationInMaps�b �_��_ �  �^�]�^ $0 maptypeindicator mapTypeIndicator�] "0 thesemediaitems theseMediaItems�a  � �\�[�Z�Y�X�W�V�U�T�S�R�Q�P�O�N�M�L�K�J�I�H�G�F�E�\ $0 maptypeindicator mapTypeIndicator�[ "0 thesemediaitems theseMediaItems�Z &0 shouldshowtraffic shouldShowTraffic�Y (0 mediaitemsmetadata mediaItemsMetadata�X 0 i  �W 0 thismediaitem thisMediaItem�V 0 thislocation thisLocation�U 0 	thistitle 	thisTitle�T 0 thisid thisID�S 0 	encodedid 	encodedID�R 0 thisurl thisURL�Q 0 thesemapitems theseMapItems�P 0 thismetadata thisMetadata�O (0 thismediaitemtitle thisMediaItemTitle�N 60 thislatitudelongitudepair thisLatitudeLongitudePair�M $0 thismediaitemurl thisMediaItemURL�L 0 thislatitude thisLatitude�K 0 thislongitude thisLongitude�J 0 acoordinate aCoordinate�I 00 aaddressbookdictionary aAddressBookDictionary�H 0 
aplacemark 
aPlacemark�G 0 amapitem aMapItem�F 0 aurl aURL�E  0 mapoptionsdict mapOptionsDict� " ��D�C�B�A�@�?�>�=�<�;�:���9�8�7�6�5�4�3�2�1�0�/�.�-�,���+��*�D 40 getlocalizedstringforkey getLocalizedStringForKey
�C 
pcls
�B 
list
�A .corecnte****       ****
�@ 
cobj
�? 
IPlo
�> 
msng
�= 
pnam
�< 
filn
�; 
ID  �: \0 ,encodereservedcharactersusingpercentencoding ,encodeReservedCharactersUsingPercentEncoding�9 0 latitude  �8 0 	longitude  �7 
�6 misccura�5 0 nsdictionary NSDictionary�4 @0 dictionarywithobjects_forkeys_ dictionaryWithObjects_forKeys_�3 0 mkplacemark MKPlacemark�2 	0 alloc  �1 N0 %initwithcoordinate_addressdictionary_ %initWithCoordinate_addressDictionary_�0 0 	mkmapitem 	MKMapItem�/ (0 initwithplacemark_ initWithPlacemark_�. 0 name  �- 0 nsurl NSURL�,  0 urlwithstring_ URLWithString_�+ D0  openmapswithitems_launchoptions_  openMapsWithItems_launchOptions_
�* .miscactvnull��� ��� null�`�jklmv� jE�Y hO�l  kE�OeE�Y fE�O� ��jv  )j)�k+ Y hOjvE�O��,� 
��&E�Y hO _k�j kh ��/E�O��,E�O���lv 8��,E�O��  
��,E�Y hO��,E�O)�k+ E�O��%E�O���mv�6FY h[OY��O�jv  )j)�k+ Y hUOjvE�O �k�j kh ��/E�O�E[�k/EQ�Z[�l/EQ�Z[�m/EQ�ZO�E[�k/EQ^ Z[�l/EQ^ ZO�] a ] a E^ Oa a ,jvjvl+ E^ Oa a ,j+ ] ] l+ E^ Oa a ,j+ ] k+ E^ O�] a ,FOa a ,�k+ E^ O] �6F[OY�HOa a ,��lva a lvl+ E^ Oa a ,�] l+ Oa   *j !UO�� �)��(�'���&�) \0 ,encodereservedcharactersusingpercentencoding ,encodeReservedCharactersUsingPercentEncoding�( �%��% �  �$�$ 0 
sourcetext 
sourceText�'  � �#�"�!� ����# 0 
sourcetext 
sourceText�" 0 sourcestring sourceString�! (0 reservedcharacters reservedCharacters�  (0 replacementstrings replacementStrings� 0 i  � 0 searchstring searchString� &0 replacementstring replacementString� 2������	!%)-159=AD�MQUY]aeimquy}������������
� misccura� 0 nsstring NSString� &0 stringwithstring_ stringWithString_� 
� .corecnte****       ****
� 
cobj� d0 0stringbyreplacingoccurrencesofstring_withstring_ 0stringByReplacingOccurrencesOfString_withString_
� 
TEXT�& ���,�k+ E�O�������������a a a a a a a a a vE�Oa a a a a a a a  a !a "a #a $a %a &a 'a (a )a *a +a ,a -a vE�O -k�j .kh �a /�/E�O�a /�/E�O���l+ 0E�[OY��O�a 1&� �������� 40 getlocalizedstringforkey getLocalizedStringForKey� ��� �  �� 0 thiskey thisKey�  � �� 0 thiskey thisKey� �
� .sysolocSutxt        TEXT� �j  ascr  ��ޭ