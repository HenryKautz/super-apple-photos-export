#!/bin/bash

for f in *.mov
do
    if [ -f "${f%.mov}.jpg" ] || [ -f "${f%.mov}.jpeg" ] || [ -f "${f%.mov}.heic" ] || [ -f "${f%.mov}.png" ]; then
	echo "Deleting $f"
	rm "$f"
    fi
done
