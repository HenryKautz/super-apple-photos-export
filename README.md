Super Apple Photos Export

11 March 2023: Updated to work under Ventura.

Exports your OS X Apple Photos albums and folders hierarchically.

* Each album turns into a 
folder with photos that are numbered so that they are in the correct
order.
* Photos folders turn into Finder folders, nested hierarchically.
* Exports full 
resolution edited versions of the photos.
* You can select the top level albums 
and folders to export.

Installation:

1. Copy Super Apple Photos Export.scpt to ~/Library/Scripts. Requires the free Photos Utilities library which is originally distributed by http://photosautomation.com/. The version of that library in this repository fixes a bug that arose when Photos changed from using "jpg" to "jpeg" file extensions in 2019.

2. Copy Photos Utilities.scptd to ~/Library/Script Libraries .
3. Optional: copy delete-live-photos.sh to ~/bin.  This shell script deletes any mov file where there is a image file (jpeg, heic, png) with the same base name.  This is useful when exporting albums that contain Apple Live Photos.
4. Optional: copy makewebpage to ~/bin.  This script make a web page pointing to a set of Google Photos albums.  After using Super Apple Photos Export to create a local folder of your albums and then uploading them all to Google Photos, use this script to create an index of those albums.
3. Optional: copy "Set Photo Title to Filename.scpt" to ~/Library/Scripts. This script sets the titles of the selected photos in Photos to their filenames.
